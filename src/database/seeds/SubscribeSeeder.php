<?php use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Models\Subscribers;

class SubscribeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for ($i=0; $i < 200; $i++) { 
            Subscribers::create([
                'email' => $faker->email
            ]);
        }
    }
}
