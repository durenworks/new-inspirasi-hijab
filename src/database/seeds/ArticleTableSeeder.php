<?php use Illuminate\Database\Seeder;

use App\Models\User;
use Carbon\Carbon;
use App\Models\Tag;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;

class ArticleTableSeeder extends Seeder
{
    public function run()
    {
        $beauty = Category::where('name','Beauty')->first();
        $user = User::where('url','robert-dedjumadilakir')->first();
        Tag::Create([
            'name' => 'kecantikan',
            'slug' => 'kecantikan',
            'counter' => 200
        ]);

        Tag::Create([
            'name' => 'wanita',
            'slug' => 'wanita',
            'counter' => 200
        ]);

        for($i = 1; $i <= 200; $i++){
            $slug = 'kecantikan-wanita-'.$i;
            $status = 'waiting for approve';
            $approval_date = null;
            $published_at = null;
            $approval_user_id = null;

            if ($i%3 == 0){
                $status = 'draft';
            }

            if($i%2 == 0){
                $status = 'approved';
                $approval_date = carbon::now();
                $published_at = carbon::now();
                $approval_user_id = $user->id;
            }

            $article = Article::Create([
                'title' => 'kecantikan '.$i,
                'slug' => $slug,
                'category_id' => $beauty->id,
                'user_id' => $user->id,
                'image' => null,
                'content' => "<p>
                Odio quisque cras vel. Ultrices cubilia velit ultrices dictum lobortis, congue primis. Viverra aliquet vel facilisis dui nulla iaculis elit conubia mollis. Nulla rhoncus etiam ridiculus lorem nunc penatibus amet amet iaculis imperdiet platea. Potenti lacinia habitant class aptent mattis elit, vivamus amet cursus mi. Turpis luctus porttitor tellus velit mus cras eleifend penatibus lorem pharetra eleifend eu. Consectetur sit convallis turpis venenatis ornare accumsan nam tortor hendrerit.
                </p>
                <p>
                Et nec ultricies rutrum ante tincidunt magna platea velit. Porttitor fringilla hendrerit egestas. Sodales et tristique velit nunc dolor netus dictum penatibus euismod sit. Curabitur blandit curae; imperdiet posuere, maecenas cursus natoque sem morbi? Risus fusce justo ligula augue vestibulum quam dictumst. Lorem vel non tellus dictum, dapibus eu phasellus. Habitant egestas urna et montes integer integer arcu. Varius ante integer himenaeos mauris urna? Magna sed nibh varius nostra ultricies. Etiam curae; ornare justo aliquet congue diam. Eget dapibus rutrum elementum turpis ac volutpat ornare. Imperdiet senectus suscipit sit nisl ultrices?
                </p>
                <p>
                Imperdiet proin nunc quis ridiculus feugiat facilisi commodo curae; facilisi bibendum. Aptent orci mattis auctor? Duis a sapien rhoncus varius adipiscing fermentum sociis non. Elit est vitae pulvinar conubia aliquet placerat. Himenaeos dignissim potenti neque cum ante sagittis. Etiam mollis laoreet mattis id sodales sagittis mauris platea rutrum feugiat sociosqu congue. Sodales vitae aptent maecenas per penatibus habitasse leo gravida. Sociosqu dis dignissim iaculis gravida sit condimentum adipiscing enim morbi rhoncus libero. Pellentesque urna sem ad turpis tortor!
                </p>
                <p>
                Id adipiscing sed dignissim consectetur tortor netus elit parturient scelerisque nisi himenaeos morbi. Non aptent sem eu. Eget arcu facilisi eleifend accumsan egestas ornare elit interdum varius. Ultricies, rhoncus condimentum vel odio sagittis conubia eget felis justo. Tortor varius pretium nec tristique tincidunt porttitor. Consequat odio natoque nibh condimentum sociosqu aliquet.
                </p>
                <p>
                Interdum at velit maecenas auctor commodo mi integer eget magna sem. Taciti bibendum malesuada justo tristique vel. Sit lacinia cum dictumst ornare. Diam lorem integer sociosqu dis posuere auctor scelerisque fusce mauris accumsan pellentesque. Sociosqu natoque pharetra nascetur. Bibendum nisl egestas, diam porttitor cubilia ut cras hac. Sem tristique platea, eget odio. Taciti curabitur.
                </p>
                <p>
                    Seeding Article By Faker ".$i."
                </p>",
                'status' => $status,
                'published_at' => $published_at,
                'approval_date' => $approval_date,
                'approval_user_id' => $approval_user_id,
                'tags' => 'kecantikan,wanita',
            ]);

            if ($i%1 == 0 && $status == 'approved') {
                for($j = 1; $j <= 50; $j++){
                    $total_comments_article = $article->total_comments;
                    Comment::Create([
                        'article_id' => $article->id, 
                        'content' => 'Seeding Comment By Faker '.$j, 
                    ]);

                    $article->total_comments = $total_comments_article+1;
                    $article->save();

                }
            }
 
        }

        $total_articles = Article::where('user_id',$user->id)
        ->where('status','approved')
        ->count();

        $total_comments = Comment::whereIn('article_id',function($query) use($user){
            $query->select('id')
            ->from('articles')
            ->where([
                ['status','approved'],
                ['user_id',$user->id],
            ]);
        })
        ->count();

        $user->total_articles = $total_articles;
        $user->total_comments = $total_comments;
        $user->save();
    }
}
