<?php use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

use Faker\Factory as Faker;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
   
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'first_name' => 'Admin',
            'last_name' => 'Inspirasi Hijab',
            'phone' => '081111111',
            'url' => 'admin-inspirasi-hijab',
            'role' => 'admin',
            'description' => 'Admin Web Inspirasi Hijab',
            'email' => 'admin@inspirasihijab.com',
            'password' => bcrypt('Administrator'),
            'email_verified_at' => carbon::now()
        ]);

        User::create([
            'name' => 'Anissa',
            'first_name' => 'Anissa',
            'last_name' => 'Azziza',
            'phone' => '08122222222',
            'url' => 'anissa-azziza',
            'role' => 'user',
            'description' => 'Testing User By Seeder',
            'email' => 'anissa@example.com',
            'password' => bcrypt('password'),
            'email_verified_at' => carbon::now()
        ]);

        User::create([
            'name' => 'Hijabers Community',
            'first_name' => 'Hijabers',
            'last_name' => 'Community',
            'phone' => '0813333333',
            'url' => 'hijabers-community',
            'role' => 'community',
            'description' => 'Testing Community By Seeder',
            'email' => 'hijabers.community@dummy.com',
            'password' => bcrypt('password'),
            'email_verified_at' => carbon::now()
        ]);

//        $faker = Faker::create('id_ID');
//
//    	for($i = 1; $i <= 200; $i++){
//            if ($i%1 == 0) $email_verified_at = carbon::now();
//            else $email_verified_at = null;
//
//            User::create([
//    			'name' => $faker->name,
//    			'first_name' => $faker->firstName,
//    			'last_name' => $faker->lastName,
//    			'phone' => $faker->phoneNumber,
//    			'role' => 'user',
//    			'url' => Str::slug($faker->name,'-').'-'.$i,
//    			'description' => 'Seeding User By Faker',
//                'email' =>$faker->email,
//                'password' => bcrypt('password1'),
//                'email_verified_at' => $email_verified_at,
//    		]);
//
//        }
        
    }
}
