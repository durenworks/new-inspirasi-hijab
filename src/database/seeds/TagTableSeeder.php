<?php use Illuminate\Database\Seeder;

use App\Models\Tag;
class TagTableSeeder extends Seeder
{
    public function run()
    {
        Tag::create([
            'name' => 'Hijab',
            'slug' => 'hijab',
            'counter' => '0'
        ]);

        Tag::create([
            'name' => 'Ramadhan',
            'slug' => 'ramadhan',
            'counter' => '0'
        ]);

        Tag::create([
            'name' => 'Muslimah',
            'slug' => 'muslimah',
            'counter' => '0'
        ]);
    }
}
