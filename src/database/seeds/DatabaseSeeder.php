<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('keywords')->delete();
        DB::table('comments')->delete();
        DB::table('articles')->delete();
        DB::table('categories')->whereNotNull('category_id')->delete();
        DB::table('categories')->whereNull('category_id')->delete();
        DB::table('subscribers')->delete();
        DB::table('users')->delete();
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(SubscribeSeeder::class);
        //$this->call(TagTableSeeder::class);
        //$this->call(ArticleTableSeeder::class);
    }
}
