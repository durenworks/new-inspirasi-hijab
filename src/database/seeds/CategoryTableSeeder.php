<?php use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Str;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        $user_1 = User::where('role','user')->orderby('created_at','desc')->first();

        $lifestyle = Category::create([
            'name' => 'Lifestyle',
            'slug' => Str::slug('lifestyle','-'),
            'sequence' => '1',
            'is_show' => true,
            'is_subcategory' => false,
            //'ambasador_user_id' => $user_1->id,
            'quote' => 'Lifestyle Quote from seeder',
        ]);

        Category::create([
            'name' => 'Beauty',
            'slug' => Str::slug('Beauty','-'),
            'sequence' => '1',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Travel',
            'slug' => Str::slug('Travel','-'),
            'sequence' => '2',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Tutorial',
            'slug' => Str::slug('Tutorial','-'),
            'sequence' => '3',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Health and Fresh',
            'slug' => Str::slug('Health and Fresh','-'),
            'sequence' => '4',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Fashion',
            'slug' => Str::slug('Fashion','-'),
            'sequence' => '5',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Trending',
            'slug' => Str::slug('Trending','-'),
            'sequence' => '6',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Community',
            'slug' => Str::slug('Community','-'),
            'sequence' => '7',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        Category::create([
            'name' => 'Culinary',
            'slug' => Str::slug('Culinary','-'),
            'sequence' => '8',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $lifestyle->id,
        ]);

        $knowledge = Category::create([
            'name' => 'Knowledge',
            'slug' => Str::slug('knowledge','-'),
            'sequence' => '2',
            'is_show' => true,
            'is_subcategory' => false,
            //'ambasador_user_id' => $user_1->id,
            'quote' => 'Knowledge Quote from seeder',
        ]);

        Category::create([
            'name' => 'News',
            'slug' => Str::slug('News','-'),
            'sequence' => '1',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'DIY Stuff',
            'slug' => Str::slug('DIY Stuff','-'),
            'sequence' => '2',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'History',
            'slug' => Str::slug('History','-'),
            'sequence' => '3',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'Bisnis Ekonomi',
            'slug' => Str::slug('Bisnis Ekonomi','-'),
            'sequence' => '4',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'Thinking of Islamic',
            'slug' => Str::slug('Thinking of Islamic','-'),
            'sequence' => '5',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'Science & Tech',
            'slug' => Str::slug('Science & Tech','-'),
            'sequence' => '6',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'Culture',
            'slug' => Str::slug('Culture','-'),
            'sequence' => '7',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        Category::create([
            'name' => 'Kesehatan',
            'slug' => Str::slug('Kesehatan','-'),
            'sequence' => '8',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $knowledge->id,
        ]);

        $sakinah = Category::create([
            'name' => 'Sakinah',
            'slug' => Str::slug('sakinah','-'),
            'sequence' => '3',
            'is_show' => true,
            'is_subcategory' => false,
            //'ambasador_user_id' => $user_1->id,
            'quote' => 'Sakinah Quote from seeder',
        ]);

        Category::create([
            'name' => 'Parenting',
            'slug' => Str::slug('Parenting','-'),
            'sequence' => '1',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Sakinnah',
            'slug' => Str::slug('Sakinnah','-'),
            'sequence' => '2',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Decor',
            'slug' => Str::slug('Decor','-'),
            'sequence' => '3',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Wanita Berbicara',
            'slug' => Str::slug('Wanita Berbicara','-'),
            'sequence' => '4',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Finansial',
            'slug' => Str::slug('Finansial','-'),
            'sequence' => '5',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Opini',
            'slug' => Str::slug('Opini','-'),
            'sequence' => '6',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        Category::create([
            'name' => 'Cooking',
            'slug' => Str::slug('Cooking','-'),
            'sequence' => '7',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $sakinah->id,
        ]);

        $tata_hati = Category::create([
            'name' => 'Tata Hati',
            'slug' => Str::slug('Tata Hati','-'),
            'sequence' => '4',
            'is_show' => true,
            'is_subcategory' => false,
            //'ambasador_user_id' => $user_1->id,
            'quote' => 'Tata Hati Quote from seeder',
        ]);

        Category::create([
            'name' => 'Hijrah',
            'slug' => Str::slug('Hijrah','-'),
            'sequence' => '1',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Akhlak',
            'slug' => Str::slug('Akhlak','-'),
            'sequence' => '2',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Tafsir & Hadist',
            'slug' => Str::slug('Tafsir & Hadist','-'),
            'sequence' => '3',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Fiqih',
            'slug' => Str::slug('Fiqih','-'),
            'sequence' => '4',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Caring (Curhat)',
            'slug' => Str::slug('Caring','-'),
            'sequence' => '5',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Psikologi',
            'slug' => Str::slug('Psikologi','-'),
            'sequence' => '6',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Ramadhan',
            'slug' => Str::slug('Ramadhan','-'),
            'sequence' => '7',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Kajian',
            'slug' => Str::slug('Kajian','-'),
            'sequence' => '8',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);

        Category::create([
            'name' => 'Hikmah',
            'slug' => Str::slug('Hikmah','-'),
            'sequence' => '9',
            'is_show' => true,
            'is_subcategory' => true,
            'category_id' => $tata_hati->id,
        ]);
    }
}
