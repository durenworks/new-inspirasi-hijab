<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'article_id','parent_id', 'content','deleted_at','deleted_user_id'
    ];

    public function deletedBy()
    {
        return $this->belongsTo('App\Models\User','deleted_user_id');
    }

    public function parent()
    {
      return $this->belongsTo('App\Models\Comment', 'parent_id');
    }

    public function childs()
    {
      return $this->hasMany('App\Models\Comment', 'parent_id')->whereNull('deleted_at');
    }

    public function article()
    {
      return $this->belongsTo('App\Models\Article');
    }
}
