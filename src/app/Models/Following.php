<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $fillable = [
        'user_id','followed'
    ];

    public function parent()
    {
      return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function childs()
    {
      return $this->hasMany('App\Models\User', 'followed');
    }
}
