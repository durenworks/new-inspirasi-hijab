<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;
use CyrildeWit\EloquentViewable\Viewable;

class Article extends Model implements ViewableContract
{
    use Viewable;

    protected $guarded = ['id'];
    protected $fillable = [
        'category_id','user_id', 'title', 'image','slug','excerpt','content','status','approval_user_id','approval_date','published_at','deleted_at','deleted_user_id','tags','remark','updated_user_id','total_views','total_comments','total_shares'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function approvedBy()
    {
        return $this->belongsTo('App\Models\User','approval_user_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function deletedBy()
    {
        return $this->belongsTo('App\Models\User','deleted_user_id');
    }

    public function getFullPath()
    {
        return Config::get('storage.article') . '/' . e($this->image);
    }

    public function getTumbnailFullPath()
    {
        return Config::get('storage.article_thumbnail') . '/' . e($this->image);
    }

    public function comment()
    {
      return $this->hasMany('App\Models\Comment')->orderby('created_at','desc');
    }

    public function showThumbnail($filename){
        $path = Config::get('storage.article_thumbnail');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function showImage($filename){
        $path = Config::get('storage.article');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    static function random($image){
        $ret = [];
        $path = Config::get('storage.article');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
}
