<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];
    protected $table = 'keywords';

    protected $fillable = [
        'name','slug', 'counter','deleted_at', 'deleted_user_id'
    ];

}
