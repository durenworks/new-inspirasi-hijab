<?php namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;


class Advertisement extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'title','image', 'url', 'click_count','is_active','user_id','deleted_at','image'
    ];

    public function getFullPath(){
        return Config::get('storage.advertisement') . '/' . e($this->filename);
    }

    public function getTumbnailFullPath(){
        return Config::get('storage.advertisement_thumbnail') . '/' . e($this->filename);
    }

    public function showThumbnail($filename){
        $path = Config::get('storage.advertisement_thumbnail');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function showImage($filename){
        $path = Config::get('storage.advertisement');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    static function random($image){
        $ret = [];
        $path = Config::get('storage.advertisement');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
}
