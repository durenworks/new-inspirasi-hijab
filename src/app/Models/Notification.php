<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'user_id','message', 'call_to_action','is_read','type'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
