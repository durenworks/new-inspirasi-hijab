<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $guarded = ['id'];
  protected $fillable = [
    'name', 'slug', 'deleted_at', 'deleted_user_id', 'is_show', 'sequence', 'is_subcategory', 'category_id', 'feature_image', 'quote', 'ambasador_user_id'
  ];

  public function userAmbasador()
  {
    return $this->belongsTo('App\Models\User', 'ambasador_user_id');
  }

  public function parent()
  {
    return $this->belongsTo('App\Models\Category', 'category_id');
  }

  public function childs()
  {
    return $this->hasMany('App\Models\Category', 'category_id')->whereNull('deleted_at')->orderby('sequence', 'asc');
  }

  public function articles()
  {
    return $this->hasMany('App\Models\Article', 'category_id');
  }

  public function getFullPath()
  {
    return Config::get('storage.category') . '/' . e($this->filename);
  }

  public function getTumbnailFullPath()
  {
    return Config::get('storage.category_thumbnail') . '/' . e($this->filename);
  }

  static function random($image)
  {
    $ret = [];
    $path = Config::get('storage.category');
    $extension = $image->getClientOriginalExtension();
    $try = 250;

    do {
      if ($try <= 0)
        throw Exception("Failed to produce randomized filename");

      $hash = static::cleanFilename(str_random(32));
      if ($extension)
        $hash = $hash . '.' . $extension;
      $file = $path . '/' . $hash;
      $try -= 1;
    } while (file_exists($file));

    $ret = [$path, $hash];
    return $ret;
  }

  static function cleanFilename($value)
  {
    $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
    $replacement = '-';
    $value = preg_replace($pattern, $replacement, $value);
    return $value;
  }
}
