<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Event extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['start','end'];
    protected $fillable = [
        'title','slug', 'image','start','end','content','status','deleted_at', 'deleted_user_id','approval_user_id','user_id','approval_date','published_at'
    ];

    public function userApproval()
    {
      return $this->belongsTo('App\Models\User', 'approval_user_id');
    }

    public function userDeleted()
    {
      return $this->belongsTo('App\Models\User', 'deleted_user_id');
    }

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function getFullPath(){
      return Config::get('storage.article') . '/' . e($this->filename);
  }

  public function getTumbnailFullPath(){
      return Config::get('storage.article_thumbnail') . '/' . e($this->filename);
  }

  static function random($image){
      $ret = [];
      $path = Config::get('storage.article');
      $extension = $image->getClientOriginalExtension();
      $try = 250;

      do {
          if ($try <= 0)
              throw Exception("Failed to produce randomized filename");

          $hash = static::cleanFilename(str_random(32));
          if ($extension)
              $hash = $hash . '.' . $extension;
          $file = $path . '/' . $hash;
          $try -= 1;
      } while (file_exists($file));

      $ret = [$path, $hash];
      return $ret;
  }

  static function cleanFilename($value){
      $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
      $replacement = '-';
      $value = preg_replace($pattern, $replacement, $value);
      return $value;
  }
}
