<?php

namespace App\Models;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Rennokki\Befriended\Traits\Follow;
use Rennokki\Befriended\Contracts\Following;

class User extends Authenticatable implements Following
{

    use Follow;
    use Notifiable;
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'phone',
        'url',
        'role',
        'description',
        'email',
        'password',
        'facebook_url',
        'twitter_url',
        'youtube_url',
        'email_verified_at',
        'last_login',
        'deleted_at',
        'deleted_user_id',
        'total_following',
        'total_followers',
        'total_articles',
        'total_comments',
        'total_reads',
        'avatar',
        'instagram_url'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateConfirmHash()
    {
        $s = hash_hmac('sha1', $this->email, $this->created_at, true);
        $s = base64_encode($s);
        $s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
        return $s;
    }

    public function checkConfirmHash($hash)
    {
        $s = hash_hmac('sha1', $this->email, $this->created_at, true);
        $s = base64_encode($s);
        $s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
        return $s == $hash;
    }

    public function article()
    {
        return $this->hasMany('App\Models\Article');
    }

    public function CheckFollowStatus($user_follow_id, $user_id)
    {
        $is_exists = db::table('followers')
            ->where([
                ['follower_id', $user_id],
                ['followable_id', $user_follow_id],
            ])
            ->exists();

        if ($is_exists) return 'Un Follow';
        else return 'Follow';
    }
}
