<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        session()->flush();

        $item = Setting::where('name', 'facebook')->first();
        if ($item) {
            $facebook = $item->value;
        }
        else {
            $facebook = '';
        }

        session(['facebook_url' => $facebook]);

        $item = Setting::where('name', 'instagram')->first();
        if ($item) {
            $instagram = $item->value;
        }
        else {
            $instagram = '';
        }

        session(['instagram_url' => $instagram]);

        $item = Setting::where('name', 'twitter')->first();
        if ($item) {
            $twitter = $item->value;
        }
        else {
            $twitter = '';
        }

        session(['twitter_url' => $twitter]);

        $item = Setting::where('name', 'youtube')->first();
        if ($item) {
            $youtube = $item->value;
        }
        else {
            $youtube = '';
        }

        session(['youtube_url' => $youtube]);
    }
}
