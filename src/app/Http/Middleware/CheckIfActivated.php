<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckIfActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admins')->user()->is_active == false) {
            Auth::guard('admins')->logout();

            return redirect()->action('Admin\Auth\LoginController@showLoginForm')
                    ->with('msg', 'This Account has Banned');
        }

        /*if (Auth::check() && Auth::user()->state != 'ACT') {
            Auth::logout();

            return redirect()->action('AccountController@showUnconfirmed');
        }*/

        return $next($request);
    }
}
