<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard('admins');
        
        //dd($auth);
        if (!$auth->check()) {
            return redirect('/admin-inspirasi-hijab/login');
        }

        
        if (Auth::guard('admins')->user()->deleted_at  != null) {
            Auth::guard('admins')->logout();

            return redirect()->action('Backend\Auth\LoginController@showLoginForm')->with('msg', 'This Account has Banned');
            
        }else if (Auth::guard('admins')->user()->email_verified_at == null) {
            Auth::guard('admins')->user()->logout();
            
            return redirect()->action('Backend\Auth\LoginController@showLoginForm')->with('msg', 'This Account not active yet');
        }

        return $next($request);
    }
}
