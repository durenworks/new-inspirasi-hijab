<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use StdClass;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            $data = db::select(db::raw("select users.id AS id
            ,users.name
            ,users.first_name
            ,users.last_name
            ,users.phone
            ,users.role
            ,users.email
            ,users.created_at
            from users 
            where users.deleted_at is null
            and role = 'user' or role = 'admin'"));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'edit_modal' => route('user.admin.edit',$data->id),
                    'delete' => route('user.admin.delete',$data->id),
                ]);
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }

        return view('backend.user.index');
    }

    public function store(Request $request)
    {

        $name = ucwords($request->name);
        $first_name = ucwords($request->first_name);
        $last_name = ucwords($request->last_name);
        $phone = $request->phone;
        $email = $request->email;
        $password = $request->password;
        $description = $request->description;
        $slug = Str::slug($name,'-');

        $is_exists = User::where('url',$slug)->whereNull('deleted_at')->exists();
        if($is_exists) return response()->json($name.' Is already exists',422);

        User::firstorCreate([
            'name' => $name,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'phone' => $phone,
            'url' => $slug,
            'role' => 'admin',
            'description' => $description,
            'email' => $email,
            'email_verified_at' => carbon::now(),
            'password' => bcrypt($password)
        ]);

        return response()->json(200);
    }

    public function edit($id){
        $user = User::find($id);
        $obj = new stdClass();
        $obj->name = $user->name;
        $obj->first_name = $user->first_name;
        $obj->last_name = $user->last_name;
        $obj->phone = $user->phone;
        $obj->description = $user->description;
        $obj->email = $user->email;
        $obj->url_update = route('user.admin.update',$id);

        return response()->json($obj,200);
    }


    public function update(Request $request, $id)
    {
        $name = ucwords($request->name);
        $first_name = ucwords($request->first_name);
        $last_name = ucwords($request->last_name);
        $phone = $request->phone;
        $email = $request->email;
        $password = $request->password;
        $description = $request->description;
        $slug = Str::slug($name,'-');

        if(User::where('url',$slug)->where('id','!=',$id)->whereNull('deleted_at')->exists()) return response()->json($name.' Is already exists',422);
        
        $user = User::find($id);
        $user->name = $name;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->url = $slug;
        $user->phone = $phone;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->description = $description;
        $user->save();

        return response()->json(200);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->deleted_at = carbon::now();
        $user->deleted_user_id = Auth::guard('admins')->user()->id;
        $user->save();
    }

    public function approval()
    {
        return view('backend.user.approval.index');
    }

    public function show($id){
        $user = User::find($id);
        return response()->json($user);
    }

    public function approved(Request $request)
    {
        $community = User::find($request->id);
        $community->email_verified_at = carbon::now();
        $community->save();
        return response()->json(200);
    }

    public function waitingData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select users.id AS id
            ,users.name
            ,users.first_name
            ,users.last_name
            ,users.phone
            ,users.role
            ,users.email
            ,users.email_verified_at
            ,users.created_at
            from users 
            where users.deleted_at is null and (role ='user' or role='admin')
            and email_verified_at is null"));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail_modal' => route('user.admin.show',$data->id)
                ]);
            })
            ->editColumn('email_verified_at',function($data){
                return '<span class="label label-default">Waiting For Approve</span>';
            })
            ->rawColumns(['action','email_verified_at'])
            ->addIndexColumn()
            ->make(true);
        }
    }
}
