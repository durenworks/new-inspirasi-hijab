<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use StdClass;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Setting;

class SettingController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            $data = db::select(db::raw("select settings.id AS id
            ,settings.name
            ,settings.value
            ,settings.created_at
            from settings"));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'edit_modal' => route('setting.admin.edit',$data->id),
                    'delete' => route('setting.admin.delete',$data->id),
                ]);
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }

        return view('backend.setting.index');
    }

    public function store(Request $request)
    {
        $name = $request->name;
        $value = $request->value;
        
        $is_exists = Setting::where('name',$name)->exists();
        if($is_exists) return response()->json($name.' Is already exists',422);

        Setting::firstorCreate([
            'name' => $name,
            'value' => $value
        ]);

        return response()->json(200);
    }

    public function edit($id){
        $setting = Setting::find($id);
        $obj = new stdClass();
        $obj->name = $setting->name;
        $obj->value = $setting->value;
        $obj->url_update = route('setting.admin.update',$id);

        return response()->json($obj,200);
    }


    public function update(Request $request, $id)
    {
        $name = $request->name;
        $value = $request->value;
       
        if(Setting::where('name',$name)->where('id','!=',$id)->exists()) return response()->json($name.' Is already exists',422);
        
        $setting = Setting::find($id);
        $setting->name = $name;
        $setting->value = $value;
        $setting->save();

        return response()->json(200);
    }

    public function destroy($id)
    {
        $setting = Setting::find($id);
        $setting->delete();
    }
}
