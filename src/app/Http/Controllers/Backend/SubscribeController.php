<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Excel;
use Yajra\Datatables\Datatables;

use App\Models\Subscribers;
class SubscribeController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            $data = Subscribers::orderby('email','desc');
            
            return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
        }

        return view('backend.subscribe.index');
    }

    public function export()
    {
        $data = Subscribers::orderby('email','desc')->get();
        return Excel::create('subscribes',function ($excel) use($data){
            $excel->sheet('active', function($sheet) use($data) {
                $sheet->setCellValue('A1','Email');
                $row=2;
                foreach ($data as $datum) {  
                  $sheet->setCellValue('A'.$row,$datum->email);
                  $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }
}
