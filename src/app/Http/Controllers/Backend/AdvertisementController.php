<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use Config;
use StdClass;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Advertisement;


class AdvertisementController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->has('search') ? strtoupper(trim($request->search)) : null;

        $advertisements = Advertisement::whereNull('deleted_at');

        if ($search) $advertisements = $advertisements->where(DB::raw('upper(title)'), 'LIKE', "%$search%");

        $advertisements = $advertisements->orderby('created_at', 'desc')->get();
        return view('backend.advertisement.index', compact('advertisements'));
    }

    public function store(Request $request)
    {
        $advertisement_thumbnaile_storage = Config::get('storage.advertisement_thumbnail');
        $advertisement_storage = Config::get('storage.advertisement');

        if (!File::exists($advertisement_thumbnaile_storage)) File::makeDirectory($advertisement_thumbnaile_storage, 0777, true);
        if (!File::exists($advertisement_storage)) File::makeDirectory($advertisement_storage, 0777, true);


        $is_exists = Advertisement::where('slug', Str::slug($request->title, '-'))->exists();
        if ($is_exists) return response()->json($request->title . ' Is already exists', 422);

        $image = $request->file('file_upload');
        if ($image) {
            $random = Advertisement::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(550, 550);
        } else {
            $_random = null;
        }


        $advertisement = Advertisement::create([
            'title'     => ucwords($request->title),
            'url'       => $request->url,
            'slug'      => Str::slug($request->title, '-'),
            'image'     => $_random
        ]);

        if ($advertisement->save() && $image) {
            $image->move($advertisement_storage, $random[1]);
            $image_resize->save($advertisement_thumbnaile_storage . '/' . $random[1]);
        }


        return response()->json(200);
    }

    public function edit($id)
    {
        $advertisement = Advertisement::find($id);
        $obj = new stdClass();
        $obj->id = $advertisement->id;
        $obj->url = $advertisement->url;
        $obj->title = $advertisement->title;
        if($advertisement->image) $obj->url_image = route('advertisement.showThumbnail', $advertisement->image);
        else $obj->url_image = null;
        
        $obj->url_update = route('advertisement.update', $advertisement->id);

        return response()->json($obj);
    }

    public function update(Request $request, $id)
    {
        $advertisement_thumbnaile_storage = Config::get('storage.advertisement_thumbnail');
        $advertisement_storage = Config::get('storage.advertisement');

        $is_exists = Advertisement::where([
            ['slug', Str::slug($request->title, '-')],
            ['id', '!=', $id],
        ])
            ->exists();

        if ($is_exists) return response()->json($request->title . ' Is already exists', 422);

        if (!File::exists($advertisement_thumbnaile_storage)) File::makeDirectory($advertisement_thumbnaile_storage, 0777, true);
        if (!File::exists($advertisement_storage)) File::makeDirectory($advertisement_storage, 0777, true);

        $advertisement = Advertisement::find($id);
        $old_image = $advertisement->image;
        $image = $request->file('image');

        if ($image) {
            $old_file_thumbnail = Config::get('storage.advertisement_thumbnail') . '/' . $old_image;
            $old_file = Config::get('storage.advertisement') . '/' . $old_image;

            if (File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
            if (File::exists($old_file)) File::delete($old_file);


            $random = Advertisement::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType();
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $image_resize->resize(550, 550);
        } else {
            $_random = $old_image;
        }

        $advertisement->title = ucwords($request->title);
        $advertisement->slug = Str::slug($request->title, '-');
        $advertisement->image = $_random;
        $advertisement->url     = $request->url;
        $advertisement->save();

        if ($advertisement->save() && $image) {
            $image->move($advertisement_storage, $random[1]);
            $image_resize->save($advertisement_thumbnaile_storage . '/' . $random[1]);
        }

        return response()->json(200);
    }

    public function showThumbnail($filename)
    {
        $path = Config::get('storage.advertisement_thumbnail');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.advertisement');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function destroy($id)
    {
        $advertisement = Advertisement::findOrFail($id);
        $image = $advertisement->getFullPath();
        $image_tumbnail = $advertisement->getTumbnailFullPath();

        if ($advertisement->delete()) {
            if (File::exists($image)) @unlink($image);
            if (File::exists($image_tumbnail)) @unlink($image_tumbnail);
        }
    }
}
