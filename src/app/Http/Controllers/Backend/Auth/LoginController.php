<?php namespace App\Http\Controllers\Backend\Auth;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/admin-inspirasi-hijab/dashboard';

    public function __construct()
    {
        $this->middleware('guest.admin', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('admins');
    }

    public function login(Request $request){
    	$this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

    	$credentials = $this->credentials($request);
		if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
   
            if ($user->deleted_at == null) {
                $update_user = User::find($user->id);
                $update_user->last_login = carbon::now();
                $update_user->save();

                Auth::guard('admins')->login($user, $request->has('remember'));
                return redirect()->intended($this->redirectPath());
                    
            } else if($user->deleted_at){
                Session::flash("flash_notification", [
                        "level"=>"info",
                        "message"=> "User sudah tidak aktif lagi"
                ]);
                
                return  redirect()
                ->back()
                ->withInput($request->only($this->username(), 'remember'));
            }
        }

        return redirect('/admin-inspirasi-hijab/login')
        ->withInput($request->only('email', 'remember'))
        ->withErrors([
            'email' => 'Incorrect Email or password',
        ]);
    }
    
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
