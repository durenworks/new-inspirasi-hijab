<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Event;

class EventController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            $data = db::select(db::raw("select events.id AS id
            ,events.title as title
            ,events.content as content
            ,events.start
            ,events.end
            ,events.status
            ,events.published_at
            ,events.created_at
            ,users.name as created_by
            from events 
            left join users on users.id = events.user_id 
            where events.deleted_at is null
            and events.user_id = '".auth::guard('admins')->user()->id."'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'edit' => route('event.admin.edit',$data->id),
                    'delete' => route('event.admin.delete',$data->id),
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                if($data->status == 'approved') return '<span class="label label-success">Approved</span>';
                else if($data->status == 'rejected')  return '<span class="label label-error">Rejected</span>';
                else return '<span class="label label-default">Waiting For Approve</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }

        $flag = Session::get('flag');
        return view('backend.event.index',compact('flag'));
    }

    public function create()
    {
        return view('backend.event.create');
    }

    public function store(Request $request)
    {
        if(Session::has('flag')) Session::forget('flag');
        $event_thumbnaile_storage = Config::get('storage.event_thumbnail');
        $event_storage = Config::get('storage.event');

        $title = ucwords($request->title);
        $slug = Str::slug($title,'-');

        if (!File::exists($event_thumbnaile_storage)) File::makeDirectory($event_thumbnaile_storage, 0777, true);
        if (!File::exists($event_storage)) File::makeDirectory($event_storage, 0777, true);

        $is_exists = Event::where('slug',$slug)->whereNull('deleted_at')->exists();
        if($is_exists) return response()->json($name.' Is already exists',422);

        $event_date = explode('-',$request->event_date);
        $start_event  = Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[0]))->format('Y-m-d H:i:s'); 
        $end_event  =  Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[1]))->format('Y-m-d H:i:s');
        $content = $request->content;
        $image = $request->file('file_upload');
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if($image){
            $random = Event::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width/$newWidth;
            $newHeight = $height/$diff;
            $image_resize->resize($newWidth, $newHeight);
        }else{
            $_random = null;
        }

        $event = Event::firstorCreate([
            'title' => $title,
            'slug' => $slug,
            'user_id' => Auth::guard('admins')->user()->id,
            'image' => $_random,
            'slug' => $slug,
            'content' => $content,
            'status' => 'waiting for approve',
            'start' => $start_event,
            'end' => $end_event,
        ]);

        if ($event->save()){
            if($image){
                $image->move($event_storage, $random[1]);
                $image_resize->save($event_thumbnaile_storage.'/'.$random[1]);
            }
        }

    }

    public function edit($id)
    {
        $event = Event::find($id);
        return view('backend.event.edit',compact('event'));
    }

    public function showThumbnail($filename){
        $path = Config::get('storage.event_thumbnail');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
   
    public function update(Request $request, $id)
    {
        if(Session::has('flag')) Session::forget('flag');
        $event_thumbnaile_storage = Config::get('storage.event_thumbnail');
        $event_storage = Config::get('storage.event');

        $title = ucwords($request->title);
        $slug = Str::slug($title,'-');

        if (!File::exists($event_thumbnaile_storage)) File::makeDirectory($event_thumbnaile_storage, 0777, true);
        if (!File::exists($event_storage)) File::makeDirectory($event_storage, 0777, true);

        $is_exists = Event::where([['slug',$slug],['id','!=',$id]])->whereNull('deleted_at')->exists();
        if($is_exists) return response()->json($name.' Is already exists',422);

        $event_date = explode('-',$request->event_date);
        $start_event  = Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[0]))->format('Y-m-d H:i:s'); 
        $end_event  =  Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[1]))->format('Y-m-d H:i:s');
        $content = $request->content;
        $image = $request->file('file_upload');
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        $event = Event::find($id);
        $old_image = $event->image;

        if($image){
            $old_file_thumbnail = $event_thumbnaile_storage . '/' . $old_image;
            $old_file = $event_storage . '/' . $old_image;
            
            if(File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
            if(File::exists($old_file)) File::delete($old_file);

            $random = Article::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width/$newWidth;
            $newHeight = $height/$diff;
            $image_resize->resize($newWidth, $newHeight);
        }else{
            $_random = $old_image;
        }

        $event->title = $title;
        $event->slug = $slug;
        $event->image = $_random;
        $event->content = $content;
        $event->start = $start_event;
        $event->end = $end_event;
        //$article->updated_user_id = Auth::guard('admins')->user()->id;
        $event->save();

        if ($event->save()){
            if($image){
                $image->move($event_storage, $random[1]);
                $image_resize->save($event_thumbnaile_storage.'/'.$random[1]);
            }
        }
    }

    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $image = $event->getFullPath();
        $image_tumbnail = $event->getTumbnailFullPath();
        $event->deleted_at = carbon::now();
        $event->deleted_user_id = Auth::guard('admins')->user()->id;

        if ($event->save()){
            if(File::exists($image)) @unlink($image);
            if(File::exists($image_tumbnail)) @unlink($image_tumbnail);
        }

        return response()->json(200);
    }

    public function approval()
    {
        $flag = Session::get('flag');
        $active_tab = 'waiting';
        return view('backend.event.approval.index',compact('flag','active_tab'));
    }

    public function approved(Request $request, $id)
    {
        if(Session::has('flag')) Session::forget('flag');

        $is_updated = $request->is_updated;
        if($is_updated == 'edit'){
            if(Session::has('flag')) Session::forget('flag');
            $event_thumbnaile_storage = Config::get('storage.event_thumbnail');
            $event_storage = Config::get('storage.event');

            $title = ucwords($request->title);
            $slug = Str::slug($title,'-');

            if (!File::exists($event_thumbnaile_storage)) File::makeDirectory($event_thumbnaile_storage, 0777, true);
            if (!File::exists($event_storage)) File::makeDirectory($event_storage, 0777, true);

            $is_exists = Event::where([['slug',$slug],['id','!=',$id]])->whereNull('deleted_at')->exists();
            if($is_exists) return response()->json($name.' Is already exists',422);

            $event_date = explode('-',$request->event_date);
            $start_event  = Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[0]))->format('Y-m-d H:i:s'); 
            $end_event  =  Carbon::createFromFormat('d/m/Y h:i A', trim($event_date[1]))->format('Y-m-d H:i:s');
            $content = $request->content;
            $image = $request->file('file_upload');
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $content = $dom->saveHTML();

            $event = Event::find($id);
            $old_image = $event->image;

            if($image){
                $old_file_thumbnail = $event_thumbnaile_storage . '/' . $old_image;
                $old_file = $event_storage . '/' . $old_image;
                
                if(File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
                if(File::exists($old_file)) File::delete($old_file);

                $random = Article::random($image);
                $_random = $random[1];
                $image_mimetype = $image->getMimeType(); 
                $image_resize = Image::make($image->getRealPath());
                $width = $image_resize->width();
                $height = $image_resize->height();

                $newWidth = 700;
                $diff = $width/$newWidth;
                $newHeight = $height/$diff;
                $image_resize->resize($newWidth, $newHeight);
            }else{
                $_random = $old_image;
            }

            $event->title = $title;
            $event->slug = $slug;
            $event->image = $_random;
            $event->content = $content;
            $event->start = $start_event;
            $event->end = $end_event;
            //$article->updated_user_id = Auth::guard('admins')->user()->id;
            $event->save();

            if ($event->save()){
                if($image){
                    $image->move($event_storage, $random[1]);
                    $image_resize->save($event_thumbnaile_storage.'/'.$random[1]);
                }
            }
        }else if($is_updated == 'approved'){
            $event = Event::find($id);
            $event->status = 'approved';
            $event->approval_user_id = Auth::guard('admins')->user()->id;
            $event->approval_date = carbon::now();
            $event->published_at = carbon::now();
            $event->save();
        }
        

        Session::flash('flag', 'success_2');
        return response()->json(200);
    }

    public function show($id,$status){
        $event = Event::find($id);
        return view('backend.event.approval.show',compact('event','status'));
    }

    public function waitingData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select events.id AS id
            ,events.title as title
            ,events.content as content
            ,events.start
            ,events.end
            ,events.status
            ,events.published_at
            ,events.created_at
            ,users.name as created_by
            from events 
            left join users on users.id = events.user_id 
            where events.deleted_at is null
            and events.status = 'waiting for approve'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('event.admin.show',[$data->id,'waiting'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-default">Waiting For Approve</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function approvedData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select events.id AS id
            ,events.title as title
            ,events.content as content
            ,events.start
            ,events.end
            ,events.status
            ,events.published_at
            ,events.created_at
            ,users.name as created_by
            from events 
            left join users on users.id = events.user_id 
            where events.deleted_at is null
            and events.status = 'approved'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('event.admin.show',[$data->id,'approved'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-success">Approved</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function rejectedData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select events.id AS id
            ,events.title as title
            ,events.content as content
            ,events.start
            ,events.end
            ,events.status
            ,events.published_at
            ,events.created_at
            ,users.name as created_by
            from events 
            left join users on users.id = events.user_id 
            where events.deleted_at is null
            and events.status = 'rejected'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('event.admin.show',[$data->id,'rejected'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-error">Rejected</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }
}
