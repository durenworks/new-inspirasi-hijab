<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Category;


class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $data = db::select(db::raw("select categories.id AS id
            ,categories.name
            ,parent.name as parent_name
            ,categories.sequence
            ,categories.quote
            ,ambasador.name as ambasador_name
            ,COALESCE(categories.category_id,categories.id) as category_id
            ,categories.created_at
            from categories 
            left join categories as parent on categories.category_id = parent.id 
            left join users as ambasador on ambasador.id = categories.ambasador_user_id 
            where categories.is_show = true and categories.deleted_at is null
            order by COALESCE(categories.category_id,categories.id) asc,
            parent_name asc,
            categories.sequence asc"));

            return datatables()->of($data)
                ->addColumn('action', function ($data) {
                    return view('backend._action', [
                        'model' => $data,
                        'edit_modal' => route('category.edit', $data->id),
                        'delete' => route('category.delete', $data->id),
                    ]);
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }


        $categories = Category::whereNull('deleted_at')->pluck('name', 'id')->all();
        $users = User::where('role', 'user')->whereNull('deleted_at')->pluck('name', 'id')->all();
        return view('backend.category.index', compact('categories', 'users'));
    }

    public function store(request $request)
    {
        $category_thumbnaile_storage = Config::get('storage.category_thumbnail');
        $category_storage = Config::get('storage.category');

        if (!File::exists($category_thumbnaile_storage)) File::makeDirectory($category_thumbnaile_storage, 0777, true);
        if (!File::exists($category_storage)) File::makeDirectory($category_storage, 0777, true);

        $name = ucwords($request->name);
        $slug = Str::slug($name, '-');
        $quote = $request->quote;
        $category_id = $request->category_id;
        $ambasador_id = $request->ambasador_id;
        $is_subcategory = ($category_id) ? true : false;
        $image = $request->file('file_upload');

        if ($category_id) {
            $max_sequence = Category::where('category_id', $category_id)->max('sequence');
            if ($max_sequence == 0) $sequence = 1;
            else $sequence = $max_sequence + 1;
        } else {
            $max_sequence = Category::max('sequence');
            if ($max_sequence == 0) $sequence = 1;
            else $sequence = $max_sequence + 1;
        }


        $is_exists = Category::where('slug', $slug)->whereNull('deleted_at')->exists();
        if ($is_exists) return response()->json($name . ' Is already exists', 422);

        if ($image) {
            $random = Category::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(550, 550);
        } else {
            $_random = null;
        }

        $category = Category::firstorCreate([
            'name' => $name,
            'slug' => $slug,
            'category_id' => $category_id,
            'is_show' => true,
            'is_subcategory' => $is_subcategory,
            'ambasador_user_id' => $ambasador_id,
            'quote' => $quote,
            'sequence' => $sequence,
            'feature_image' => $random[1],
        ]);

        if ($category->save() && $image) {
            $image->move($category_storage, $random[1]);
            $image_resize->save($category_thumbnaile_storage . '/' . $random[1]);
        }
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $obj = new stdClass();
        $obj->name = $category->name;
        $obj->sequence = $category->sequence;
        $obj->parent_id = $category->category_id;
        $obj->user_ambasador_id = $category->ambasador_user_id;
        $obj->quote = $category->quote;
        // $obj->image = route('category.admin.showThumbnail',$category->feature_image);
        $obj->url_update = route('category.update', $id);

        return response()->json($obj, 200);
    }

    public function update(Request $request, $id)
    {
        $category_thumbnaile_storage = Config::get('storage.category_thumbnail');
        $category_storage = Config::get('storage.category');

        if (!File::exists($category_thumbnaile_storage)) File::makeDirectory($category_thumbnaile_storage, 0777, true);
        if (!File::exists($category_storage)) File::makeDirectory($category_storage, 0777, true);


        $name = ucwords($request->name);
        $slug = Str::slug($name, '-');
        $quote = $request->quote;
        $category_id = $request->category_id;
        $ambasador_id = $request->ambasador_id;
        $is_subcategory = ($category_id) ? true : false;
        $image = $request->file('file_upload');

        if ($category_id) {
            $max_sequence = Category::where('category_id', $category_id)->max('sequence');
            if ($max_sequence == 0) $sequence = 1;
            else $sequence = $max_sequence + 1;
        } else {
            $max_sequence = Category::max('sequence');
            if ($max_sequence == 0) $sequence = 1;
            else $sequence = $max_sequence + 1;
        }

        if (Category::where('slug', $slug)->whereNull('deleted_at')->where('id', '!=', $id)->exists())
            return response()->json($name . ' Is already exists', 422, 422);

        $category = Category::find($id);
        $old_image = $category->feature_image;

        if ($image) {
            $old_file_thumbnail = null;
            $old_file = null;
            if ($old_image) {
                $old_file_thumbnail = $category_thumbnaile_storage . '/' . $old_image;
                $old_file = $category_storage . '/' . $old_image;
            }

            if ($old_file && $old_file_thumbnail) {
                if (File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
                if (File::exists($old_file)) File::delete($old_file);
            }

            $random = Category::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType();
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $image_resize->resize(550, 550);
        } else {
            $_random = $old_image;
        }


        $category->name = ucwords($request->name);
        $category->slug = $slug;
        $category->category_id = $category_id;
        $category->is_show = true;
        $category->is_subcategory = $is_subcategory;
        $category->sequence = $sequence;
        $category->feature_image = $_random;
        $category->quote = $quote;
        $category->ambasador_user_id = $ambasador_id;
        $category->save();

        return response()->json(200);
    }

    public function destroy(Request $request, $id)
    {
        $category = Category::find($id);
        $category->is_show = false;
        $category->deleted_at = carbon::now();
        $category->deleted_user_id = Auth::guard('admins')->user()->id;
        $category->save();
    }

    public function showThumbnail($filename)
    {
        $path = Config::get('storage.category_thumbnail');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.category');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}




 /*
    
    $data = Category::whereNull('deleted_at')
    ->whereNull('category_id')
    ->where('is_show',true)
    ->orderby('sequence','asc')
    ->get();

    $array = array();
    foreach ($data as $key => $datum) {
        $obj = new stdClass();
        $obj->id = $datum->id;
        $obj->name = $datum->name;
        $obj->slug = $datum->slug;
        $obj->sequence = $datum->sequence;
        $obj->child = $this->getChild($datum->id,1);
        $array [] = $obj;
    }

    function getChild($id,$level){
        $checks = collect(Category::where([
            ['category_id',$id],
            ['is_show',true]
        ])
        ->orderby('sequence','asc')->get());

        $child = array();
        foreach ($checks as $key => $check) {
            $val = $check->toArray();
            $obj = new stdClass();
            $obj->id = $val['id'];
            $obj->name = $val['name'];
            $obj->slug = $val['slug'];
            $obj->sequence = $val['sequence'];
            $obj->child =$this->getChild($val['id'],++$level);
            $child [] = $obj;
        }

        return $child;
    }*/
