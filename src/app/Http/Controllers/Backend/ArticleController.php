<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use SEO\Seo;
use App\Models\Tag;
use App\Models\Comment;
use App\Models\Article;
use App\Models\Category;
use App\Models\Notification;


class ArticleController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            $data = db::select(db::raw("select articles.id AS id
            ,articles.title as title
            ,articles.content as content
            ,articles.tags as tags
            ,categories.name as category_name
            ,articles.status
            ,articles.published_at
            ,articles.created_at
            from articles 
            left join categories on articles.category_id = categories.id 
            where articles.deleted_at is null
            and articles.user_id = '".auth::guard('admins')->user()->id."'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'edit' => route('article.admin.edit',$data->id),
                    'delete' => route('article.admin.delete',$data->id),
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                if($data->status == 'approved') return '<span class="label label-success">Approved</span>';
                else if($data->status == 'rejected')  return '<span class="label label-error">Rejected</span>';
                else return '<span class="label label-default">Waiting For Approve</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }

        $flag = Session::get('flag');
        return view('backend.article.index',compact('flag'));
    }

    public function create()
    {
        $categories = Category::where('is_subcategory','0')->whereNull('deleted_at')->orderby('sequence','asc')->get();
        $tags = Tag::select('name')->whereNull('deleted_at')->get();
        $array = array();
        foreach ($tags as $key => $value) {
            $array [] = $value->name;
        }
        return view('backend.article.create',compact('categories','array'));
    }

    public function store(Request $request)
    {
        if(Session::has('flag')) Session::forget('flag');
        $article_thumbnaile_storage = Config::get('storage.article_thumbnail');
        $article_storage = Config::get('storage.article');

        if (!File::exists($article_thumbnaile_storage)) File::makeDirectory($article_thumbnaile_storage, 0777, true);
        if (!File::exists($article_storage)) File::makeDirectory($article_storage, 0777, true);


        $title = ucwords($request->title);
        $selected_tag = $request->selected_tag;
        
        if($selected_tag)
        {
            $replace = str_replace(","," ",$selected_tag);
            $count = Article::where('tags',$selected_tag)->count();
            if($count == 0) $sequence = 1;
            else $sequence = $count+1;
            $slug = Str::slug($replace,'-').'-'.$sequence;
        } else
        {
            $count = Article::where('title',$title)->count();
            if($count == 0) $sequence = 1;
            else $sequence = $count+1;
            $slug = Str::slug($title,'-').'-'.$sequence;
        } 

        $selected_category = $request->selected_category;
        $content = $request->content;
        $image = $request->file('file_upload');
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if($image)
        {
            $random = Article::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width/$newWidth;
            $newHeight = $height/$diff;
            $image_resize->resize($newWidth, $newHeight);
        }else
        {
            $_random = null;
        }

        $article = Article::firstorCreate([
            'title' => $title,
            'slug' => $slug,
            'category_id' => $selected_category,
            'user_id' => Auth::guard('admins')->user()->id,
            'image' => $_random,
            'slug' => $slug,
            'content' => $content,
            'status' => 'waiting for approve',
            'tags' => $selected_tag,
            
        ]);

        if ($article->save())
        {
            if($image)
            {
                $image->move($article_storage, $random[1]);
                $image_resize->save($article_thumbnaile_storage.'/'.$random[1]);
            }
           
            if($selected_tag)
            {
                $split_tag = explode(',',$selected_tag);
                foreach ($split_tag as $key => $value) {
                    $tag_slug = Str::slug($value,'-');
                    $is_exists = Tag::where('slug',$tag_slug)->first();

                    if($is_exists){
                        $counter = $is_exists->counter;
                        $is_exists->counter = $counter+1;
                        $is_exists->save();
                    }else{
                        Tag::firstorCreate([
                            'name' => $value,
                            'slug' => $tag_slug,
                            'counter' => 1
                        ]);
                    }
                }
            }

            Seo::save($article, route('detail', $article->slug),
            [
                'title'     => $article->title,
                'tags'      => $selected_tag
            ], 
            [ 
                'title'     => $article->title,
                'tags'      => $selected_tag
                , 'images'  => [ $article->getFullPath() ] 
            ]); 
            
        }

        Session::flash('flag', 'success');
        return response()->json(200);
    }

    public function edit($id)
    {
        $categories = Category::where('is_subcategory','0')->whereNull('deleted_at')->orderby('sequence','asc')->get();
        $tags = Tag::select('name')->whereNull('deleted_at')->get();
        $array = array();
        foreach ($tags as $key => $value) {
            $array [] = $value->name;
        }

        $article = Article::find($id);
        return view('backend.article.edit',compact('categories','array','article'));
    }

    public function showThumbnail($filename)
    {
        $path = Config::get('storage.article_thumbnail');
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
   
    public function update(Request $request, $id)
    {
        if(Session::has('flag')) Session::forget('flag');

        $article_thumbnaile_storage = Config::get('storage.article_thumbnail');
        $article_storage = Config::get('storage.article');

        if (!File::exists($article_thumbnaile_storage)) File::makeDirectory($article_thumbnaile_storage, 0777, true);
        if (!File::exists($article_storage)) File::makeDirectory($article_storage, 0777, true);

        $title = ucwords($request->title);
        $selected_tag = $request->selected_tag;
        
        if($selected_tag){
            $replace = str_replace(","," ",$selected_tag);
            $count = Article::where('tags',$selected_tag)->where('id','!=',$id)->count();
            if($count == 0) $sequence = 1;
            else $sequence = $count+1;
            $slug = Str::slug($replace,'-').'-'.$sequence;
        } else{
            $count = Article::where('title',$title)->where('id','!=',$id)->count();
            if($count == 0) $sequence = 1;
            else $sequence = $count+1;
            $slug = Str::slug($title,'-').'-'.$sequence;
        } 

        $selected_category = $request->selected_category;
        $content = $request->content;
        $image = $request->file('file_upload');
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        $article = Article::find($id);
        $old_image = $article->image;
        $old_tags = $article->tags;

        if($image){
            $old_file_thumbnail = $article_thumbnaile_storage . '/' . $old_image;
            $old_file = $article_storage . '/' . $old_image;
            
            if(File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
            if(File::exists($old_file)) File::delete($old_file);

            $random = Article::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType(); 
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width/$newWidth;
            $newHeight = $height/$diff;
            $image_resize->resize($newWidth, $newHeight);
        }else{
            $_random = $old_image;
        }

        $article->title = $title;
        $article->slug = $slug;
        $article->category_id = $selected_category;
        $article->image = $_random;
        $article->content = $content;
        $article->tags = $selected_tag;
        $article->updated_user_id = Auth::guard('admins')->user()->id;
        $article->save();

        if ($article->save()){
            if($image){
                $image->move($article_storage, $random[1]);
                $image_resize->save($article_thumbnaile_storage.'/'.$random[1]);
            }
           
            if($selected_tag){
                $split_tag = explode(',',$selected_tag);
                foreach ($split_tag as $key => $value) {
                    $tag_slug = Str::slug($value,'-');
                    $is_exists = Tag::where('slug',$tag_slug)->exists();

                    if(!$is_exists){
                        Tag::firstorCreate([
                            'name' => $value,
                            'slug' => $tag_slug,
                            'counter' => 1
                        ]);
                    }
                }
            }else{
               
                $split_tag = explode(',',$old_tags);
                foreach ($split_tag as $key => $value) {
                    $tag_slug = Str::slug($value,'-');
                    $is_exists = Tag::where('slug',$tag_slug)->first();

                    if($is_exists){
                        $counter = $is_exists->counter;
                        $is_exists->counter = $counter-1;
                        $is_exists->save();
                    }
                }
            }
            
        }

        Session::flash('flag', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $image = $article->getFullPath();
        $image_tumbnail = $article->getTumbnailFullPath();
        $tags = $article->tags;
        $article->deleted_at = carbon::now();
        $article->deleted_user_id = Auth::guard('admins')->user()->id;

        if ($article->save()){
            //if(File::exists($image)) @unlink($image);
            //if(File::exists($image_tumbnail)) @unlink($image_tumbnail);

            $split_tag = explode(',',$tags);
            foreach ($split_tag as $key => $value) {
                $tag_slug = Str::slug($value,'-');
                $is_exists = Tag::where('slug',$tag_slug)->first();

                if($is_exists){
                    $counter = $is_exists->counter;
                    $is_exists->counter = $counter-1;
                    $is_exists->save();
                }
            }
        }

        return response()->json(200);
    }

    public function approval()
    {
        $flag = Session::get('flag');
        $active_tab = 'waiting';
        return view('backend.article.approval.index',compact('flag','active_tab'));
    }

    public function approved(Request $request, $id)
    {
        if(Session::has('flag')) Session::forget('flag');

        $is_updated = $request->is_updated;
        if($is_updated == 'edit'){
            $article_thumbnaile_storage = Config::get('storage.article_thumbnail');
            $article_storage = Config::get('storage.article');

            if (!File::exists($article_thumbnaile_storage)) File::makeDirectory($article_thumbnaile_storage, 0777, true);
            if (!File::exists($article_storage)) File::makeDirectory($article_storage, 0777, true);

            $title = ucwords($request->title);
            $selected_tag = $request->selected_tag;
            
            
            if($selected_tag){
                $replace = str_replace(","," ",$selected_tag);
                $count = Article::where('tags',$selected_tag)->where('id','!=',$id)->count();
                if($count == 0) $sequence = 1;
                else $sequence = $count+1;
                $slug = Str::slug($replace,'-').'-'.$sequence;
            } else{
                $count = Article::where('title',$title)->where('id','!=',$id)->count();
                if($count == 0) $sequence = 1;
                else $sequence = $count+1;
                $slug = Str::slug($title,'-').'-'.$sequence;
            } 

            $selected_category = $request->selected_category;
            $content = $request->content;
            $image = $request->file('file_upload');
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $content = $dom->saveHTML();

            $article = Article::find($id);
            $old_image = $article->image;
            $old_tags = $article->tags;

            if($image){
                $old_file_thumbnail = $article_thumbnaile_storage . '/' . $old_image;
                $old_file = $article_storage . '/' . $old_image;
                
                if(File::exists($old_file_thumbnail)) File::delete($old_file_thumbnail);
                if(File::exists($old_file)) File::delete($old_file);

                $random = Article::random($image);
                $_random = $random[1];
                $image_mimetype = $image->getMimeType(); 
                $image_resize = Image::make($image->getRealPath());
                $width = $image_resize->width();
                $height = $image_resize->height();

                $newWidth = 700;
                $diff = $width/$newWidth;
                $newHeight = $height/$diff;
                $image_resize->resize($newWidth, $newHeight);
            }else{
                $_random = $old_image;
            }

            $article->title = $title;
            $article->slug = $slug;
            $article->category_id = $selected_category;
            $article->image = $_random;
            $article->content = $content;
            $article->tags = $selected_tag;
            $article->updated_user_id = Auth::guard('admins')->user()->id;
            $article->save();

            if ($article->save()){
                if($image){
                    $image->move($article_storage, $random[1]);
                    $image_resize->save($article_thumbnaile_storage.'/'.$random[1]);
                }
            
                if($selected_tag){
                    $split_tag = explode(',',$selected_tag);
                    foreach ($split_tag as $key => $value) {
                        $tag_slug = Str::slug($value,'-');
                        $is_exists = Tag::where('slug',$tag_slug)->exists();

                        if(!$is_exists){
                            Tag::firstorCreate([
                                'name' => $value,
                                'slug' => $tag_slug,
                                'counter' => 1
                            ]);
                        }
                    }
                }else{
                
                    $split_tag = explode(',',$old_tags);
                    foreach ($split_tag as $key => $value) {
                        $tag_slug = Str::slug($value,'-');
                        $is_exists = Tag::where('slug',$tag_slug)->first();

                        if($is_exists){
                            $counter = $is_exists->counter;
                            $is_exists->counter = $counter-1;
                            $is_exists->save();
                        }
                    }
                }
                
            }
        }else if($is_updated == 'approved'){
            $article = Article::find($id);
            $article->status = 'approved';
            $article->approval_user_id = Auth::guard('admins')->user()->id;
            $article->approval_date = carbon::now();
            $article->published_at = carbon::now();
            $article->save();

            $followers = db::table('followers')
            ->where([
                 ['followable_id',$article->user->id],
            ])
            ->get();

            foreach ($followers as $key => $follower) {
                Notification::create([
                    'user_id' => $follower->follower_id,
                    'message' => $article->user->name.' ,menambahkan Artikel “<b>'.ucwords($article->title).'</b>”',
                    'call_to_action' => route('detail',$article->slug),
                    'type' => 'new-article',
                ]);
            }
        }
        

        Session::flash('flag', 'success_2');
        return response()->json(200);
    }

    public function show($id,$status){
        $categories = Category::where('is_subcategory','0')->whereNull('deleted_at')->orderby('sequence','asc')->get();
        $tags = Tag::select('name')->whereNull('deleted_at')->get();
        $array = array();
        foreach ($tags as $key => $value) {
            $array [] = $value->name;
        }

        $article = Article::find($id);
        return view('backend.article.approval.show',compact('categories','array','article','status'));
    }

    public function destroyComment($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->deleted_at = carbon::now();
        $comment->deleted_user_id = Auth::guard('admins')->user()->id;
        $comment->save();
        return response()->json(200);
    }

    public function commentData(request $request){
        if(request()->ajax()) {
            $data = db::select(db::raw("select comments.id AS id
            ,comments.content as content
            ,comments.created_at
            from comments 
            where comments.deleted_at is null
            and comments.article_id = '".$request->id."'
            "));
            

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'delete' => route('article.admin.delete.comment',$data->id)
                ]);
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function waitingData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select articles.id AS id
            ,articles.title as title
            ,articles.content as content
            ,articles.tags as tags
            ,categories.name as category_name
            ,articles.status
            ,articles.published_at
            ,articles.created_at
            ,users.name as created_by
            from articles 
            left join users on users.id = articles.user_id 
            left join categories on articles.category_id = categories.id 
            where articles.deleted_at is null
            and articles.status = 'waiting for approve'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('article.admin.show',[$data->id,'waiting'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 40), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-default">Waiting For Approve</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function approvedData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select articles.id AS id
            ,articles.title as title
            ,articles.content as content
            ,articles.tags as tags
            ,categories.name as category_name
            ,articles.status
            ,articles.published_at
            ,articles.created_at
            ,users.name as created_by
            from articles 
            left join categories on articles.category_id = categories.id 
            left join users on articles.user_id = users.id 
            where articles.deleted_at is null
            and articles.status = 'approved'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('article.admin.show',[$data->id,'approved'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 40), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-success">Approved</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function rejectedData(){
        if(request()->ajax()) {
            $data = db::select(db::raw("select articles.id AS id
            ,articles.title as title
            ,articles.content as content
            ,articles.tags as tags
            ,categories.name as category_name
            ,articles.status
            ,articles.published_at
            ,articles.created_at
            ,users.name as created_by
            from articles 
            left join categories on articles.category_id = categories.id 
            left join users on articles.user_id = users.id 
            where articles.deleted_at is null
            and articles.status = 'rejected'
            "));
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('backend._action', [
                    'model' => $data,
                    'detail' => route('article.admin.show',[$data->id,'rejected'])
                ]);
            })
            ->editColumn('content',function($data){
                $_content = strip_tags($data->content);
                $content = substr($_content, 0, strrpos(substr($_content, 0, 100), ' ')) . '...';
                return $content;
            })
            ->editColumn('status',function($data){
                return '<span class="label label-error">Rejected</span>';
            })
            ->rawColumns(['action','status'])
            ->addIndexColumn()
            ->make(true);
        }
    }
}
