<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Config;

class GetMediaController extends Controller
{

  public function avatar($filename)
  {
    $path = Config::get('storage.user_avatar');

    $resp = response()->download($path . '/' . $filename);
    $resp->headers->set('Content-Disposition', 'inline');
    $resp->headers->set('X-Content-Type-Options', 'nosniff');

    //disable cache
    $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
    $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
    $resp->headers->set('Expires', '0'); // Proxies.

    return $resp;
  }

  public function advertisement($filename)
  {
    $path = Config::get('storage.advertisement');

    $resp = response()->download($path . '/' . $filename);
    $resp->headers->set('Content-Disposition', 'inline');
    $resp->headers->set('X-Content-Type-Options', 'nosniff');

    //disable cache
    $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
    $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
    $resp->headers->set('Expires', '0'); // Proxies.

    return $resp;
  }

  public function featureImage($filename)
  {
    $path = Config::get('storage.category');

    $resp = response()->download($path . '/' . $filename);
    $resp->headers->set('Content-Disposition', 'inline');
    $resp->headers->set('X-Content-Type-Options', 'nosniff');

    //disable cache
    $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
    $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
    $resp->headers->set('Expires', '0'); // Proxies.

    return $resp;
  }
}
