<?php

namespace App\Http\Controllers\FrontEnd;

use DB;
use File;
use Auth;
use Mail;
use Config;
use SEO\Seo;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Tag;
use App\Models\User;
use App\Models\Article;
use App\Models\Category;
use App\Models\Notification;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.user', ['except' => [
            'activation',
            'showArticleThumbnail',
            'showAvatar'
        ]]);
    }

    public function index($url)
    {
        $profile = User::where('url', $url)->first();
        $notification_comments = Notification::select('message', 'call_to_action', db::raw('count(0) as total'))
            ->where([
                ['user_id', Auth::guard('web')->user()->id],
                ['type', 'comment'],
            ])
            ->whereNull('is_read')
            ->groupby('message', 'call_to_action')
            ->get();

        $notification_followers = Notification::where([
            ['user_id', Auth::guard('web')->user()->id],
        ])
            ->whereIn('type', ['user-follow', 'view', 'new-article'])
            ->whereNull('is_read')
            ->get();

        return view('frontend.account.notifikasi', ['login' => 'loggedin', 'nav' => 'notifikasi'], compact('profile', 'notification_followers', 'notification_comments'));
    }

    public function event($url)
    {
        $profile = User::where('url', $url)->first();
        return view('frontend.account.event', ['nav' => 'event'], compact('profile'));
    }

    public function createEvent($url)
    {
        $profile = User::where('url', $url)->first();
        return view('frontend.account.create-event', compact('profile'));
    }

    public function setting($url)
    {
        $profile = User::where('url', $url)->first();
        return view('frontend.account.pengaturan', ['login' => 'loggedin', 'nav' => 'pengaturan'], compact('profile', 'url'));
    }


    public function update(Request $request, $url, $id)
    {
        $avatar_storage = Config::get('storage.user_avatar');

        if (!File::exists($avatar_storage)) File::makeDirectory($avatar_storage, 0777, true);

        $user = User::find($id);
        $name = $request->name;
        $phone = $request->phone;
        $old_password = $request->old_password;
        $password = $request->password;
        $retype_password = $request->retype_password;
        $url = $request->url;
        $description = $request->description;
        $facebook_url = $request->facebook_url;
        $youtube_url = $request->youtube_url;
        $twitter_url = $request->twitter_url;
        $instagram_url = $request->instagram_url;

        /*if(!$old_password) return redirect()->back()
        ->withErrors([
            'old_password' => 'Silahkan masukan password lama anda terlebih dahulu',
        ]);*/

        /*
        echo bcrypt('password1').' <br/>'.$user->password;die();
        if( bcrypt($old_password) != $user->password) return redirect()->back()
        ->withErrors([
            'old_password' => 'Password lama anda salah',
        ]);*/

        if ($password != $retype_password) return redirect()->back()
            ->withErrors([
                'retype_password' => 'Password yang anda ketikan idak sama dengan password baru',
            ]);

        if (!$url) return redirect()->back()
            ->withErrors([
                'url' => 'Silahkan masukan url terlebih dahulu',
            ]);

        if (User::where('url', Str::slug($url, '-'))->where('id', '!=', $id)->whereNull('deleted_at')->exists()) return redirect()->back()
            ->withErrors([
                'url' => 'url sudah digunakan, silahkan cari nama lainnya',
            ]);

        $image = $user->avatar;
        $now = Carbon::now();
        if ($request->hasFile('avatar')) {
            if ($request->file('avatar')->isValid()) {
                $file = $request->file('avatar');

                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now->timestamp . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 300;
                $diff = $width / $newWidth;
                $newHeight = $height / $diff;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save(storage_path('app/user_avatar/' . $image));
            }
        }
        $user->name = $name;
        $user->url = Str::slug($url, '-');
        $user->phone = $phone;
        if ($password) $user->password = bcrypt($password);

        $user->description = $description;
        $user->facebook_url = $facebook_url;
        $user->youtube_url = $youtube_url;
        $user->twitter_url = $twitter_url;
        $user->instagram_url = $instagram_url;
        $user->avatar = $image;
        $user->save();

        return redirect()->route('myaccount.pengaturan', $user->url); //->withSuccess();


    }


    public function activation($id, $hash)
    {
        $user = User::find($id);
        if (!$user || $user->email_verified_at != '' || !$user->checkConfirmHash($hash)) {
            if (Auth::guard('web')->check())
                return redirect('/')
                    ->with('error_code', 5)
                    ->withErrors([
                        'info' => 'User anda belum aktif, silahkan cek email anda terlebih dahulu',
                    ]);
            else
                return redirect('/')
                    ->with('error_code', 5)
                    ->withErrors([
                        'info' => 'User anda belum aktif, silahkan cek email anda terlebih dahulu',
                    ]);;
        }

        $user->email_verified_at = carbon::now();
        $user->save();

        Mail::send('frontend.email.account_welcome', ['user' => $user], function ($message) use ($user) {
            $message->to($user->email, $user->name)->subject("Welcome!");
        });

        if (Auth::guard('web')->check())
            return redirect()->action('AccountController@index')->withNotification(true)->withSuccess('Activation Account Success');
        else
            return redirect('/')
                ->with('error_code', 5)
                ->withErrors([
                    'info' => 'Activation Account Success',
                ]);;
    }

    public function writeArticle($url)
    {
        $profile = User::where('url', $url)->first();
        $categories = Category::where('is_subcategory', '0')->whereNull('deleted_at')->orderby('sequence', 'asc')->get();
        $tags = Tag::select('name')->whereNull('deleted_at')->get();
        $array = array();
        foreach ($tags as $key => $value) {
            $array[] = $value->name;
        }
        return view('frontend.account.tulis', ['login' => 'loggedin'], compact('categories', 'array', 'profile'));
    }

    public function follow($id, $status)
    {
        $user_follow = User::find($id);
        $user = User::find(Auth::guard('web')->user()->id);

        if ($status == 'Follow') {
            $is_exists = db::table('followers')
                ->where([
                    ['follower_id', $user->id],
                    ['followable_id', $user_follow->id],
                ])
                ->exists();

            if ($is_exists) return redirect()->back()
                ->withErrors([
                    'follow' => 'This User has been followed',
                ]);


            $user->follow($user_follow);
            $total_following = $user->following()->count(); // 1
            $total_followers = $user_follow->followers()->count(); // 1

            $user_follow->total_followers = $total_followers;
            $user_follow->save();

            $user->total_following = $total_following;
            $user->save();

            Notification::create([
                'user_id' => $user->id,
                'message' => 'Anda telah mengikuti ' . $user_follow->name,
                'call_to_action' => route('profile', $user_follow->url),
                'type' => 'user-follow',
            ]);

            Notification::create([
                'user_id' => $user_follow->id,
                'message' => 'Anda telah diikuti ' . $user->name,
                'call_to_action' => route('profile', $user->url),
                'type' => 'user-follow',
            ]);
        } else {
            db::table('followers')
                ->where([
                    ['follower_id', $user->id],
                    ['followable_id', $user_follow->id],
                ])
                ->delete();

            $total_following = $user->following()->count();
            $total_followers = $user_follow->followers()->count();

            $user_follow->total_followers = $total_followers;
            $user_follow->save();

            $user->total_following = $total_following;
            $user->save();
        }

        return redirect()->back();
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.user');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function notification($url)
    {
        $profile = User::where('url', $url)->first();

        $notification_comments = Notification::select('message', 'call_to_action', db::raw('count(0) as total'))
            ->where([
                ['user_id', Auth::guard('web')->user()->id],
                ['type', 'comment'],
            ])
            ->whereNull('is_read')
            ->groupby('message', 'call_to_action')
            ->get();

        $notification_followers = Notification::where([
            ['user_id', Auth::guard('web')->user()->id],
        ])
            ->whereIn('type', ['user-follow', 'view', 'new-article'])
            ->whereNull('is_read')
            ->get();
        return view('frontend.account.notifikasi', ['login' => 'loggedin', 'nav' => 'notifikasi'], compact('notification_followers', 'notification_comments', 'profile'));
    }

    public function artikel($url)
    {
        $profile = User::where('url', $url)->first();
        $articles = Article::where('user_id', $profile->id)
            // ->where('status', 'approved')
            ->orderby('created_at', 'desc')
            ->get();

        return view('frontend.account.artikel', ['login' => 'loggedin', 'nav' => 'artikel'], compact('profile', 'articles'));
    }

    public function draft($url)
    {
        $profile = User::where('url', $url)->first();
        $articles = Article::where('user_id', $profile->id)
            ->where('status', 'draft')
            ->orderby('created_at', 'desc')
            ->get();

        return view('frontend.account.draft', ['login' => 'loggedin', 'nav' => 'draft'], compact('profile', 'articles'));
    }


    public function updateNotification($username, $id, $type, request $request)
    {
        if ($type == 'user-follow' || $type == 'view' || $type == 'new-article') {
            $notification = Notification::find($id);
            $call_to_action = $notification->call_to_action;
            $notification->is_read = true;
            $notification->save();
        } else if ($type == 'comment') {
            $profile = User::where('url', $username)->first();
            $message = $request->message;
            $limit = $request->total;

            $notifications = Notification::where([
                ['message', $message],
                ['user_id', $profile->id],
            ])
                ->whereNull('is_read')
                ->limit($limit)
                ->get();

            $call_to_action = '';
            foreach ($notifications as $key => $notification) {
                $call_to_action = $notification->call_to_action;
                $notification->is_read = true;
                $notification->save();
            }
        }

        return redirect($call_to_action);
    }

    public function showArticleThumbnail($username, $filename)
    {
        $path = Config::get('storage.article_thumbnail');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function store(Request $request, $username)
    {
        if (Session::has('flag')) Session::forget('flag');
        $article_thumbnaile_storage = Config::get('storage.article_thumbnail');
        $article_storage = Config::get('storage.article');

        if (!File::exists($article_thumbnaile_storage)) File::makeDirectory($article_thumbnaile_storage, 0777, true);
        if (!File::exists($article_storage)) File::makeDirectory($article_storage, 0777, true);

        $profile = User::where('url', $username)->first();

        $title = ucwords($request->title);
        $selected_tag = $request->selected_tag;

        if ($selected_tag) {
            $replace = str_replace(",", " ", $selected_tag);
            $count = Article::where('tags', $selected_tag)->count();
            if ($count == 0) $sequence = 1;
            else $sequence = $count + 1;
            $slug = Str::slug($replace, '-') . '-' . $sequence;
        } else {
            $count = Article::where('title', $title)->count();
            if ($count == 0) $sequence = 1;
            else $sequence = $count + 1;
            $slug = Str::slug($title, '-') . '-' . $sequence;
        }

        $selected_category = $request->selected_category;
        $content = $request->content;
        $image = $request->file('file_upload');
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if ($image) {
            $random = Article::random($image);
            $_random = $random[1];
            $image_mimetype = $image->getMimeType();
            $image_resize = Image::make($image->getRealPath());
            $width = $image_resize->width();
            $height = $image_resize->height();

            $newWidth = 700;
            $diff = $width / $newWidth;
            $newHeight = $height / $diff;
            $image_resize->resize($newWidth, $newHeight);
        } else {
            $_random = null;
        }

        $article = Article::firstorCreate([
            'title' => $title,
            'slug' => $slug,
            'category_id' => $selected_category,
            'user_id' => $profile->id,
            'image' => $_random,
            'slug' => $slug,
            'content' => $content,
            'status' => 'waiting for approve',
            'tags' => $selected_tag,

        ]);

        if ($article->save()) 
        {
            Seo::save($article, route('detail', $article->slug),
            [
                'title'     => $article->title,
                'tags'      => $selected_tag
            ], 
            [ 
                'title'     => $article->title,
                'tags'      => $selected_tag
                , 'images'  => [ $article->getFullPath() ] 
            ]); 
            
            if ($image) {
                $image->move($article_storage, $random[1]);
                $image_resize->save($article_thumbnaile_storage . '/' . $random[1]);
            }

            if ($selected_tag) {
                $split_tag = explode(',', $selected_tag);
                foreach ($split_tag as $key => $value) {
                    $tag_slug = Str::slug($value, '-');
                    $is_exists = Tag::where('slug', $tag_slug)->first();

                    if ($is_exists) {
                        $counter = $is_exists->counter;
                        $is_exists->counter = $counter + 1;
                        $is_exists->save();
                    } else {
                        Tag::firstorCreate([
                            'name' => $value,
                            'slug' => $tag_slug,
                            'counter' => 1
                        ]);
                    }
                }
            }
        }

        return redirect()->route('myaccount.artikel', $username);
    }
}
