<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use App\Models\User;
use stdClass;

class ArticleController extends Controller
{
  public function loadMore(Request $request)
  {
    $url = $request->author;
    $id = $request->id;
    $user = User::where('url', $url)->first();

    if (!$user) {
      return null;
    }
    $output = '';
    $nextId = null;
    $articles = $user->article()->where('id', '>', $id)->limit(4)->get();
    if (!$articles->isEmpty()) {
      foreach ($articles as $key => $article) {
        # code...
        $img = asset('images/default-image.png');
        if ($article->image) {
          $img = route(
            'myaccount.artikel.showTh umbnail',
            [$article->user->url, $article->image]
          );
        }
        $urlCategory = route('category', $article->category->slug);
        $category = $article->category->name;
        $urlArticle = route('detail', $article->slug);
        $createdAt = $article->created_at->format('d/m/Y');
        $urlProfile = route('profile', $article->user->url);
        $author = $article->user->last_name;

        $output .= '
          <div class="post-item post-vertical">
            <div class="post-item-image">
                <img src="' . $img . '" alt="">
            </div>
            <div class="post-item-content">
              <ul class="post-item-categories">
                <li>
                  <a href="' . $urlCategory . '">
                    ' . $category . '
                  </a>
                </li>
              </ul>
              <h4 class="post-item-heading">
                <a href="' . $urlArticle . '">
                  ' . $article->title . '
                </a>
              </h4>
              <p>' . $article->excerpt . '</p>
              <div class="post-item-footer pt-3 mt-auto">
                <span>' . $createdAt . '</span>
                <span class="font-italic">
                  &nbsp;&nbsp;&nbsp;- by <a href="' . $urlProfile . '">
                    ' . $author . '
                  </a>
                </span>
              </div>
            </div>
          </div>
        ';
      }
      // echo $output;
      $nextId = $article->id;
    }
    $data = new stdClass();
    $data->el = $output;
    $data->nextId = $nextId;
    return json_encode($data);
  }
}
