<?php namespace App\Http\Controllers\Frontend\Auth;

use Mail;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use App\Models\User;


class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = '/';
    
    public function __construct()
    {
        $this->middleware('guest.user', ['except' => 'logout']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function create(request $request)
    {
        $username = Str::slug($request->username,'-');
        $is_exists = User::where('url',$username)->whereNull('deleted_at')->exists();
        $is_email_exists = User::where('email',$request->email)->exists();

        if($is_exists){
            if($request->role == 'user'){
                return redirect()->back()
                ->with('error_code', 4)
                ->withInput()
                ->withErrors([
                    'info' => 'Username sudah digunakan',
                ]);
            }else{
                return redirect()->back()
                ->with('error_code', 3)
                ->withInput()
                ->withErrors([
                    'info' => 'Username sudah digunakan',
                ]);
            }
        }

        if($is_email_exists){
            if($request->role == 'user'){
                return redirect()->back()
                ->with('error_code', 4)
                ->withInput()
                ->withErrors([
                    'info' => 'Email sudah digunakan',
                ]);
            }else{
                return redirect()->back()
                ->with('error_code', 3)
                ->withInput()
                ->withErrors([
                    'info' => 'Email sudah digunakan',
                ]);
            }
        }

        $user = User::create([
            'name' => ucwords($request->name),
            'email' => $request->email,
            'role' => $request->role,
            'url' => $username,
            'email_verified_at' => carbon::now(),
            'password' => Hash::make($request->password),
        ]);

        if($user->save()){
            if($request->role == 'user'){
                Mail::send('frontend.email.account_confirmation',['user' => $user],function($message) use ($user){
                    $message->to($user->email,$user->name)->subject('[Inspirasi Hijab] Confirmation Account');
                });

                $success_msg = 'User anda berhasil dibuat, silahkan cek email anda terlebih dahulu untuk aktifasi';
            }else{
                $success_msg = 'User anda berhasil dibuat, tinggal menunggu admin untuk melakukan aktifasi terhadap akun anda';
            }
            
        }

        return redirect('/')
        ->with('error_code', 5)
        ->withErrors([
            'info' => $success_msg,
        ]);
    }
}
