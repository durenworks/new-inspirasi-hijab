<?php namespace App\Http\Controllers\Frontend\Auth;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest.user', ['except' => 'logout']);
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function showLoginForm()
    {
        return view('frontend.login');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

       $credentials = $this->credentials($request);
		if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();

            if ($user->deleted_at == null && $user->email_verified_at !=null) {
                $update_user = User::find($user->id);
                $update_user->last_login = carbon::now();
                $update_user->save();

                Auth::guard('web')->login($user, $request->has('remember'));
                if($update_user->role == 'user') return redirect()->intended('/account/'.$update_user->url.'/profile');
                else if($update_user->role == 'community') return redirect()->intended('/account/'.$update_user->url.'/community/profile');

            } else {
                if($user->deleted_at){
                    return redirect()->back()
                    ->with('error_code', 5)
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        'info' => 'User sudah tidak aktif lagi',
                    ]);

                }else if($user->email_verified_at == ''){
                    return redirect()->back()
                    ->with('error_code', 5)
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        'info' => 'User sudah belum aktif, silahkan cek email anda terlebih dahulu',
                    ]);
                }
            }
        }

        //return redirect()->back()
        return redirect()->back()
        ->with('error_code', 5)
        ->withInput($request->only('email', 'remember'))
        ->withErrors([
            'email' => 'Incorrect Email or password',
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
