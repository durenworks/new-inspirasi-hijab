<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use File;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Event;
use App\Models\Category;

use CyrildeWit\EloquentViewable\Support\Period;

class PageController extends Controller
{
  //    public function articleDetail($slug)
  //    {
  //      $article = Article::where('slug', $slug)
  //                ->first();
  //
  //      views($article)
  //        ->delayInSession(30)
  //        ->record();
  //
  //      return views($article)
  //          ->count();
  //    }

  public function homepage()
  {
    $slider = Article::where('status', 'approved')->inRandomOrder()->paginate(4);
    $articles = Article::where('status', 'approved')->orderBy('approval_date', 'desc')->paginate(4);
    $users = User::orderBy('total_followers', 'desc')->limit(10)->get();
    $communities = User::where('role', 'community')->orderBy('total_followers', 'desc')->limit(10)->get();

    $categoryId = Article::select('category_id')->groupBy('category_id')->inRandomOrder()->first();
    $category = null;
    $categoryArticles = null;
    if (!empty($categoryId)) {
      if ($categoryId->category_id) {
        $category = Category::find($categoryId->category_id);
        $categoryArticles = $category->articles()->where('status', 'approved')->orderBy('published_at', 'desc')->limit(6)->get();
      }
    }


    return view('frontend.homepage', compact('articles', 'slider', 'users', 'communities', 'category', 'categoryArticles'));
  }

  public function detail($slug)
  {
    if ($slug == 'admin-inspirasi-hijab') return redirect()->route('backend.home');
    $expiresAt = Carbon::now();

    $article = Article::where('slug', $slug)->first();
    if (!$article) {
      return abort(404);
    }

    if (views($article)->count() == 0) {
      views($article)
        ->record();
      Notification::create([
        'user_id' => $article->user->id,
        'message' => 'Artikel “<b>' . ucwords($article->title) . '</b>” Telah Di Baca <span class="badge badge-danger ml-2 rounded-circle">' . views($article)->count() . '</span>',
        'call_to_action' => route('detail', $article->slug),
        'type' => 'view',
      ]);
    } else {
      $last_view = db::table('views')
        ->where([
          ['viewable_id', $article->id]
        ])
        ->orderby('viewed_at', 'desc')
        ->first();

      $diff = $expiresAt->diffInMinutes($last_view->viewed_at);
      if ($diff >= 30) {
        views($article)
          ->record();

        $is_exists = Notification::where([
          ['call_to_action', route('detail', $article->slug)],
          ['user_id', $article->user->id],
          ['type', 'view'],
        ])
          ->whereNull('is_read')
          ->first();

        if ($is_exists) {
          $is_exists->message = 'Artikel “<b>' . ucwords($article->title) . '</b>” Telah Di Baca <span class="badge badge-danger ml-2 rounded-circle">' . views($article)->count() . '</span>';
          $is_exists->save();
        } else {
          Notification::create([
            'user_id' => $article->user->id,
            'message' => 'Artikel “<b>' . ucwords($article->title) . '</b>” Telah Di Baca <span class="badge badge-danger ml-2 rounded-circle">' . views($article)->count() . '</span>',
            'call_to_action' => route('detail', $article->slug),
            'type' => 'view',
          ]);
        }
      }
    }

    $article->total_views =  views($article)->count();
    $article->save();

    $category = $article->category;
    $category = $category->parent;
    $ambasador = $category->userAmbasador;

    return view('frontend.detail', compact('article', 'category', 'ambasador'));
  }

  public function events()
  {
    $slider = Event::paginate(4);
    $events = Event::paginate(10);
    $users = User::orderBy('total_followers', 'desc')->limit(10)->get();

    $categoryId = Article::select('category_id')->groupBy('category_id')->inRandomOrder()->first();
    $category = null;
    $categoryArticles = null;
    if (!empty($categoryId)) {
      if ($categoryId->category_id) {
        $category = Category::find($categoryId->category_id);
        $categoryArticles = $category->articles()->where('status', 'approved')->orderBy('published_at', 'desc')->limit(6)->get();
      }
    }

    return view('frontend.event', compact('events', 'slider', 'users', 'category', 'categoryArticles'));
  }

  public function event($slug)
  {
    $event = Event::where('slug', $slug)
      ->first();
    return view('frontend.detail-event', compact('event'));
  }

  public function profile($url)
  {
    $profile = User::where('url', $url)->first();

    return view('frontend.account.profile', compact('profile'));
  }

  public function penulis()
  {
    $auth = Auth::guard('web')->user();

    $users = User::where('role', 'user')->whereNull('deleted_at');
    if ($auth) $users = $users->where('url', '!=', $auth->url);
    $users = $users->get();

    return view('frontend.penulis', ['navbar' => 'penulis'], compact('users'));
  }

  public function faq()
  {
    return view('frontend.faq');
  }

  public function tos()
  {
    return view('frontend.tos');
  }

  public function search(Request $request)
  {
    $query = urldecode($request->s);

    $articles = Article::where('status', 'approved')
      ->where('title', 'LIKE', "%$query%")
      ->orWhere('content', 'LIKE', "%$query%")
      ->paginate(4);
    $users = User::orderBy('total_followers', 'desc')->limit(10)->get();
    $hitsArticles = null;
    if ($articles->count() === 0) {
      $hitsArticles = Article::where('status', 'approved')->orderByUniqueViews('asc', Period::pastDays(7))->paginate(4);
    }

    return view('frontend.search', compact('articles', 'users', 'hitsArticles', 'query'));
  }

  public function showAvatar($filename)
  {
    $user_avatar = Config::get('storage.user_avatar');
    if (!File::exists($user_avatar)) File::makeDirectory($user_avatar, 0777, true);

    $path = Config::get('storage.user');
    $resp = response()->download($path . '/' . $filename);
    $resp->headers->set('Content-Disposition', 'inline');
    $resp->headers->set('X-Content-Type-Options', 'nosniff');

    return $resp;
  }

  public function postComment(request $request, $slug, $id)
  {
    $article = Article::find($id);
    Comment::create([
      'article_id' => $id,
      'content' => $request->comments_text
    ]);

    Notification::create([
      'user_id' => $article->user->id,
      'message' => 'Artikel “<b>' . ucwords($article->title) . '</b> …” Telah Di komentari',
      'call_to_action' => route('detail', $article->slug),
      'type' => 'comment',
    ]);

    return redirect()->back();
  }

  public function category($slug)
  {
    $category = Category::where('slug', $slug)->first();
    if (!$category) {
      return redirect()->route('homepage');
    }
    $articles = $category->articles()->where('status', 'approved')->orderBy('approval_date', 'desc')->paginate(4);
    $slider = $category->articles()->where('status', 'approved')->inRandomOrder()->limit(4);
    $users = User::orderBy('total_followers', 'desc')->limit(10)->get();
    $hitsArticles = null;
    if ($articles->count() === 0) {
      $hitsArticles = Article::where('status', 'approved')->orderByUniqueViews('asc', Period::pastDays(7))->paginate(4);
    }

    return view('frontend.category', compact('articles', 'slider', 'users', 'hitsArticles'));
  }

  public function community()
  {
    $communities = User::where('role', 'community')->orderBy('total_followers', 'desc')->get();

    return view('frontend.community', ['navbar' => 'community'], compact('communities'));
  }
}
