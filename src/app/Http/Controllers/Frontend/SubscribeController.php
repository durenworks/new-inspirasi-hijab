<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Config;
use Illuminate\Http\Request;
use App\Models\Subscribers;

class SubscribeController extends Controller
{

  public function subscribe(Request $request)
  {
    $subscribe = new Subscribers();
    $subscribe->fill([
      'email' => $request->email
    ]);
    $subscribe->save();

    return redirect()->back();
  }
}
