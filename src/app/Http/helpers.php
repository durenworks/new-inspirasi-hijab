<?php

/**
 * Created by PhpStorm.
 * User: Dadittya
 * Date: 19/05/2019
 * Time: 15:24
 */

use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Arr;
use App\Models\Article;
use App\Models\Advertisement;
use App\Models\Setting;

if (!function_exists('indonesianDate')) {

  function indonesianDate($date, $day_name = true)
  {
    $dt = new  \Carbon\Carbon($date);
    setlocale(LC_TIME, 'IND');

    if ($day_name) {
      return $dt->formatLocalized('%A, %e %B %Y'); // Senin, 3 September 2018
    } else {
      return $dt->formatLocalized('%e %B %Y %H:%I:%S'); // 3 September 2018
    }
  }
}

if (!function_exists('upload_image')) {
  function upload_image($files, $location, $oldfile = null)
  {
    $upload_path = Config::get('storage.' . $location);


    //var_dump(Config::get('storage.'.$location)); die();
    //$file_name = $files->getClientOriginalName();
    $extension = $files->getClientOriginalExtension();

    // rename image name if duplicate
    /*while ($model::where($field, $file_name)->first() != null) {
        $file_name = rand(11111,99999).'.'.$extension;
    }*/
    $ret = new StdClass();
    $try = 250;
    do {
      if ($try <= 0)
        throw Exception("Storage::random fails to produce randomized filename");
      $hash = str_random(32);
      if ($extension)
        $hash = $hash . '.' . $extension;
      $file = $upload_path . '/' . $hash;
      $try -= 1;
    } while (file_exists($file));
    //$ret = array($upload_path, $hash, $file);

    //$arrImage = $file_name;
    $files->move($upload_path, $hash);
    if ($oldfile) {
      $full_path = $upload_path . '/' . $oldfile;
      File::delete($full_path);
    }

    return $hash;
  }
}

if (!function_exists('getEnumValues')) {
  function getEnumValues($table, $column)
  {
    $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type;
    preg_match('/^enum\((.*)\)$/', $type, $matches);
    $enum = array();
    foreach (explode(',', $matches[1]) as $value) {
      $v = trim($value, "'");
      $enum = Arr::add($enum, $v, ucfirst($v));
    }
    return $enum;
  }
}

if (!function_exists('extractSummernoteContent')) {
  function extractSummernoteContent($input)
  {
    $dom = new \DomDocument();
    $internalErrors = libxml_use_internal_errors(true);
    $dom->loadHtml(mb_convert_encoding($input, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    libxml_use_internal_errors($internalErrors);
    $images = $dom->getElementsByTagName('img');

    //var_dump($images);die();

    foreach ($images as $img) {
      $src = $img->getAttribute('src');
      if (preg_match('/data:image/', $src)) {
        // get the mimetype
        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
        $mimetype = $groups['mime'];
        $filename = uniqid();
        $filepath = "/images/summernote/" . $filename . '.' . $mimetype;

        $image = Image::make($src)
          ->encode($mimetype, 100)
          ->save(public_path($filepath));

        $new_src = asset($filepath);
        $img->removeAttribute('src');
        $img->setAttribute('src', $new_src);
      }
    }

    return $dom->saveHTML();
  }
}

if (!function_exists('popularArticle')) {
  function popularArticle()
  {
    $articles = Article::where('published_at', '!=', null)->orderBy('total_views', 'desc')->limit(4)->get();
    return $articles;
  }
}

if (!function_exists('getAds')) {
  function getAds()
  {
    $ads = Advertisement::inRandomOrder()->first();
    return $ads;
  }
}

if (!function_exists('getSettingValue')) {
    function getSettingValue($name)
    {
        $setting = Setting::where('name', $name)->first();
        if ($setting) {
            return $setting->value;
        }
        else {
            return '#';
        }
    }
}
