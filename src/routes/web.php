<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\PageController@homepage')->name('homepage');
Route::get('/{slug}', 'Frontend\PageController@detail')->name('detail');
Route::post('/{slug}/{article}/comment', 'Frontend\PageController@postComment')->name('postComment');
Route::get('/page/faq', 'Frontend\PageController@faq')->name('faq');
Route::get('/page/tos', 'Frontend\PageController@tos')->name('tos');
Route::get('/profile/{url}', 'Frontend\PageController@profile')->name('profile');
Route::get('/page/penulis', 'Frontend\PageController@penulis')->name('penulis');
Route::get('/page/penulis/{avatar}/user-avatar', 'Frontend\PageController@showAvatar')->name('profile.avatar');
Route::get('/page/events', 'Frontend\PageController@events')->name('event');
Route::get('/page/event/{event?}', 'Frontend\PageController@event')->name('event.detail');
Route::get('/category/{slugSubcategory}', 'Frontend\PageController@category')->name('category');
Route::get('/page/lifestyle', function () {
    return view('frontend.lifestyle', ['navbar' => 'lifestyle']);
})->name('lifestyle');
Route::get('/page/search', 'Frontend\PageController@search')->name('search');

Route::get('/page/community', 'Frontend\PageController@community')->name('community');

Route::get('/page/followers', function () {
    return view('frontend.account.followers', ['login' => 'loggedin', 'navbar' => '']);
})->name('followers');

Route::get('/page/inspirasi', function () {
    return view('frontend.homepage', ['navbar' => 'inspirasi']);
})->name('inspirasi');

Route::post('/subscribe', 'Frontend\SubscribeController@subscribe')->name('subscribe');
Route::post('/article/load_more', 'Frontend\ArticleController@loadMore')->name('article.load_more');


// Route::get('/profile', function() {
//   return view('frontend.account.profile', ['login' => 'loggedin', 'nav' => 'notifikasi']);
// })->name('profile');

//Route::get('/{slug}', 'Frontend\PageController@articleDetail')->name('article.detail');

Route::group(['prefix' => 'account', 'namepsace' => 'Frontend'], function () {
    Route::get('/login', 'Frontend\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Frontend\Auth\LoginController@login')->name('do_login');
    Route::post('/logout', 'Frontend\Auth\LoginController@logout')->name('do_logout');

    Route::get('/{username}/tulis', 'Frontend\AccountController@writeArticle')->name('myaccount.tulis');
    Route::get('/{username}/profile', 'Frontend\AccountController@index')->name('myaccount');
    Route::get('/{username}/community/profile', 'Frontend\AccountController@index')->name('community.account');
    Route::get('/{username}/community/event', 'Frontend\AccountController@event')->name('community.event');
    Route::get('/{username}/community/event/create', 'Frontend\AccountController@createEvent')->name('community.event.create');
    Route::get('/{username}/profile-setting', 'Frontend\AccountController@setting')->name('myaccount.pengaturan');
    Route::get('/{username}/notifikasi', 'Frontend\AccountController@notification')->name('myaccount.notifikasi');
    Route::get('/{username}/artikel', 'Frontend\AccountController@artikel')->name('myaccount.artikel');
    Route::get('/{username}/draft', 'Frontend\AccountController@draft')->name('myaccount.draft');
    Route::get('/{username}/{image}/article-image', 'Frontend\AccountController@showArticleThumbnail')->name('myaccount.artikel.showThumbnail');

    Route::post('/{username}/{id}/{type}/notifikasi', 'Frontend\AccountController@updateNotification')->name('myaccount.updateNotifikasi');
    Route::put('/{username}/{id}/profile-setting', 'Frontend\AccountController@update')->name('myaccount.update.pengaturan');
    Route::post('/follow/{id}/{status}', 'Frontend\AccountController@follow')->name('myaccount.follow');
    Route::post('/{username}/artikel', 'Frontend\AccountController@store')->name('myaccount.article.store');


    Route::get('/register', function () {
        return view('auth.register');
    })->name('register');

    Route::post('/register', 'Frontend\Auth\RegisterController@create')->name('do_register');
    Route::get('/activation/{id}/{hash}', 'Frontend\AccountController@activation')->name('activation');
});

//Route for get Media Image
Route::get('media/avatar/{filename}', 'Frontend\GetMediaController@avatar')->name('media.avatar');
Route::get('media/advertisement/{filename}', 'Frontend\GetMediaController@advertisement')->name('media.advertisement');
Route::get('media/feature_image/{filename}', 'Frontend\GetMediaController@featureImage')->name('media.feature_image');

Route::group(['prefix' => 'admin-inspirasi-hijab', 'namespace' => 'Backend'], function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('backend.login');
    Route::get('/dashboard', 'HomeController@index')->name('backend.home');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('backend.forgot_password');
    Route::get('/password/reset/{id}/{date}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('backend.resetpassword');

    Route::post('/login', 'Auth\LoginController@login')->name('backend.do_login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('backend.do_logout');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('backend.sendlink');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('backend.reset');

    Route::get('/user', 'UserController@index')->name('user.admin.index');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('user.admin.edit');

    Route::post('/user/store', 'UserController@store')->name('user.admin.store');
    Route::put('/user/{id}/edit', 'UserController@update')->name('user.admin.update');
    Route::put('/user/{id}/delete', 'UserController@destroy')->name('user.admin.delete');

    Route::get('/user/approval', 'UserController@approval')->name('user.admin.approval');
    Route::get('/user/{id}/show', 'UserController@show')->name('user.admin.show');
    Route::get('/user/approval/waiting-for-approve', 'UserController@waitingData');

    Route::post('/user/approved', 'UserController@approved')->name('community.admin.approved');

    Route::get('/community', 'CommunityController@index')->name('community.admin.index');
    Route::get('/community/{id}/edit', 'CommunityController@edit')->name('community.admin.edit');

    Route::post('/community/store', 'CommunityController@store')->name('community.admin.store');
    Route::put('/community/{id}/edit', 'CommunityController@update')->name('community.admin.update');
    Route::put('/community/{id}/delete', 'CommunityController@destroy')->name('community.admin.delete');

    Route::get('/community/approval', 'CommunityController@approval')->name('community.admin.approval');
    Route::get('/community/{id}/show', 'CommunityController@show')->name('community.admin.show');
    Route::get('/community/approval/waiting-for-approve', 'CommunityController@waitingData');

    Route::post('/community/approved', 'CommunityController@approved')->name('community.admin.approved');

    Route::get('/category', 'CategoryController@index')->name('category.index');
    Route::get('/category/{id}/edit', 'CategoryController@edit')->name('category.edit');
    Route::get('/category/{filename}/image-thumbnail', 'CategoryController@showThumbnail')->name('category.admin.showThumbnail');
    Route::get('/category/{filename}/image', 'CategoryController@showImage')->name('category.admin.showImage');

    Route::post('/category/store', 'CategoryController@store')->name('category.store');
    Route::post('/category/{id}/edit', 'CategoryController@update')->name('category.update');
    Route::put('/category/{id}/delete', 'CategoryController@destroy')->name('category.delete');

    Route::get('/advertisement', 'AdvertisementController@index')->name('advertisement.index');
    Route::get('/advertisement/{id}/edit', 'AdvertisementController@edit')->name('advertisement.edit');
    Route::get('/advertisement/{filename}/image-thumbnail', 'AdvertisementController@showThumbnail')->name('advertisement.showThumbnail');
    Route::get('/advertisement/{filename}/image', 'AdvertisementController@showImage')->name('advertisement.showImage');

    Route::post('/advertisement/store', 'AdvertisementController@store')->name('advertisement.store');
    Route::post('/advertisement/{id}/edit', 'AdvertisementController@update')->name('advertisement.update');
    Route::delete('/advertisement/{id}/delete', 'AdvertisementController@destroy')->name('advertisement.delete');

    Route::get('/article', 'ArticleController@index')->name('article.admin.index');
    Route::get('/article/create', 'ArticleController@create')->name('article.admin.create');
    Route::get('/article/{id}/{status}/show', 'ArticleController@show')->name('article.admin.show');
    Route::get('/article/{id}/edit', 'ArticleController@edit')->name('article.admin.edit');
    Route::get('/article/{filename}/image-thumbnail', 'ArticleController@showThumbnail')->name('article.admin.showThumbnail');
    Route::get('/article/{filename}/image', 'ArticleController@showImage')->name('article.admin.showImage');

    Route::get('/article/approval', 'ArticleController@approval')->name('article.admin.approval');
    Route::get('/article/approval/comment-data', 'ArticleController@commentData')->name('article.admin.comment');
    Route::get('/article/approval/waiting-data', 'ArticleController@waitingData')->name('article.admin.approval.waiting');
    Route::get('/article/approval/approved-data', 'ArticleController@approvedData')->name('article.admin.approval.approved');
    Route::get('/article/approval/rejected-data', 'ArticleController@rejectedData')->name('article.admin.approval.rejected');

    Route::post('/article/store', 'ArticleController@store')->name('article.admin.store');
    Route::post('/article/{id}/edit', 'ArticleController@update')->name('article.admin.update');
    Route::post('/article/{id}/approved', 'ArticleController@approved')->name('article.admin.approved');
    Route::put('/article/{id}/delete', 'ArticleController@destroy')->name('article.admin.delete');
    Route::put('/article/{id}/delete-comment', 'ArticleController@destroyComment')->name('article.admin.delete.comment');

    Route::get('/event', 'EventController@index')->name('event.admin.index');
    Route::get('/event/create', 'EventController@create')->name('event.admin.create');
    Route::get('/event/{id}/{status}/show', 'EventController@show')->name('event.admin.show');
    Route::get('/event/{id}/edit', 'EventController@edit')->name('event.admin.edit');
    Route::get('/event/{filename}/image-thumbnail', 'EventController@showThumbnail')->name('event.admin.showThumbnail');
    Route::get('/event/{filename}/image', 'EventController@showImage')->name('event.admin.showImage');

    Route::get('/event/approval', 'EventController@approval')->name('event.admin.approval');
    Route::get('/event/approval/waiting-data', 'EventController@waitingData')->name('event.admin.approval.waiting');
    Route::get('/event/approval/approved-data', 'EventController@approvedData')->name('event.admin.approval.approved');
    Route::get('/event/approval/rejected-data', 'EventController@rejectedData')->name('event.admin.approval.rejected');

    Route::post('/event/store', 'EventController@store')->name('event.admin.store');
    Route::post('/event/{id}/edit', 'EventController@update')->name('event.admin.update');
    Route::post('/event/{id}/approved', 'EventController@approved')->name('event.admin.approved');
    Route::put('/event/{id}/delete', 'EventController@destroy')->name('event.admin.delete');

    Route::get('/setting', 'SettingController@index')->name('setting.admin.index');
    Route::get('/setting/{id}/edit', 'SettingController@edit')->name('setting.admin.edit');

    Route::post('/setting/store', 'SettingController@store')->name('setting.admin.store');
    Route::put('/setting/{id}/edit', 'SettingController@update')->name('setting.admin.update');
    Route::delete('/setting/{id}/delete', 'SettingController@destroy')->name('setting.admin.delete');

    Route::get('/subscribe', 'SubscribeController@index')->name('subscribe.admin.index');
    Route::get('/subscribe/export-to-excel', 'SubscribeController@export')->name('subscribe.admin.export');
});
