<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Inspirasi Hijab - Login</title>
  <link rel="stylesheet" href="{{ asset('fe/css/frontend.css') }}">
</head>
<body>
  <div class="container">
    <div class="d-flex h-100vh">
      <div class="col-md-12 p-0 m-auto">
        <div class="border rounded">
          <div class="row">
            <div class="col-md-6">
              <img src="{{ asset('images/register-banner.png') }}" class="rounded-left img-fluid" alt="">
            </div>
            <div class="col-md-6">
              <div class="ath__form__container">
                <h5 class="heading-text font-weight-bold text-blue">Daftar Sebagai:</h5>
                <ul class="nav nav-pills nav-fill nav-account mb-4">
                  <li class="nav-item">
                    <a class="nav-link active" href="#personal" id="personal-tab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">Personal</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#komunitas" id="komunitas-tab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="false">Komunitas</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane fade show active" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                    <form method="POST" class="ath" action="{{ route('do_register') }}">
                      {{ csrf_field() }}
                      <input type="text" value="user" name="role">
                      <div class="form-group">
                        <input type="email" class="form-control ath__fc" id="email" name="email" placeholder="Masukan Email" required value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control ath__fc" id="username" placeholder="Masukan Nama kamu" name="name" required>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control ath__fc" id="password" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-row">
                        <div class="col-1">
                          <input type="checkbox" name="agreement" value="1">
                        </div>
                        <div class="col">
                          <p class="agreement">Dengan klik Daftar, Anda telah menyetujui <a href="#">Syarat & Ketentuan</a> serta <a href="#">kebijakan Privasi</a> Inspirasi Hijab</p>
                        </div>
                      </div>
                      <div class="form-row d-flex align-items-center">
                        <div class="col">
                          <a href="{{ route('login') }}" class="text-blue">Masuk</a>
                        </div>
                        <div class="col">
                          <button type="submit" class="btn btn-success float-right">Daftar</button>
                        </div>
                      </div>
                        @if ($errors->has('info'))
                            <strong>{{ $errors->first('info') }}</strong>
                        @endif
                    </form>
                  </div>
                  <div class="tab-pane fade" id="komunitas" role="tabpanel" aria-labelledby="komunitas-tab">
                    <form method="POST" class="ath" action="{{ route('do_register') }}">
                        {{ csrf_field() }}
                        <input type="text" value="community" name="role">
                        <div class="form-group">
                          <input type="text" class="form-control ath__fc" id="nama-komunitas" name="nama" placeholder="Nama Komunitas" required>
                          @if ($errors->has('nama'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('nama') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <input type="email" class="form-control ath__fc" id="email-komunitas" name="email" placeholder="Masukan Email" required>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control ath__fc" id="username-komunitas" placeholder="Masukan Name kamu" name="name" required>
                          @if ($errors->has('username'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('username') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <input type="password" class="form-control ath__fc" id="password-komunitas" placeholder="Password" name="password" required>
                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-row">
                          <div class="col-1">
                            <input type="checkbox" name="agreement" value="1">
                          </div>
                          <div class="col">
                            <p class="agreement">Dengan klik Daftar, Anda telah menyetujui <a href="#">Syarat & Ketentuan</a> serta <a href="#">kebijakan Privasi</a> Inspirasi Hijab</p>
                          </div>
                        </div>
                        <div class="form-row d-flex align-items-center">
                          <div class="col">
                            <a href="{{ route('login') }}" class="text-blue">Masuk</a>
                          </div>
                          <div class="col">
                            <button type="submit" class="btn btn-success float-right">Daftar</button>
                          </div>
                        </div>
                          @if ($errors->has('info'))
                              <strong>{{ $errors->first('info') }}</strong>
                          @endif
                      </form>
                  </div>
                </div>

                <div class="hr-or my-3">
                  <span class="hr-or__line"></span>
                  <span class="hr-or__text">or</span>
                  <span class="hr-or__line"></span>
                </div>

                <div class="sociallog">
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-fb.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-ig.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-google.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('fe/js/app.js') }}"></script>
  <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
  <script src="{{ asset('fe/js/frontend.js') }}"></script>
</body>
</html>
