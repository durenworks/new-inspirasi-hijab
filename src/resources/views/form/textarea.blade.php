<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label col-lg-2 text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="col-lg-10">
		{!! Form::textarea($field, isset($default) ? $default : null, 
			[
				'class' => 'form-control',
				'placeholder' => isset($placeholder) ? $placeholder : '',
			] + (isset($attributes) ? $attributes : [])
		) !!}
		
		@if (isset($required))
		<span class="help-block text-danger">{{ $required }}</span>
		@endif
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>