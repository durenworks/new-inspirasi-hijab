<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label col-lg-2 text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="col-lg-10">
		<div class="input-group">
			{!! 
				Form::text(
					$field,
					isset($default) ? $default : null,
					[
						'class' => 'form-control input-date '.(isset($class) ? $class : ''),
						'placeholder' => isset($placeholder) ? $placeholder : ''
					] + (isset($attributes) ? $attributes : [])
				)
			!!}
			<span class="input-group-addon">
				<span class="icon-calendar2"></span>
			</span>
		</div>
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>