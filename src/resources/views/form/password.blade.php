<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
	<label
		for="{{ $field }}" class="control-label col-lg-2 text-semibold"
	>
		{{ isset($label) ? $label : '' }}
	</label>
	<div class="col-lg-10">
		{!! Form::password($field, [
			'class' => 'form-control',
			'placeholder' => isset($placeholder) ? $placeholder : '',
			'id' => isset($id) ? $id : ''
		]) !!}
		
		@if (isset($required))
		<span class="help-block text-danger">{{ $required }}</span>
		@endif
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>