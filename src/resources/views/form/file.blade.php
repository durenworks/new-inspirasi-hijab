<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label col-lg-2 text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="col-lg-10">
		{!! 
			Form::file(
				$field,
				isset($attributes) ? $attributes : []
			)
		!!}

		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>