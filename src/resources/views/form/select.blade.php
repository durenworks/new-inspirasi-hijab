<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}"">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label {{ isset($label_col) ? $label_col : 'col-lg-2' }} text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="{{ isset($form_col) ? $form_col : 'col-lg-10' }}">
		{!! 
			Form::select(
				$field,
				$options,
				isset($default) ? $default : '',
				[
					'class' => 'form-control ' . (isset($class) ? $class : '')
				] + (isset($attributes) ? $attributes : [])
			) 
		!!}
		@if (isset($required))
		<span class="help-block text-danger">{{ $required }}</span>
		@endif
		@if (isset($help))
		<span class="help-block">{!! $help !!}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>