<div class="navbar navbar-default" id="navbar-second">
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav">
				<li class="{{ isset($active) && $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('backend.home') }}"><i class="icon-display4 position-left"></i> Dashboard</a></li>
				<li class="{{ isset($active) && $active == 'category' ? 'active' : '' }}" ><a href="{{ route('category.index') }}"><i class="icon-list"></i> Category</a></li>
				<li class="{{ isset($active) && $active == 'advertisement' ? 'active' : '' }}" ><a href="{{ route('advertisement.index') }}"><i class="icon-bell3"></i> Advertisement</a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-user position-left"></i> User <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						<li class="{{ isset($active) && $active == 'user' ? 'active' : '' }}" ><a href="{{ route('user.admin.index') }}"><i class="icon-file-text"></i> Data</a></li>
						<li class="{{ isset($active) && $active == 'user-approval' ? 'active' : '' }}" ><a href="{{ route('user.admin.approval') }}"><i class="icon-user-check"></i> Approval</a></li>
					</ul>
				</li>


				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-users4 position-left"></i> Community <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						<!--<li class="{{ isset($active) && $active == 'community' ? 'active' : '' }}" ><a href="{{ route('community.admin.index') }}"><i class="icon-file-text"></i> Data</a></li>-->
						<li class="{{ isset($active) && $active == 'community-approval' ? 'active' : '' }}" ><a href="{{ route('community.admin.approval') }}"><i class="icon-user-check"></i> Approval</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-blog position-left"></i> Article <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						<li class="{{ isset($active) && $active == 'article' ? 'active' : '' }}" ><a href="{{ route('article.admin.index') }}"><i class="icon-file-text"></i> Data</a></li>
						<li class="{{ isset($active) && $active == 'article-approval' ? 'active' : '' }}" ><a href="{{ route('article.admin.approval') }}"><i class="icon-file-check"></i> Approval</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-calendar3 position-left"></i> Event <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						<li class="{{ isset($active) && $active == 'event' ? 'active' : '' }}" ><a href="{{ route('event.admin.index') }}"><i class="icon-file-text"></i> Data</a></li>
						<li class="{{ isset($active) && $active == 'event-approval' ? 'active' : '' }}" ><a href="{{ route('event.admin.approval') }}"><i class="icon-file-check"></i> Approval</a></li>
					</ul>
				</li>
				<li class="{{ isset($active) && $active == 'subscribe' ? 'active' : '' }}"><a href="{{ route('subscribe.admin.index') }}"><i class="icon-users position-left"></i> Subscribe</a></li>
				<li class="{{ isset($active) && $active == 'setting' ? 'active' : '' }}"><a href="{{ route('setting.admin.index') }}"><i class="icon-gear position-left"></i> Setting</a></li>
				

				
			</ul>

		</div>
	</div>
