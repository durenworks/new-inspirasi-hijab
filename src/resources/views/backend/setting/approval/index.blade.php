@extends('layouts.backend', ['menu_active' => 'user-approval'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">User</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li>User</li>
		<li class="active">Approval</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="waitingCommunityTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Status</th>
							<th>Action</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection


@section('modal')
	@include('backend.community.approval._approval_modal')
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		waitingDataTables();
		var dtable = $('#waitingCommunityTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	});
	
	function waitingDataTables(){
		$('#waitingCommunityTable').DataTable().destroy();
		$('#waitingCommunityTable tbody').empty();

		$('#waitingCommunityTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			scrollY:500,
			scroller:true,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/community/approval/waiting-for-approve',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'first_name', name: 'first_name',searchable:true,orderable:true},
				{data: 'last_name', name: 'last_name',searchable:true,orderable:true},
				{data: 'phone', name: 'phone',searchable:true,orderable:true},
				{data: 'email', name: 'email',searchable:true,orderable:true},
				{data: 'email_verified_at', name: 'email_verified_at',searchable:false,visible:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[8, 'desc']
			],
		});
	}

	function detailModal(url){
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#approvalCommunityform').attr('action', response.url_update);
			$('#approval_name').val(response.name);
			$('#approval_first_name').val(response.first_name);
			$('#approval_last_name').val(response.last_name);
			$('#approval_phone').val(response.phone);
			$('#approval_email').val(response.email);
			$('#approval_description').val(response.description);
			$('#approval_id').val(response.id);
			$('#approvalCommunityModal').modal();
		});
	}

	$('#approvalCommunityform').submit(function (event) {
		event.preventDefault();
		
		$('#approvalCommunityModal').modal('hide');
		bootbox.confirm("Are you sure want to approve this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "post",
					url: $('#approvalCommunityform').attr('action'),
					data: $('#approvalCommunityform').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#approvalCommunityform').trigger("reset");
						$('#approvalCommunityModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#approvalCommunityModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been approved.');
					$('#waitingCommunityTable').DataTable().ajax.reload();

				});
			}else{
				$('#approvalCommunityModal').modal();
			}
		});
	});

</script>
@endsection
