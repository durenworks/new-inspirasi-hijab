<div class="modal fade" id="insertSettingModal" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'insertSettingform', 'class' => 'form-horizontal', 'url' => route('setting.admin.store')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">USER FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'name',
								'label' => 'Name',
								'placeholder' => 'Please Insert Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'name'
								]
							])

							@include('form.text', [
								'field' => 'value',
								'label' => 'Value',
								'placeholder' => 'Please Insert Value Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'value'
								]
							])

							

							
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>