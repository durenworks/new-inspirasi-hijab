@extends('layouts.backend', ['menu_active' => 'setting'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Setting</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Setting</a></li>
		<li class="active">Data</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertSettingModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="settingTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Value</th>
							<th>Action</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('backend.setting._update_modal')
	@include('backend.setting._insert_modal')
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		$('#settingTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/setting',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'value', name: 'value',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[4, 'desc']
			],
		});
	});

	function edit(url){
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#updateSettingform').attr('action', response.url_update);
			$('#update_name').val(response.name);
			$('#update_value').val(response.value);
			$('#updateSettingModal').modal();
		});
	}
	$('#insertSettingform').submit(function (event) {
		event.preventDefault();
		var name = $('#name').val();
		var value = $('#value').val();
		
		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if (!value) {
			$("#alert_error").trigger("click", 'Please Type Value First.');
			return false;
		}

		$('#insertSettingModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insertSettingform').attr('action'),
					data: $('#insertSettingform').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#insertSettingform').trigger("reset");
						$('#insertSettingModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#insertSettingModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					$('#settingTable').DataTable().ajax.reload();

				});
			}else{
				$('#insertSettingModal').modal();
			}
		});
	});

	$('#updateSettingform').submit(function (event) {
		event.preventDefault();
		var name = $('#update_name').val();
		var value = $('#update_value').val();

		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if (!value) {
			$("#alert_error").trigger("click", 'Please Type Value First.');
			return false;
		}

		$('#updateSettingModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "PUT",
					url: $('#updateSettingform').attr('action'),
					data: $('#updateSettingform').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#updateSettingform').trigger("reset");
						$('#updateSettingModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#updateSettingModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					$('#settingTable').DataTable().ajax.reload();

				});
			}else{
				$('#updateSettingModal').modal();
			}
		});
	});

	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "delete",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					$('#settingTable').DataTable().ajax.reload();
				});
			}
		});
	}
</script>
@endsection
