@extends('layouts.backend', ['menu_active' => 'advertisement'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Advertisement</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Advertisement</a></li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
					<form class="heading-form" action="{{ route('advertisement.index') }}">
						<div class="form-group has-feedback">
							<input type="search" name="search" class="form-control" style="width: 500px;" placeholder="Please type the keyword and press enter to start searching">
							<div class="form-control-feedback">
								<i class="icon-search4 text-size-base text-muted"></i>
							</div>
						</div>
					</form>
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertAdvertisementModal"><i class="icon-plus2 position-left"></i>Create New</a>
			
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				@foreach($advertisements as $key => $advertisement)
				
						<div class="col-lg-3 col-sm-6">
							<div class="thumbnail">
								<div class="row">
									<div class="thumb">
										@if($advertisement->image)
											<img src="{{ route('advertisement.showThumbnail',$advertisement->image) }}" alt="" style="max-width:100%">
										@endif
									</div>
								</div>
								

								<div class="caption">
									<h6 class="no-margin">{{ $advertisement->title }} <span style="padding-left: 40%;"><a href="{{route('advertisement.edit',$advertisement->id)}}" style="color: black;" class="edit_article" id="edit_{{$key}}" data-id="{{$key}}"><i class="icon-pencil6"></i></a> <a href="{{ route('advertisement.delete', $advertisement->id)}}" class="delete_article" style="color: red;" id="delete_{{$key}}" data-id="{{ $key }}"><i class="icon-close2"></i></a></span></h6>
								</div>
							</div>
						</div>

				@endforeach
			</div>
		</div>
@endsection

@section('modal')
	@include('backend.advertisement._insert_modal')
	@include('backend.advertisement._update_modal')
@endsection

@section('content-js')
<script>
	$('#insertAdvertisementform').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var url = $('#url').val();
		if (!title){
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		if (!url){
			$("#alert_error").trigger("click", 'Please Type Url First.');
			return false;
		}

		$('#insertAdvertisementModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insertAdvertisementform').attr('action'),
					data:new FormData($("#insertAdvertisementform")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
						$('#insertAdvertisementModal').modal('hide');
					},
					success: function (response) {
						$('#insertAdvertisementform').trigger("reset");
						$("#alert_success").trigger("click", 'Data has been saved');
						location.reload();

					},
					error: function (response) {
						$.unblockUI();
						$('#insertAdvertisementModal').modal();
					}
				});
			}else{
				$('#insertAdvertisementModal').modal();
			}
		});
	});

	$('.delete_article').on('click',function(event){
		event.preventDefault();
		var url = $(this).attr('href');

		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "delete",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}
				).done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been deleted');
					location.reload();
				});
			}
		});
	});

	$('.edit_article').on('click',function(event){
		event.preventDefault();
		var url = $(this).attr('href');

		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#updateAdvertisementform').attr('action', response.url_update);
			$('#update_title').val(response.title);
			$('#update_url').val(response.url);
			$('#show_image').attr('src', response.url_image);
			$('#updateAdvertisementModal').modal();
		});
	});

	$('#updateAdvertisementform').submit(function (event) {
			event.preventDefault();
			var update_title = $('#update_title').val();
			var update_url = $('#update_url').val();
			
			if (!update_title) {
				$("#alert_error").trigger("click", 'Please Type Title First.');
				return false;
			}

			if (!update_url) {
				$("#alert_error").trigger("click", 'Please Type url First.');
				return false;
			}


			$('#updateAdvertisementModal').modal('hide');
			$.ajax({
				type: "POST",
				url: $('#updateAdvertisementform').attr('action'),
				data:new FormData($("#updateAdvertisementform")[0]),
				processData: false,
				contentType: false,
				beforeSend: function () {
					$.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
					

				},
				success: function (response) {
					$.unblockUI();
					$('#updateAdvertisementform').trigger("reset");
					$('#updateAdvertisementModal').modal('hide');

				},
				error: function (response) {
					$.unblockUI();
					if (response.status == 422) $("#alert_error").trigger("click",response.responseJSON);

					$('#updateAdvertisementModal').modal();
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data has been changed.');
				location.reload();
			});
		});
</script>
@endsection
