<div class="modal fade" id="insertAdvertisementModal" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'insertAdvertisementform', 'class' => 'form-horizontal', 'url' => route('advertisement.store') ,'enctype' => 'multipart/form-data']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">ADVERTISEMENT FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'title',
								'label' => 'Title',
								'placeholder' => 'Please Insert Title',
								'required' => '*Required',
								'attributes' => [
									'id' => 'title'
								]
							])

							@include('form.text', [
								'field' => 'url',
								'label' => 'Url',
								'placeholder' => 'Please Insert url',
								'attributes' => [
									'id' => 'url'
								]
							])

							@include('form.file', [
								'field' => 'file_upload',
								'label' => 'Image',
								'attributes' => [
									'id' => 'file_upload'
								]
							])
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>