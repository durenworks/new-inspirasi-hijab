@extends('layouts.backend', ['menu_active' => 'event-approval'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Event</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('event.admin.approval') }}" id="url_event_index" >Event</a></li>
		<li class="active">Review</li>
	</ul>
@endsection

@section('content')
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('event.admin.approved',$event->id),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal',
			'id'=> 'form'
		])
	!!}
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>
		<div class="panel-body">
			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'default' => $event->title,
				'placeholder' => 'Please Insert Title',
				'required' => '*Required',
				'attributes' => [
					'id' => 'title'
				]
			])

			@include('form.date', [
				'field' => 'event_date',
				'label' => 'Event Date',
				'default' => $event->start->format('d/m/Y h:i A').' - '.$event->end->format('d/m/Y h:i A'),
				'class' => 'daterange-time',
				'placeholder' => 'dd/mm/yyyy'
			])

			@include('form.file', [
				'field' => 'file_upload',
				'label' => 'Image',
				'attributes' => [
					'id' => 'file_upload'
				]
			])

			<div class="row col-md-offset-2">
				<img src="{{route('event.admin.showThumbnail',$event->image)}}" alt="" style="max-width:15%">
			</div>
		</div>
	</div>
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="form-group" id="_content">
				{!! $event->content !!}
			</div>
			<div class="form-group hidden" id="click2edit">
				<textarea class="form-control summernote" id="content" name="content"> {{ $event->content }}</textarea>
				<span class="help-block text-danger">*Required</span>
			</div>
		</div>
		<hr/>
		<div class="form-group text-center" style="margin-top: 10px;">
			{!! Form::hidden('is_updated','false' , array('id' => 'is_updated')) !!}
			@if($status == 'waiting')
				<button type="submit" id="save" class="btn btn-success hidden">Save<i class="icon-checkmark3 position-left"></i></button>
				<button type="button" id="edit" class="btn btn btn-primary">Edit <i class="icon-pencil3 position-left"></i></button>
				<button type="submit" id="approved" class="btn btn-primary-pink">Approved <i class="icon-floppy-disk position-left"></i></button>
			@endif
			<a href="{{ route('event.admin.approval') }}" class="btn btn-danger" >Cancel  <span class="icon-arrow-left52"></span></a>
	</div>
	<br/>
	{!! Form::close() !!}
	</div>
@endsection

@section('content-js')
<script type="text/javascript" src="{{ mix('js/datepicker.js') }}"></script>
<script>
	$('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });
	
	$('#edit').on('click', function() {
		$('#is_updated').val('edit');
        $('#save').removeClass( "hidden" );
        $('#click2edit').removeClass( "hidden" );
        $('#_content').addClass( "hidden" );
        $('#edit').addClass( "hidden" );
	})

	$('#approved').on('click', function() {
		$('#is_updated').val('approved');
	})

	$('#save').on('click', function() {
		$('#is_updated').val('edit');
	})
	
	$('.daterange-time').daterangepicker({
		timePicker: true,
		applyClass: 'bg-slate-600',
		cancelClass: 'btn-default',
		locale: {
			format: 'DD/MM/YYYY h:mm a'
		}
	});

	
	$('#form').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var content = $('#content').val();
		
		if (!title) {
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		
		if (!content.replace(/<[^>]+>/g, '')) {
			$("#alert_error").trigger("click", 'Please Fill Content First.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						var url = $('#url_event_index').attr('href');
						document.location.href = url;
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);
					}
				})
				.done(function ($result) {

				});
			}
		});
	});
</script>
@endsection
