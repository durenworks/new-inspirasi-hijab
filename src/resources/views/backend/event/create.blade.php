@extends('layouts.backend', ['menu_active' => 'event'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Event</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('event.admin.index') }}" id="url_article_index" >Event</a></li>
		<li class="active">Create</li>
	</ul>
@endsection

@section('content')
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('event.admin.store'),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal',
			'id'=> 'form'
		])
	!!}
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>
		<div class="panel-body">
			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'placeholder' => 'Please Insert Title',
				'required' => '*Required',
				'attributes' => [
					'id' => 'title'
				]
			])

			@include('form.date', [
				'field' => 'event_date',
				'label' => 'Event Date',
				'class' => 'daterange-time',
				'placeholder' => 'dd/mm/yyyy'
			])
			
			@include('form.file', [
				'field' => 'file_upload',
				'label' => 'Image',
				'attributes' => [
					'id' => 'file_upload'
				]
			])
		</div>
	</div>
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="form-group">
				<textarea class="form-control summernote" id="content" name="content"></textarea>
				<span class="help-block text-danger">*Required</span>
			</div>
		</div>
		<hr/>
		<div class="form-group text-center" style="margin-top: 10px;">
			<button type="submit" class="btn btn-primary-pink">Save <i class="icon-floppy-disk position-left"></i></button>
			<a href="{{ route('article.admin.index') }}" class="btn btn-danger" >Cancel  <span class="icon-arrow-left52"></span></a>
	</div>
	<br/>
	{!! Form::close() !!}
	</div>

@endsection

@section('content-js')
<script type="text/javascript" src="{{ mix('js/datepicker.js') }}"></script>
<script>
	$('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });
	
	$('.daterange-time').daterangepicker({
		timePicker: true,
		applyClass: 'bg-slate-600',
		cancelClass: 'btn-default',
		locale: {
			format: 'DD/MM/YYYY h:mm a'
		}
	});

	$('#form').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var content = $('#content').val();
		
		if (!title) {
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		if (!content.replace(/<[^>]+>/g, '')) {
			$("#alert_error").trigger("click", 'Please Fill Content First.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						var url = $('#url_article_index').attr('href');
						document.location.href = url;
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);
					}
				});
			}
		});
	});
</script>
@endsection
