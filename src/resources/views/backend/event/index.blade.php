@extends('layouts.backend', ['menu_active' => 'event'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Event</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li>Event</li>
		<li class="active">Data</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('event.admin.create') }}" class="btn btn-default"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="eventTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Created By</th>
							<th>Title</th>
							<th>Start</th>
							<th>End</th>
							<th>Content</th>
							<th>Status</th>
							<th>Published At</th>
							<th>Action</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data has been saved');
		else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data has been changed');
		
		
		$('#eventTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/event',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_by', name: 'created_by',searchable:true,orderable:true},
				{data: 'title', name: 'title',searchable:true,orderable:true},
				{data: 'start', name: 'start',searchable:true,orderable:true},
				{data: 'end', name: 'end',searchable:true,orderable:true},
				{data: 'content', name: 'content',searchable:true,orderable:true},
				{data: 'status', name: 'status',searchable:true,orderable:true},
				{data: 'published_at', name: 'published_at',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[9, 'desc']
			],
		});
	});
	
	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					$('#eventTable').DataTable().ajax.reload();
				});
			}
		});
	}

</script>
@endsection
