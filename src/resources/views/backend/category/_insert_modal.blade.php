<div class="modal fade" id="insertCategoryModal" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'insertCetegoryform', 'class' => 'form-horizontal', 'url' => route('category.store'),'enctype' => 'multipart/form-data']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">CATEGORY FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'name',
								'label' => 'Name',
								'placeholder' => 'Please Insert Category Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'name'
								]
							])

							@include('form.select', [
								'field' => 'category_id',
								'label' => 'Parent',
								'options' => [
									'' => '-'
								]+$categories,
								'class' => 'select-search',
								'attributes' => [
									'id' => 'list_category'
								]
							])
							<div id="show_only_parent">
								@include('form.textarea', [
									'field' => 'quote',
									'label' => 'Quote',
									'placeholder' => 'Please Insert Quote Name',
									'attributes' => [
										'id' => 'quote'
									]
								])

								@include('form.file', [
									'field' => 'file_upload',
									'label' => 'Persona',
									'attributes' => [
										'id' => 'file_upload'
									]
								])


								@include('form.select', [
									'field' => 'ambasador_id',
									'label' => 'Ambasador',
									'options' => [
										'' => '-'
									]+$users,
									'class' => 'select-search',
									'attributes' => [
										'id' => 'list_user'
									]
								])
							</div>
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>