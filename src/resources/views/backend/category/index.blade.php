@extends('layouts.backend', ['menu_active' => 'category'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Category</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Catagory</a></li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertCategoryModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="categoryTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Parent Name</th>
							<th>Ambasador Name</th>
							<th>Quote</th>
							<th>Action</th>
							<th>Sequence</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('backend.category._update_modal')
	@include('backend.category._insert_modal')
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		$('#categoryTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/category',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'parent_name', name: 'parent_name',searchable:true,orderable:true},
				{data: 'ambasador_name', name: 'ambasador_name',searchable:true,orderable:true},
				{data: 'quote', name: 'quote',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'sequence', name: 'sequence',searchable:false, visible:false},
				{data: 'category_id', name: 'category_id',searchable:false, visible:false},
				{data: 'created_at', name: 'created_at',searchable:false, visible:false}
			],
			order:[ 
				[5, 'asc'],
				[2, 'asc'],
				[4, 'asc']
			],
		});
	});

	function edit(url){
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#updateCetegoryform').attr('action', response.url_update);
			$('#update_name').val(response.name);
			$("#update_list_category").val(response.parent_id).trigger('change');
			$("#update_list_user").val(response.user_ambasador_id).trigger('change');
			$("#update_quote").val(response.quote);
			$('#show_image').attr('src', response.image);
			if(response.parent_id == null)
			{
				$('#update_show_only_parent').removeClass('hidden');
			}else
			{
				$('#update_show_only_parent').addClass('hidden');
			}
			
			$('#updateCategoryModal').modal();
		});
	}

	$('#insertCetegoryform').submit(function (event) {
		event.preventDefault();
		var name = $('#name').val();
		var parent = $('#list_category').val();

		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if(parent == ''){
			var quote = $('#quote').val();
			var file_upload = $('#file_upload').val();
			var ambasador = $('#list_user').val();

			if (!quote) {
				$("#alert_error").trigger("click", 'Please Type Quote First.');
				return false;
			}

			if (!file_upload) {
				$("#alert_error").trigger("click", 'Please Select Image First.');
				return false;
			}

			if (!ambasador) {
				$("#alert_error").trigger("click", 'Please Select Ambasador First.');
				return false;
			}
		}


		$('#insertCategoryModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insertCetegoryform').attr('action'),
					data:new FormData($("#insertCetegoryform")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#insertCetegoryform').trigger("reset");
						$('#insertCategoryModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#insertCategoryModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					location.reload();

				});
			}else{
				$('#insertCategoryModal').modal();
			}
		});
	});

	$('#updateCetegoryform').submit(function (event) {
		event.preventDefault();
		var name = $('#update_name').val();
		var parent = $('#update_list_category').val();

		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if(parent == ''){
			var quote = $('#update_quote').val();
			var file_upload = $('#update_file_upload').val();
			var ambasador = $('#update_list_user').val();

			if (!quote) {
				$("#alert_error").trigger("click", 'Please Type Quote First.');
				return false;
			}

			if (!ambasador) {
				$("#alert_error").trigger("click", 'Please Select Ambasador First.');
				return false;
			}
		}

		$('#updateCategoryModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#updateCetegoryform').attr('action'),
					data:new FormData($("#updateCetegoryform")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#updateCetegoryform').trigger("reset");
						$('#updateCategoryModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#updateCategoryModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					location.reload();

				});
			}else{
				$('#updateCategoryModal').modal();
			}
		});
	});

	$('#update_list_category').on('change',function(){
		var value = $(this).val(); 
		if(value == '')
		{
			$('#update_show_only_parent').removeClass('hidden');
		}else
		{
			$('#update_show_only_parent').addClass('hidden');
		}
	});

	$('#list_category').on('change',function(){
		var value = $(this).val(); 
		if(value == '')
		{
			$('#show_only_parent').removeClass('hidden');
		}else
		{
			$('#show_only_parent').addClass('hidden');
		}
	});

	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					location.reload();
				});
			}
		});
	}
</script>
@endsection
