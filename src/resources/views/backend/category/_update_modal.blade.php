<div class="modal fade" id="updateCategoryModal" role="dialog" aria-labelledby="updateModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'updateCetegoryform', 'class' => 'form-horizontal', 'url' => '#','enctype' => 'multipart/form-data']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">CATEGORY FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'name',
								'label' => 'Name',
								'placeholder' => 'Please Insert Category Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'update_name'
								]
							])

							@include('form.select', [
								'field' => 'category_id',
								'label' => 'Parent',
								'options' => [
									'' => '-'
								]+$categories,
								'class' => 'select-search',
								'attributes' => [
									'id' => 'update_list_category'
								]
							])

							<div id="update_show_only_parent">
								@include('form.textarea', [
									'field' => 'quote',
									'label' => 'Quote',
									'placeholder' => 'Please Insert Quote Name',
									'attributes' => [
										'id' => 'update_quote'
									]
								])

								@include('form.file', [
									'field' => 'file_upload',
									'label' => 'Persona',
									'attributes' => [
										'id' => 'update_file_upload'
									]
								])

								<div class="row">
									<div class="col-md-offset-2"> <img src="#" id="show_image" alt="" style="max-width:15%"> </div>
									<br/>
								</div>


								@include('form.select', [
									'field' => 'ambasador_id',
									'label' => 'Ambasador',
									'options' => [
										'' => '-'
									]+$users,
									'class' => 'select-search',
									'attributes' => [
										'id' => 'update_list_user'
									]
								])
							</div>
							
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>