@extends('layouts.backend', ['menu_active' => 'community'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Community</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Community</a></li>
		<li class="active">Data</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertCommunityModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="communityTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Status</th>
							<th>Action</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('modal')
	@include('backend.community._update_modal')
	@include('backend.community._insert_modal')
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		$('#communityTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/community',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'first_name', name: 'first_name',searchable:true,orderable:true},
				{data: 'last_name', name: 'last_name',searchable:true,orderable:true},
				{data: 'phone', name: 'phone',searchable:true,orderable:true},
				{data: 'email', name: 'email',searchable:true,orderable:true},
				{data: 'email_verified_at', name: 'email_verified_at',searchable:false,visible:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[7, 'desc']
			],
		});
	});

	function edit(url){
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
			$('#updateUserform').attr('action', response.url_update);
			$('#update_name').val(response.name);
			$('#update_first_name').val(response.first_name);
			$('#update_last_name').val(response.last_name);
			$('#update_phone').val(response.phone);
			$('#update_email').val(response.email);
			$('#update_description').val(response.description);
			$('#updateUserModal').modal();
		});
	}

	function validateEmail(email)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
	
	$('#insertUserform').submit(function (event) {
		event.preventDefault();
		var name = $('#name').val();
		var first_name = $('#first_name').val();
		var email = $('#email').val();
		var password = $('#password').val();
		var repassword = $('#repassword').val();

		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if (!first_name) {
			$("#alert_error").trigger("click", 'Please Type First First.');
			return false;
		}

		if (!email) {
			$("#alert_error").trigger("click", 'Please Type Email First.');
			return false;
		}

		if(!validateEmail(email)){
			$("#alert_info").trigger("click", 'Email is not valid.');
			return false;
		}

		if (!password) {
			$("#alert_error").trigger("click", 'Please Type Password First.');
			return false;
		}

		if (repassword != password) {
			$("#alert_error").trigger("click", 'Your Password is not matching.');
			return false;
		}

		$('#insertUserModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insertUserform').attr('action'),
					data: $('#insertUserform').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#insertUserform').trigger("reset");
						$('#insertUserModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#insertUserModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					$('#userTable').DataTable().ajax.reload();

				});
			}else{
				$('#insertCategoryModal').modal();
			}
		});
	});

	$('#updateUserform').submit(function (event) {
		event.preventDefault();
		var name = $('#update_name').val();
		var first_name = $('#update_first_name').val();
		var email = $('#update_email').val();
		var password = $('#update_password').val();
		var repassword = $('#update_repassword').val();

		if (!name) {
			$("#alert_error").trigger("click", 'Please Type Name First.');
			return false;
		}

		if (!first_name) {
			$("#alert_error").trigger("click", 'Please Type First First.');
			return false;
		}

		if (!email) {
			$("#alert_error").trigger("click", 'Please Type Email First.');
			return false;
		}

		if(!validateEmail(email)){
			$("#alert_info").trigger("click", 'Email is not valid.');
			return false;
		}

		
		if (repassword != password) {
			$("#alert_error").trigger("click", 'Your Password is not matching.');
			return false;
		}

		$('#updateUserModal').modal('hide');
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "PUT",
					url: $('#updateUserform').attr('action'),
					data: $('#updateUserform').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						$('#updateUserform').trigger("reset");
						$('#updateUserModal').modal('hide');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);

						$('#updateUserModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data has been saved.');
					$('#userTable').DataTable().ajax.reload();

				});
			}else{
				$('#updateUserModal').modal();
			}
		});
	});

	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					$('#userTable').DataTable().ajax.reload();
				});
			}
		});
	}
</script>
@endsection
