<div class="modal fade" id="updateCommunityModal" role="dialog" aria-labelledby="updateModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'PUT', 'id' => 'updateCommunityform', 'class' => 'form-horizontal', 'url' => '#']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">COMMUNITY FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
					   @include('form.text', [
								'field' => 'name',
								'label' => 'Name',
								'placeholder' => 'Please Insert User Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'update_name'
								]
							])

							@include('form.text', [
								'field' => 'first_name',
								'label' => 'First Name',
								'placeholder' => 'Please Insert First Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'update_first_name'
								]
							])

							@include('form.text', [
								'field' => 'last_name',
								'label' => 'Last Name',
								'placeholder' => 'Please Insert Last Name',
								'attributes' => [
									'id' => 'update_last_name'
								]
							])

							@include('form.text', [
								'field' => 'phone',
								'label' => 'Phone',
								'placeholder' => 'Please Insert Phone',
								'attributes' => [
									'id' => 'update_phone'
								]
							])

							@include('form.text', [
								'field' => 'email',
								'label' => 'Email',
								'placeholder' => 'Please Insert Email',
								'required' => '*Required',
								'attributes' => [
									'id' => 'update_email'
								]
							])

							@include('form.password', [
								'field' => 'password',
								'label' => 'Password',
								'placeholder' => 'Please Insert Password',
								'required' => '*Required',
								'id' => 'update_password'
							])

							@include('form.password', [
								'field' => 'repassword',
								'label' => 'Repeat password',
								'placeholder' => 'Please Repeat Password',
								'required' => '*Required',
								'id' => 'update_repassword'
								
							])

							@include('form.textarea', [
								'field' => 'description',
								'label' => 'Description',
								'placeholder' => 'Please Type About You',
								'attributes' => [
									'id' => 'update_description'
								]
							])

							
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>