<div class="modal fade" id="approvalCommunityModal" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'approvalCommunityform', 'class' => 'form-horizontal', 'url' => route('community.admin.approved')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">USER FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'name',
								'label' => 'Name',
								'placeholder' => 'Please Insert User Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'approval_name',
									'readonly' => 'readonly'
								]
							])

							@include('form.text', [
								'field' => 'first_name',
								'label' => 'First Name',
								'placeholder' => 'Please Insert First Name',
								'required' => '*Required',
								'attributes' => [
									'id' => 'approval_first_name',
									'readonly' => 'readonly'
								]
							])

							@include('form.text', [
								'field' => 'last_name',
								'label' => 'Last Name',
								'placeholder' => 'Please Insert Last Name',
								'attributes' => [
									'id' => 'approval_last_name',
									'readonly' => 'readonly'
								]
							])

							@include('form.text', [
								'field' => 'phone',
								'label' => 'Phone',
								'placeholder' => 'Please Insert Phone',
								'attributes' => [
									'id' => 'approval_phone',
									'readonly' => 'readonly'
								]
							])

							@include('form.text', [
								'field' => 'email',
								'label' => 'Email',
								'placeholder' => 'Please Insert Email',
								'required' => '*Required',
								'attributes' => [
									'id' => 'approval_email',
									'readonly' => 'readonly'
								]
							])

							@include('form.textarea', [
								'field' => 'description',
								'label' => 'Description',
								'placeholder' => 'Please Type About You',
								'attributes' => [
									'id' => 'approval_description',
									'readonly' => 'readonly'
								]
							])

							{!! Form::hidden('id',null , array('id' => 'approval_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-primary-pink">Approved</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>