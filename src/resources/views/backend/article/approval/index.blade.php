@extends('layouts.backend', ['menu_active' => 'article-approval'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Article</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li>Article</li>
		<li class="active">Approval</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="tabbable tab-content-bordered">
				<ul class="nav nav-tabs">
					<li class="@if($active_tab == 'waiting') active @endif"><a href="#waiting" data-toggle="tab" aria-expanded="false" onclick="changeTab('waiting')">Waiting for Approve</a></li>
					<li class="@if($active_tab == 'approved') active @endif"><a href="#approved" data-toggle="tab" aria-expanded="true" onclick="changeTab('approved')">Approved</a></li>
					<li class="@if($active_tab == 'rejected') active @endif"><a href="#rejected" data-toggle="tab" aria-expanded="true" onclick="changeTab('rejected')">Rejected</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane @if($active_tab == 'waiting') active @endif" id="waiting">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="waitingTable">
									<thead>
										<tr>
											<th>Id</th>
											<th>Created By</th>
											<th>Category</th>
											<th>Title</th>
											<th>Content</th>
											<th>Tags</th>
											<th>Status</th>
											<th>Published At</th>
											<th>Action</th>
											<th>Created at</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'approved') active @else fade @endif" id="approved">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="approvedTable">
									<thead>
										<tr>
											<th>Id</th>
											<th>Created By</th>
											<th>Category</th>
											<th>Title</th>
											<th>Content</th>
											<th>Tags</th>
											<th>Status</th>
											<th>Published At</th>
											<th>Action</th>
											<th>Created at</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'rejected') active @else fade @endif" id="rejected">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="rejectedTable">
									<thead>
										<tr>
											<th>Id</th>
											<th>Created By</th>
											<th>Category</th>
											<th>Title</th>
											<th>Content</th>
											<th>Tags</th>
											<th>Status</th>
											<th>Published At</th>
											<th>Action</th>
											<th>Created at</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
	{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		$('#active_tab').on('change',function(){
			var active_tab = $('#active_tab').val();
			if(active_tab == 'waiting'){
				waitingDataTables();

				var dtable = $('#waitingTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();

				
			}else if( active_tab == 'approved'){
				approvedDataTables();

				var dtable = $('#approvedTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();
			}else{
				rejectedDataTables();

				var dtable = $('#rejectedTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();
			}
		});

		var active_tab = $('#active_tab').val();
		if(active_tab == 'waiting'){
			waitingDataTables();

			var dtable = $('#waitingTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();

		}else if( active_tab == 'approved'){
			approvedDataTables();

			var dtable = $('#approvedTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();
		}else{
			rejectedDataTables();

			var dtable = $('#rejectedTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();
		}
	});
	
	function changeTab(status){
		$('#active_tab').val(status).trigger('change');
	}

	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					$('#articleTable').DataTable().ajax.reload();
				});
			}
		});
	}
	function waitingDataTables(){
		$('#waitingTable').DataTable().destroy();
		$('#waitingTable tbody').empty();

		$('#waitingTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			fixedHeader: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			bAutoWidth: false, 
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/article/approval/waiting-data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_by', name: 'created_by',searchable:true,orderable:true,width: "20px"},
				{data: 'category_name', name: 'category_name',searchable:true,orderable:true,width: "20px"},
				{data: 'title', name: 'title',searchable:true,orderable:true,width: "20px"},
				{data: 'content', name: 'content',searchable:true,orderable:true,width:'50px'},
				{data: 'tags', name: 'tags',searchable:true,orderable:true,width: "20px"},
				{data: 'status', name: 'status',searchable:true,orderable:true,width: "20px"},
				{data: 'published_at', name: 'published_at',searchable:true,orderable:true,width: "20px"},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true,width: "20px"},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[8, 'desc']
			],
		});
	}

	function approvedDataTables(){
		$('#approvedTable').DataTable().destroy();
		$('#approvedTable tbody').empty();

		$('#approvedTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			scrollY:500,
			scroller:true,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/article/approval/approved-data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_by', name: 'created_by',searchable:true,orderable:true},
				{data: 'category_name', name: 'category_name',searchable:true,orderable:true},
				{data: 'title', name: 'title',searchable:true,orderable:true},
				{data: 'content', name: 'content',searchable:true,orderable:true},
				{data: 'tags', name: 'tags',searchable:true,orderable:true},
				{data: 'status', name: 'status',searchable:true,orderable:true},
				{data: 'published_at', name: 'published_at',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[8, 'desc']
			],
		});
	}

	function rejectedDataTables(){
		$('#rejectedTable').DataTable().destroy();
		$('#rejectedTable tbody').empty();

		$('#rejectedTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			scrollY:500,
			scroller:true,
			autoWidth: false,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/article/approval/rejected-data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_by', name: 'created_by',searchable:true,orderable:true},
				{data: 'category_name', name: 'category_name',searchable:true,orderable:true},
				{data: 'title', name: 'title',searchable:true,orderable:true},
				{data: 'content', name: 'content',searchable:true,orderable:true},
				{data: 'tags', name: 'tags',searchable:true,orderable:true},
				{data: 'status', name: 'status',searchable:true,orderable:true},
				{data: 'published_at', name: 'published_at',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[8, 'desc']
			],
			columnDefs: [
				{ width: "0px", targets: "3" },
				{ width: "0px", targets: "4" }
			]
		});
	}

</script>
@endsection
