@extends('layouts.backend', ['menu_active' => 'article-approval'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Article</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Home</a></li>
		<li><a href="{{ route('article.admin.approval') }}" id="url_article_index" >Article</a></li>
		<li class="active">Review</li>
	</ul>
@endsection

@section('content')
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('article.admin.approved',$article->id),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal',
			'id'=> 'form'
		])
	!!}
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>
		<div class="panel-body">
			@include('form.text', [
				'field' => 'title',
				'label' => 'Title',
				'default' => $article->title,
				'placeholder' => 'Please Insert Title',
				'required' => '*Required',
				'attributes' => [
					'id' => 'title'
				]
			])

			<div class="form-group">
				<label for="tag" class="control-label col-lg-2 text-semibold">Tags</label>
				<div class="col-lg-10">
					<input type="text" value="{{$article->tags}}" id="selected_tag" name="selected_tag" data-role="tagsinput" class="tagsinput-typeahead">
					<span class="help-block">Tekan ENTER untuk memisahkan label</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="category" class="control-label col-lg-2 text-semibold">
					Category
				</label>
				<div class="col-lg-10">
					<select name="selected_category" id="selected_category" data-placeholder="Please Select Category First" class="select-clear">
						<option value=""></option>
						@foreach($categories as $category)
							<optgroup label="{{ $category->name }}">
								@foreach($category->childs as $subcategory)
								<option value="{{ $subcategory->id }}" @if($article->category_id == $subcategory->id) selected @endif>{{$subcategory->name}}</option>
								@endforeach
							</optgroup>
						@endforeach
					</select>
					<span class="help-block text-danger">*Required</span>
				</div>
			</div>

			@include('form.file', [
				'field' => 'file_upload',
				'label' => 'Image',
				'attributes' => [
					'id' => 'file_upload'
				]
			])

			<div class="row col-md-offset-2">
				@if($article->image)
					<img src="{{route('article.admin.showThumbnail',$article->image)}}" alt="" style="max-width:15%">
				@endif
			</div>
		</div>
	</div>
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="form-group" id="_content">
				{!! $article->content !!}
			</div>
			<div class="form-group hidden" id="click2edit">
				<textarea class="form-control summernote" id="content" name="content"> {{ $article->content }}</textarea>
				<span class="help-block text-danger">*Required</span>
			</div>
		</div>
	</div>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title text-semiold">Comments<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="commentTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Content</th>
							<th>Action</th>
							<th>Created at</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<br/>
	<hr/>
		<div class="form-group text-center" style="margin-top: 10px;">
			{!! Form::hidden('is_updated','false' , array('id' => 'is_updated')) !!}
			@if($status == 'waiting')
				<button type="submit" id="save" class="btn btn-success hidden">Save<i class="icon-checkmark3 position-left"></i></button>
				<button type="button" id="edit" class="btn btn btn-primary">Edit <i class="icon-pencil3 position-left"></i></button>
				<button type="submit" id="approved" class="btn btn-primary-pink">Approved <i class="icon-floppy-disk position-left"></i></button>
			@endif
			<a href="{{ route('article.admin.approval') }}" class="btn btn-danger" >Cancel  <span class="icon-arrow-left52"></span></a>
		</div>

	{!! Form::close() !!}
	</div>

	
	{!! Form::hidden('id',$article->id, array('id' => 'article_id')) !!}
	{!! Form::hidden('tag',json_encode($array) , array('id' => 'tag')) !!}
@endsection

@section('content-js')
<script src="{{ mix('js/summernote.js') }}"></script>
<script src="{{ mix('js/tags.js') }}"></script>
<script>
	$('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });
	
	$('#edit').on('click', function() {
		$('#is_updated').val('edit');
        $('#save').removeClass( "hidden" );
        $('#click2edit').removeClass( "hidden" );
        $('#_content').addClass( "hidden" );
        $('#edit').addClass( "hidden" );
	})

	$('#approved').on('click', function() {
		$('#is_updated').val('approved');
	})

	$('#save').on('click', function() {
		$('#is_updated').val('edit');
	})
	
	$(document).ready( function () {
		var article_id = $('#article_id').val();
		$('#commentTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/article/approval/comment-data?id='+article_id,
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'content', name: 'content',searchable:true,orderable:true},
				{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
				{data: 'created_at', name: 'created_at',width:'50px',searchable:false,visible:false}
			],
			order:[ 
				[3, 'desc']
			],
		});
		
		var substringMatcher = function(strs) {
			return function findMatches(q, cb) {
				var matches, substringRegex;

				// an array that will be populated with substring matches
				matches = [];

				// regex used to determine if a string contains the substring `q`
				substrRegex = new RegExp(q, 'i');

				// iterate through the pool of strings and for any string that
				// contains the substring `q`, add it to the `matches` array
				$.each(strs, function(i, str) {
					if (substrRegex.test(str)) {

						// the typeahead jQuery plugin expects suggestions to a
						// JavaScript object, refer to typeahead docs for more info
						matches.push({ value: str });
					}
				});
				cb(matches);
			};
		};

		// Data
		var states = JSON.parse($('#tag').val());

		// Attach typeahead
		$('.tagsinput-typeahead').tagsinput('input').typeahead(
			{
				hint: true,
				highlight: true,
				minLength: 1
			},
			{
				name: 'states',
				displayKey: 'value',
				source: substringMatcher(states)
			}
		).bind('typeahead:selected', $.proxy(function (obj, datum) { 
			this.tagsinput('add', datum.value);
			this.tagsinput('input').typeahead('val', '');
		}, $('.tagsinput-typeahead')));
	});

	function hapus(url){
		bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
			if(result){
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
					}
				}).done(function ($result) {
					$("#alert_success").trigger("click", 'Data Berhasil hapus');
					$('#commentTable').DataTable().ajax.reload();
				});
			}
		});
	}
	
	$('#form').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var selected_tag = $('#selected_tag').val();
		var selected_category = $('#selected_category').val();
		var content = $('#content').val();
		
		if (!title) {
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		/*if (!selected_tag) {
			$("#alert_error").trigger("click", 'Please Type Tag First.');
			return false;
		}*/

		if (!selected_category) {
			$("#alert_error").trigger("click", 'Please Select Category First.');
			return false;
		}

		if (!content.replace(/<[^>]+>/g, '')) {
			$("#alert_error").trigger("click", 'Please Fill Content First.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
						

					},
					success: function (response) {
						$.unblockUI();
						var url = $('#url_article_index').attr('href');
						document.location.href = url;
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);
					}
				})
				.done(function ($result) {

				});
			}
		});
	});
</script>
@endsection
