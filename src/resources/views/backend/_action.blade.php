
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($detail_modal))
                <li><a href="#" onclick="detailModal('{!! $detail_modal !!}')" ><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($edit))
                <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
            @endif
        </ul>
    </li>
</ul>
