@extends('layouts.backend', ['menu_active' => 'subscribe'])

@section('title')
	<h4><i class="icon-exclamation position-left"></i> <span class="text-semibold">Subscribe</h4>
	<ul class="breadcrumb breadcrumb-caret position-right">
		<li><a href="{{ route('backend.home')}}">Subscribe</a></li>
		<li class="active">Data</li>
	</ul>
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
            <a href="{{route('subscribe.admin.export')}}" class="btn btn-default">Export All <i class="icon-file-download2 position-left"></i></a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="subscribeTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Email</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
<script>
	$(document).ready( function () {
		$('#subscribeTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-inspirasi-hijab/subscribe',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'email', name: 'email',searchable:true,orderable:true}
			]
		});
	});
</script>
@endsection
