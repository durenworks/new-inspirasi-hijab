@extends('layouts.master_layout')


@section('content')
    <div class="col-sm-offset-4 col-sm-4">
        <form method="POST" action="{{ route('admin.sendlink') }}">
            {{ csrf_field() }}

            <div class="panel panel-body login-form">
                <div class="text-center">
                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                    <h5 class="content-group">Forget Password <small class="display-block">Your credentials</small></h5>
                </div>

                <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}"">
                    <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Send Password Reset Request <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('content-js')
    <script type="text/javascript" src="{{ mix('js/login.js')}}"></script>
@endsection