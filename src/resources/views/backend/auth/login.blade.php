<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inspirasi Hijab | Admin</title> 

	<link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
	@yield("content-css")

	<script src="{{ mix('js/backend.js') }}"></script>
	
</head>

<body class="login-container pink-background">
   <div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
                    <form method="POST" action="{{ route('backend.do_login') }}">
                        {{ csrf_field() }}
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
								<h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
							</div>

							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
                                <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" required autofocus autocomplete="off">

								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary-pink btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="text-center">
								<a href="#">Forgot password?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
