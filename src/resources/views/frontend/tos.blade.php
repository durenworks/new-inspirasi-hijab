@extends('frontend.template')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb mt-md-5">
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li class="active"><a href="#">Terms of Service</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Sidebar -->
      <div class="col-md-4">
        <div class="list-group">
          <a href="#tos-1" class="list-group-item list-group-item-action d-flex align-items-center active" id="tos-1-tab" data-toggle="tab" href="#tos1" role="tab" aria-controls="home" aria-selected="true">Definis <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#tos-2" class="list-group-item list-group-item-action d-flex align-items-center" id="tos-2-tab" data-toggle="tab" href="#tos2" role="tab" aria-controls="home" aria-selected="false">Ketentuan Layanan <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#tos-3" class="list-group-item list-group-item-action d-flex align-items-center" id="tos-3-tab" data-toggle="tab" href="#tos3" role="tab" aria-controls="home" aria-selected="false">Ketentuan Konten <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#tos-4" class="list-group-item list-group-item-action d-flex align-items-center" id="tos-4-tab" data-toggle="tab" href="#tos4" role="tab" aria-controls="home" aria-selected="false">Ketentuan Perubahan <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#tos-5" class="list-group-item list-group-item-action d-flex align-items-center" id="tos-5-tab" data-toggle="tab" href="#tos5" role="tab" aria-controls="home" aria-selected="false">Undang Undang ITE  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
        </div>
      </div>
      <!-- End Sidebar -->

      <!-- Post -->
      <div class="col-md-8">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="faq-1" role="tabpanel" aria-labelledby="home-tab">
            <p>1. Mengacu pada Undang-Undang No. 11 tahun 2008 tentang Informasi dan Transaksi Elektronika (UU ITE) pasal 27, penulis dilarang keras menyalin penuh tulisan dari sumber lain tanpa mencantumkan sumber. Ide dan konten artikel tidak harus 100% orisinil, tetapi tetap harus mencantumkan sumber yang relevan</p>
            <p>2. Mengacu pada UU ITE pasal 27, artikel tidak boleh mengandung hoax, hate speech, konten asusila, atau hal apapun yang merugikan pihak lain</p>
            <p>3. Adapun komentar yang dibagikan di kolom artikel inspirasihijab.com, tidak boleh mengandung hoax, hate speech, konten asusila, spam komersil, atau hal apapun yang merugikan pihak lain. Apabila ditemukan komentar yang mengandung unsur-unsur diatas, maka akan dihapus oleh admin inspirasihijab.com tanpa pengecualian atau pemberitahuan terlebih dahulu</p>
            <p>4. Komunitas dapat mempublikasikan event di website inspirasihijab.com secara gratis dengan pengajuan maksimal 7 hari sebelum acara. Detail yang harus dilengkapi untuk publikasi event meliputi: Nama Komunitas, PIC Komunitas, Detail Pelaksanaan, Kuota Peserta, Tema Event, Fasilitas Event</p>
            <p>5. Inspirasihijab.com tidak memiliki tanggung jawab untuk memenuhi target peserta event komunitas.</p>
          </div>
          <div class="tab-pane fade" id="faq-2" role="tabpanel" aria-labelledby="profile-tab">FAQ 2</div>
          <div class="tab-pane fade" id="faq-3" role="tabpanel" aria-labelledby="contact-tab">FAQ 3</div>
          <div class="tab-pane fade" id="faq-4" role="tabpanel" aria-labelledby="contact-tab">FAQ 4</div>
          <div class="tab-pane fade" id="faq-5" role="tabpanel" aria-labelledby="contact-tab">FAQ 5</div>
        </div>
      </div>
      <!-- End Post -->
    </div>
  </div>
</section>
@endsection
