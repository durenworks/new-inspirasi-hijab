@extends('frontend.template')

@section('content')
<section id="header-3" class="mb-md-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="header-content">
          <ul class="breadcrumb mt-md-5">
            <li><a href="{{ route( 'homepage' ) }}">Home</a></li>
            <li>
              <a href="{{ route( 'category', $article->category->slug ) }}">
                {{ $article->category->name }}
              </a>
            </li>
            <li class="active"><a href="#">{{ $article->title }}</a></li>
          </ul>
          <div class="card mt-md-4 border-0">
            <div class="card-body text-center has--box-shadow">
              <a href="#" class="link-pink">{{ $article->category->name }}</a>
              <h1 class="heading-text my-md-4">{{ $article->title }}</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Post -->
      <div class="col-md-8">
        <div class="post-detail mt-md-5">
          {!! $article->content !!}
        </div>
        @if($ambasador)
          <div class="blockquotes orange">
            <div class="row d-flex align-items-center">
              <div class="col-sm-12 col-md-3">
                @if ($category->feature_image)
                  <img src="{{ route('media.feature_image', $category->feature_image) }}" alt="">
                @else
                  <img src="{{ asset('images/avatar.png') }}" alt="">
                @endif
                <div class="bio">
                    <span>{{ $ambasador->name }},</span>
                    <span>26th Vlogger/Influencer</span>
                </div>
              </div>
              <div class="col-sm-12 col-md-9">
                <p class="quotes">“{{ $category->quote }}”</p>
              </div>
            </div>
          </div>
        @endif
        <hr class="my-5">
        <div class="comments bg-light">
          <h5 class="heading-text font-weight-bolder mb-4">BERI KOMENTAR</h5>
          <div class="c__form">
            <form action="{{ route ('postComment',[$article->slug,$article->id])}}" method="post">
             {{ csrf_field() }}
              <div class="form-group">
                <textarea name="comments_text" class="form-control" id="" cols="30" rows="10" placeholder="Tulis Komentar Anda"></textarea>
              </div>
              <div class="form-group text-right">
                @guest
                <button type="submit" class="btn btn-lg btn-success">Kirim Komentar</button>
                @else
                <button type="submit" class="btn btn-lg btn-success">Kirim Komentar</button>
                @endguest
              </div>
            </form>
          </div>
          <div class="cl">
            @if ( $article->comment->count() > 0 )
              @foreach ( $article->comment as $comment )
                @if ( $comment->parent_id == 0 )
                <div class="cl__item">
                  <div class="cl__avatar">
                    <img src="{{ asset('images/avatar.png') }}" alt="">
                  </div>
                  <div class="cl__dt">
                    <div class="cl__dt__mt">
                      <b>Jhone Philipe</b>
                      <span class="text-muted">&nbsp;&nbsp; {{ $comment->created_at->format('d M Y') }} &nbsp;&nbsp; {{ $comment->created_at->format('H:i') }}</span>
                    </div>
                    <p>{{ $comment->content }}</p>
                    <div class="cl__dt__rpbtn">
                      <a href="#"><i class="fa fa-reply"></i> Balas</a>
                      <a href="#"><i class="fa fa-info"></i> Laporkan</a>
                    </div>

                    @if ( $comment->childs->count() > 0 )
                      @foreach ( $comment->childs as $subcomment )
                        <!-- Comments Reply -->
                        <div class="cl__rp">
                          <div class="cl__item">
                            <div class="cl__avatar">
                              <img src="{{ asset('images/avatar.png') }}" alt="">
                            </div>
                            <div class="cl__dt">
                              <div class="cl__dt__mt">
                                <b>Jhone Philipe</b>
                                <span class="text-muted">&nbsp;&nbsp; 7 April 2019 &nbsp;&nbsp; 20:55</span>
                              </div>
                              <p>{{ $subcomment->content }}</p>
                              <a href="#"></a>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    @endif
                  </div>
                </div>
                @endif
              @endforeach
            @endif
          </div>
        </div>
      </div>
      <!-- End Post -->

      <!-- Sidebar -->
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          <div class="widget">
            <div class="profile">
              @if($article->user->avatar)
                <a href="{{ route('profile',$article->user->url) }}">
                  <img
                    src="{{ route('media.avatar',$article->user->avatar ) }}"
                    alt="avatar"
                    style="width: 107px; height:107px; object-fit: contain"
                  >
                </a>
              @else
                <a href="{{ route('profile',$article->user->url) }}">
                  <img
                    src="{{ asset('images/avatar-default.png') }}"
                    alt=""
                    style="width: 107px; height:107px; object-fit: contain"
                  >
                </a>
              @endif
              {{-- <img src="{{ asset('images/avatar.png') }}" alt="avatar"> --}}
              <h6 class="font-weight-bold">{{ $article->user->name }}</h6>
              <p class="text-muted mb-2">Contributing Writer</p>
              <div class="d-flex align-items-center">
                <span class="pr-3">{{ $article->created_at->format('d M Y') }}</span>
                <div class="social flex-fill d-flex">
                  <a href="{{ $article->user->facebook_url }}" class="social-link flex-fill facebook"><i class="fab fa-facebook"></i></a>
                  <a href="{{ $article->user->twitter_url }}" class="social-link flex-fill twitter"><i class="fab fa-twitter"></i></a>
                  <a href="#" class="social-link flex-fill instagram"><i class="fab fa-instagram"></i></a>
                </div>

                @if ( Auth::user() )
                  @if(Auth::user()->id != $article->user->id)
                  @php
                    $status_follow = $article->user->CheckFollowStatus($article->user->id,Auth::guard('web')->user()->id);
                  @endphp
                  <a href="{{ route('myaccount.follow',[$article->user->id,$status_follow]) }}" class="btn btn-primary btn-sm flex-fill"
                    onclick="event.preventDefault();
                        document.getElementById('follow-form').submit();">
                    {{ $status_follow }}</a>
                    @if ($errors->has('follow'))
                        <br/>
                        <span>
                            <strong>{{ $errors->first('follow') }}</strong>
                        </span>
                    @endif
                    <form id="follow-form" action="{{ route('myaccount.follow',[$article->user->id,$status_follow]) }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  @else
                    <a href="{{route('myaccount.notifikasi',$article->user->url )}}" class="btn btn-primary btn-sm flex-fill">My Profile</a>
                  @endif
                @else
                  <a href="#" class="btn btn-primary btn-sm flex-fill" data-toggle="modal" data-target="#login">Follow</a>
                @endif
              </div>
            </div>
          </div>
          @include('frontend._sidebar')
        </div>
      </div>
      <!-- End Sidebar -->
    </div>
  </div>
</section>
@endsection
