<section id="penulis" class="bg-soft-pink mt-5">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <h3 class="heading-text font-weight-bold">PENULIS</h3>
      </div>
    </div>
    <div class="row slick-5-with-arrows">
      @foreach($users as $user)
        <div class="col">
          <div class="card writer">
            @if($user->avatar)
              <a href="{{ route('profile',$user->url) }}"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ route('media.avatar',$user->avatar ) }}" alt=""></a>
            @else
                <a href="{{ route('profile',$user->url) }}" class="text-center"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/avatar-default.jpeg ') }}" alt=""></a>
            @endif

            <div class="card-body text-center">
              <h6 class="card-title mb-0 font-weight-bolder px-3"><a href="{{ route('profile',$user->url) }}" style="color:black">{{ $user->name }}</a></h6>
              <p class="join-date mb-0">Bergabung : {{ $user->created_at->format('d/m/Y') }}</p>
              <p class="link"><a href="{{ route('profile',$user->url) }}" class="text-muted">{{ route('profile',$user->url) }}</a></p>
              <p class="card-text">
              {{ $user->description }}
              </p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <a href="{{ route('penulis') }}" class="btn btn-outline-success">Lihat Semua</a>
      </div>
    </div>
  </div>
</section>
