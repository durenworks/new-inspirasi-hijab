@extends('frontend.template')

@section('content')

<section class="account-page py-4">
  <div class="container">
    <div class="account-header">
      <div class="cover">
        <img src="{{ asset('images/account-page-header.jpg') }}" alt="">
      </div>
      <div class="account-profile d-flex align-items-center p-4">
        <div class="account-avatar col-md-3 pr-md-3">
          @if($profile->avatar)
            <img src="{{ route('media.avatar', $profile->avatar) }}" class="float-right" alt="">
          @else
            <img src="{{ asset('images/avatar-default.png') }}" class="float-right" alt="">
          @endif
        </div>
        <div class="account-detail col">
          @if ( ! isset($profile) )
            <h3 class="font-weight-bold"> {{ ucwords(Auth::guard('web')->user()->name)}} </h3>
          @else
            <h3 class="font-weight-bold"> {{ $profile->name }} </h3>
          @endif
          <div class="d-flex align-items-center">
            <span class="mr-md-3">
              @if ( !isset($profile) )
                <small> <i class="far fa-calendar-alt"></i> Bergabung:  {{ Auth::guard('web')->user()->created_at->format('d/m/Y') }}</small>
              @else
                <small> <i class="far fa-calendar-alt"></i> Bergabung:  {{ $profile->created_at->format('d/m/Y') }}</small>
              @endif
            </span>
            <div class="social d-flex">
              @if($profile->facebook_url)
                <a href="{{ $profile->facebook_url }}" class="social-link mr-3 facebook">
                  <i class="fab fa-facebook"></i>
                </a>
              @endif
              @if($profile->twitter_url)
                <a href="{{ $profile->twitter_url }}" class="social-link mr-3 twitter">
                  <i class="fab fa-twitter"></i>
                </a>
              @endif
              @if($profile->instagram_url)
                <a href="{{ $profile->instagram_url }}" class="social-link mr-3 instagram">
                  <i class="fab fa-instagram"></i>
                </a>
              @endif
              @if($profile->youtube_url)
                <a href="{{ $profile->youtube_url }}" class="social-link mr-3 instagram">
                  <i class="fab fa-youtube"></i>
                </a>
              @endif
            </div>
          </div>
        </div>
        <div class="account-button col">
          @php
            $authId = null;
            if(Auth::user())
              $authId = Auth::user()->id;
          @endphp
          @if(!$authId)
          <a
            href="#"
            data-toggle="modal"
            data-target="#login"
            class="btn btn-primary">follow</a>
          @elseif($authId != $profile->id)
              @php
              $status_follow = 'follow';
              if($authId)
                $status_follow = $profile->CheckFollowStatus($profile->id,Auth::guard('web')->user()->id);
              @endphp
              <a href="{{ route('myaccount.follow',[$profile->id,$status_follow]) }}" class="btn btn-primary"
              onclick="event.preventDefault();
                  document.getElementById('follow-form').submit();">
              {{ $status_follow }}</a>
              @if ($errors->has('follow'))
                  <br/>
                  <span>
                      <strong>{{ $errors->first('follow') }}</strong>
                  </span>
              @endif
              <form id="follow-form" action="{{ route('myaccount.follow',[$profile->id,$status_follow]) }}" method="POST" style="display: none;">
              {{ csrf_field() }}
              </form>
          @elseif ( Auth::user() && Auth::user()->id == $profile->id )
            <a href="{{ route('myaccount.pengaturan',Auth::guard('web')->user()->url) }}" class="btn btn-outline-success float-right">Edit Profile</a>
          @endif
        </div>
      </div>
    </div>
    <div class="account-content">
      <div class="row">
        @yield('account-content')
      </div>
    </div>
  </div>
</section>

@endsection
