@extends('frontend.template')

@section('content')
<section id="header" class="mb-md-5">
  <div id="carouselHeader" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselIHeader data-slide-to="0" class="active"></li>
      <li data-target="#carouselIHeader data-slide-to="1"></li>
      <li data-target="#carouselIHeader data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
              <div class="pr-md-5">
                <div class="post-item post-vertical p-4">
                  <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li><a href="#">FASHION HIJAB</a></li>
                    </ul>
                  </div>
                  <h2 class="post-item-heading"><a href="#">Be Confidence With Azalea Hijab Shampoo Anti Dandruff & Hair Mist</a></h2>
                  <div class="post-item-footer pt-3">
                    <span class="text-muted float-right">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="post-section" class="mb-md-5">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="list-posts pr-md-3 mt-5">
          <div class="list-post-heading d-flex align-items-center mb-5">
            <h3 class="heading-text has-line font-weight-bold">MOST POPULAR</h3>
            <a href="#" class="list-post--show-all ml-auto">Lihat Semua</a>
          </div>
          <div class="list-post-content">
            <div class="post-item post-vertical">
              <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-1.png') }}" alt="">
              </div>
              <div class="post-item-content">
                <ul class="post-item-categories">
                  <li><a href="#">FASHION HIJAB</a></li>
                </ul>
                <h4 class="post-item-heading"><a href="#">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                <a href="#" class="read-more">Continue Reading</a>
                <div class="post-item-footer pt-3">
                  <span class=" text-muted">27/01/2019</span>
                  <span class="text-muted">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                </div>
              </div>
            </div>
            <div class="post-item post-vertical">
              <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-2.png') }}" alt="">
              </div>
              <div class="post-item-content">
                <ul class="post-item-categories">
                  <li><a href="#">FASHION HIJAB</a></li>
                </ul>
                <h4 class="post-item-heading"><a href="#">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                <a href="#" class="read-more">Continue Reading</a>
                <div class="post-item-footer pt-3">
                  <span class=" text-muted">27/01/2019</span>
                  <span class="text-muted">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                </div>
              </div>
            </div>
            <div class="post-item post-vertical">
              <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-3.png') }}" alt="">
              </div>
              <div class="post-item-content">
                <ul class="post-item-categories">
                  <li><a href="#">FASHION HIJAB</a></li>
                </ul>
                <h4 class="post-item-heading"><a href="#">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                <a href="#" class="read-more">Continue Reading</a>
                <div class="post-item-footer pt-3">
                  <span class=" text-muted">27/01/2019</span>
                  <span class="text-muted">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                </div>
              </div>
            </div>
            <div class="post-item post-vertical">
              <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-4.png') }}" alt="">
              </div>
              <div class="post-item-content">
                <ul class="post-item-categories">
                  <li><a href="#">FASHION HIJAB</a></li>
                </ul>
                <h4 class="post-item-heading"><a href="#">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                <a href="#" class="read-more">Continue Reading</a>
                <div class="post-item-footer pt-3">
                  <span class=" text-muted">27/01/2019</span>
                  <span class="text-muted">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          <div class="widget">
            <div class="cta-write pt-5 pb-4 px-4 text-center d-flex justify-content-center align-items-end">
              <a href="#" class="btn btn-success rounded-0 mt-5">TULIS ARTIKELMU DISINI</a>
            </div>
          </div>
          <div class="widget">
            <h5 class="widget-heading">MOST LIKED ARTICLE</h5>
            <div class="list-thumbnail">
              <div class="thumbnail-item">
                <div class="thumbnail-image">
                  <a href="#">
                    <img src="{{ asset('images/artikel-image-1.png') }}" class="img-fluid" alt="image">
                  </a>
                </div>
                <div class="thumbnail-content">
                  <h6 class="thumbnail-heading"><a href="#">Lorem ipsum dolor sit per sempre</a></h6>
                  <p class="thumbnail-footer">
                    <a href="#" class="text-muted font-italic"> - Dedjumadilakir</a>
                    <span class="viewer"><i class="fa fa-eye"></i> 100</span>
                  </p>
                </div>
              </div>
              <div class="thumbnail-item">
                <div class="thumbnail-image">
                  <a href="#">
                    <img src="{{ asset('images/artikel-image-1.png') }}" class="img-fluid" alt="image">
                  </a>
                </div>
                <div class="thumbnail-content">
                  <h6 class="thumbnail-heading"><a href="#">Lorem ipsum dolor sit per sempre</a></h6>
                  <p class="thumbnail-footer">
                    <a href="#" class="text-muted font-italic"> - Dedjumadilakir</a>
                    <span class="viewer"><i class="fa fa-eye"></i> 100</span>
                  </p>
                </div>
              </div>
              <div class="thumbnail-item">
                <div class="thumbnail-image">
                  <a href="#">
                    <img src="{{ asset('images/artikel-image-1.png') }}" class="img-fluid" alt="image">
                  </a>
                </div>
                <div class="thumbnail-content">
                  <h6 class="thumbnail-heading"><a href="#">Lorem ipsum dolor sit per sempre</a></h6>
                  <p class="thumbnail-footer">
                    <a href="#" class="text-muted font-italic"> - Dedjumadilakir</a>
                    <span class="viewer"><i class="fa fa-eye"></i> 100</span>
                  </p>
                </div>
              </div>
              <div class="thumbnail-item">
                <div class="thumbnail-image">
                  <a href="#">
                    <img src="{{ asset('images/artikel-image-1.png') }}" class="img-fluid" alt="image">
                  </a>
                </div>
                <div class="thumbnail-content">
                  <h6 class="thumbnail-heading"><a href="#">Lorem ipsum dolor sit per sempre</a></h6>
                  <p class="thumbnail-footer">
                    <a href="#" class="text-muted font-italic"> - Dedjumadilakir</a>
                    <span class="viewer"><i class="fa fa-eye"></i> 100</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="widget">
            <a href="#">
              <img src="{{ asset('images/iklan.png') }}" class="img-fluid" alt="Iklan">
            </a>
          </div>
          <div class="widget">
            <h5 class="widget-heading">FOLLOW & LIKE US ON</h5>
            <div class="social d-flex">
              <a href="#" class="social-link flex-fill facebook"><i class="fab fa-facebook fa-2x"></i></a>
              <a href="#" class="social-link flex-fill twitter"><i class="fab fa-twitter fa-2x"></i></a>
              <a href="#" class="social-link flex-fill instagram"><i class="fab fa-instagram fa-2x"></i></a>
              <a href="#" class="social-link flex-fill youtube"><i class="fab fa-youtube fa-2x"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
