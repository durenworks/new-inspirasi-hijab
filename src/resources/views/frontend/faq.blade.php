@extends('frontend.template')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb mt-md-5">
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li class="active"><a href="#">FAQ</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Sidebar -->
      <div class="col-md-4">
        <div class="list-group">
          <a href="#faq-1" class="list-group-item list-group-item-action d-flex align-items-center active" id="faq-1-tab" data-toggle="tab" href="#faq1" role="tab" aria-controls="home" aria-selected="true">Perihal Konten  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#faq-2" class="list-group-item list-group-item-action d-flex align-items-center" id="faq-2-tab" data-toggle="tab" href="#faq2" role="tab" aria-controls="home" aria-selected="false">Perihan Tehnis dan Gangguan  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#faq-3" class="list-group-item list-group-item-action d-flex align-items-center" id="faq-3-tab" data-toggle="tab" href="#faq3" role="tab" aria-controls="home" aria-selected="false">Tips dan Tutorial  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#faq-4" class="list-group-item list-group-item-action d-flex align-items-center" id="faq-4-tab" data-toggle="tab" href="#faq4" role="tab" aria-controls="home" aria-selected="false">Perihal Kerjasama  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
          <a href="#faq-5" class="list-group-item list-group-item-action d-flex align-items-center" id="faq-5-tab" data-toggle="tab" href="#faq5" role="tab" aria-controls="home" aria-selected="false">Bantuan  <i class="fas fa-arrow-right float-right ml-auto"></i></a>
        </div>
      </div>
      <!-- End Sidebar -->

      <!-- Post -->
      <div class="col-md-8">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="faq-1" role="tabpanel" aria-labelledby="home-tab">
            <div class="accordion" id="accordionExample">
              <div class="card card-faq">
                <div class="card-header" id="accord-heading-1">
                  <a href="#" class="d-flex align-items-center" data-toggle="collapse" data-target="#accord-1" aria-expanded="true" aria-controls="accord-1">
                    Apa itu Inspirasi Hijab?
                    <i class="fas fa-sort-down ml-auto"></i>
                  </a>
                </div>

                <div id="accord-1" class="collapse show" aria-labelledby="accord-heading-1" data-parent="#accordionExample">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card card-faq">
                <div class="card-header" id="accord-heading-2">
                  <a href="#" class="d-flex align-items-center" data-toggle="collapse" data-target="#accord-2" aria-expanded="true" aria-controls="collapseOne">
                    Siapa yang mengisi dan menayangkan konten di Inspirasi Hijab?
                    <i class="fas fa-sort-down ml-auto"></i>
                  </a>
                </div>

                <div id="accord-1" class="collapse" aria-labelledby="accord-heading-2" data-parent="#accordionExample">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="faq-2" role="tabpanel" aria-labelledby="profile-tab">FAQ 2</div>
          <div class="tab-pane fade" id="faq-3" role="tabpanel" aria-labelledby="contact-tab">FAQ 3</div>
          <div class="tab-pane fade" id="faq-4" role="tabpanel" aria-labelledby="contact-tab">FAQ 4</div>
          <div class="tab-pane fade" id="faq-5" role="tabpanel" aria-labelledby="contact-tab">FAQ 5</div>
        </div>
      </div>
      <!-- End Post -->
    </div>
  </div>
</section>
@endsection
