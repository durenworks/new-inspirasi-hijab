@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    @include('frontend.account._nav')

    <div class="box mt-3">
      <div class="box-content row d-flex align-items-center">
        <div class="col">
            <p class="mb-0">EVENT</p>
        </div>
        <div class="col">
            <a href="{{ route( 'community.event.create', Auth::guard('web')->user()->url ) }}">+ BUAT EVENT</a>
        </div>
        <div class="col">
            <div class="d-flex align-items-center ml-auto">
            <p class="mb-0 text-muted mr-3">Tampilkan</p>
            <ul class="nav nav-pills nav-filter">
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">All (Default)</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">All (Default)</a>
                    <a class="dropdown-item" href="#">Artikel Terbaru</a>
                    <a class="dropdown-item" href="#">Terbanyak Dibaca</a>
                    <a class="dropdown-item" href="#">Terbanyak Dikomentari</a>
                    <a class="dropdown-item" href="#">Terbanyak Dibagikan</a>
                </div>
                </li>
            </ul>
            </div>
        </div>
      </div>
    </div>
    <div class="list-posts mt-3">
      <div class="list-post-content">
        <div class="post-item post-vertical event">
            <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-1.png') }}" alt="">
            </div>
            <div class="post-item-content m-0">
                <div class="p-3">
                    <ul class="post-item-categories">
                    <li><a href="#">AKAN BERLANGSUNG</a></li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route('event.detail') }}">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                    <a href="{{ route('event.detail') }}" class="read-more">Continue Reading</a>
                </div>
                <div class="post-item-footer border-0 bg-soft-pink py-3 px-4 mt-2">
                    <div class="row">
                        <div class="col">
                            <span class="d-block">KAMIS, 27 Mei 2019</span>
                            <span class="d-block"><i class="far fa-clock"></i> 10.00 PM</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <div class="d-block text-center my-5">
      <button class="btn btn-outline-success">Selanjutnya</button>
    </div>
  </div>
@endsection
