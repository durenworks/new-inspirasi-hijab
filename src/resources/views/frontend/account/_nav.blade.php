<div class="filters mt-4">
  <ul class="nav nav-pills nav-fill nav-account">
    <li class="nav-item">
      <a class="nav-link  {{ $nav == 'notifikasi' ? 'active' : '' }}" href="{{ route('myaccount.notifikasi',Auth::guard('web')->user()->url) }}">Notifikasi</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  {{ $nav == 'artikel' ? 'active' : '' }}" href="{{ route('myaccount.artikel',Auth::guard('web')->user()->url) }}">Artikel</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  {{ $nav == 'draft' ? 'active' : '' }}" href="{{ route('myaccount.draft',Auth::guard('web')->user()->url) }}">Draft</a>
    </li>
    @if ( Auth::user()->role == 'community' )
    <li class="nav-item">
      <a class="nav-link  {{ $nav == 'event' ? 'active' : '' }}" href="{{ route('community.event',Auth::guard('web')->user()->url) }}">Event</a>
    </li>
    @endif
    <li class="nav-item">
      <a class="nav-link  {{ $nav == 'pengaturan' ? 'active' : '' }}" href="{{ route('myaccount.pengaturan',Auth::guard('web')->user()->url) }}">Pengaturan</a>
    </li>
  </ul>
</div>
