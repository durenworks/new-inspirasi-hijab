@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    @include('frontend.account._nav')

    <div class="box mt-3">
      <div class="box-content d-flex align-items-center">
        <p class="mb-0">PENGATURAN AKUN ANDA</p>
      </div>
    </div>

    <div class="mt-3 form-account">
      {{ 
        Form::open([
          'method' => 'PUT',
          'id' => 'insertProfileform',
          'class' => 'form-horizontal',
          'url' => route('myaccount.update.pengaturan',[Auth::guard('web')->user()->url,Auth::guard('web')->user()->id] ),
          'enctype' => 'multipart/form-data',
        ]) 
      }}
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="{{ Auth::guard('web')->user()->name }}">
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="milkaceria@gmail.com" value="{{ Auth::guard('web')->user()->email }}" readonly="readonly">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Hp</label>
          <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phoneHelp" placeholder="0812981xxxxx" value="{{ Auth::guard('web')->user()->phone }}">
        </div>
        <div class="form-group">
          <label for="password">Ubah Password Masuk </label>
          <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Password Lama">
          @if ($errors->has('old_password'))
              <span class="help-block">
                  <strong>{{ $errors->first('old_password') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="new_password" name="password" placeholder="Password Baru">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="Ketik Ulang Password Baru">
          @if ($errors->has('retype_password'))
              <span class="help-block">
                  <strong>{{ $errors->first('retype_password') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="url">URL Profile</label>
          <input type="text" class="form-control" id="url" name="url" placeholder="milkaceria" value="{{ Auth::guard('web')->user()->url }}">
          <small id="emailHelp" class="form-text text-muted">https://www.inspirasihijab.com/…</small>
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
          <label for="deskription">Deskripsi Profile</label>
          <textarea name="description" class="form-control" id="description">{{ Auth::guard('web')->user()->description }}</textarea>
        </div>
        <div class="form-group">
          <label for="facebook">Facebook URL</label>
          <input type="text" class="form-control" id="facebook_url" name="facebook_url" placeholder="Masukan Facebook url Anda" value="{{ Auth::guard('web')->user()->facebook_url }}">
          <small id="emailHelp" class="form-text text-muted"><sup>*</sup>Contoh: https://www.facebook.com/inspirasihijab.com</small>
        </div>
        <div class="form-group">
          <label for="youtube">Youtube URL</label>
          <input type="text" class="form-control" id="youtube_url" name="youtube_url" placeholder="Masukan Youtube url Anda" value="{{ Auth::guard('web')->user()->youtube_url }}">
          <small id="emailHelp" class="form-text text-muted"><sup>*</sup>Contoh: https://www.youtube.com/inspirasihijab.com</small>
        </div>
        <div class="form-group">
          <label for="instagram">Instagram URL</label>
          <input type="text" class="form-control" id="instagram_url" name="instagram_url" placeholder="Masukan Instagram url Anda" value="{{ Auth::guard('web')->user()->instagram_url }}">
          <small id="emailHelp" class="form-text text-muted"><sup>*</sup>Contoh: https://www.instagram.com/inspirasihijab.com</small>
        </div>
        <div class="form-group">
          <label for="twitter">Twitter URL</label>
          <input type="text" class="form-control" id="twitter_url" name="twitter_url" placeholder="Masukan Twitter url Anda" value="{{ Auth::guard('web')->user()->twitter_url }}">
          <small id="emailHelp" class="form-text text-muted"><sup>*</sup>Contoh: https://www.twitter.com/inspirasihijab.com</small>
        </div>
        <div class="form-group">
            <label for="avatar">Avatar</label>
            <input type="file" class="form-control" name="avatar">
        </div>
        <button type="submit" class="btn btn-outline-success float-right" id="btn_submit" >Simpan</button>
      {{ Form::close() }}
    </div>
  </div>
@endsection
