@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    <div class="filters d-flex align-items-center p-3 mt-4">
      <p class="mb-0 text-muted">Artikel</p>
      <div class="d-flex align-items-center ml-auto">
        <p class="mb-0 text-muted mr-3">Tampilkan</p>
        <ul class="nav nav-pills nav-filter">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">All (Default)</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="#">All (Default)</a>
              <a class="dropdown-item" href="#">Artikel Terbaru</a>
              <a class="dropdown-item" href="#">Terbanyak Dibaca</a>
              <a class="dropdown-item" href="#">Terbanyak Dikomentari</a>
              <a class="dropdown-item" href="#">Terbanyak Dibagikan</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="list-posts mt-4">
      <div class="list-post-content" id="load-data">
        @php
          $articles = $profile->article()->limit(4)->get();
        @endphp
        @foreach ( $articles as $article )
          <div class="post-item post-vertical">
            <div class="post-item-image">
              @if($article->image)
                <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
              @else
                <img src="{{ asset('images/default-image.png') }}" alt="">
              @endif
            </div>
            <div class="post-item-content">
              <ul class="post-item-categories">
                <li>
                  <a href="{{ route('category', $article->category->slug ) }}">
                    {{ $article->category->name }}
                  </a>
                </li>
              </ul>
              <h4 class="post-item-heading">
                <a href="{{ route( 'detail', $article->slug ) }}">
                  {{ $article->title }}
                </a>
              </h4>
              <p>{{ $article->excerpt }}</p>
              <div class="post-item-footer pt-3 mt-auto">
                <span>{{ $article->created_at->format('d/m/Y') }}</span>
                <span class="font-italic">
                  &nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">
                    {{ $article->user->last_name }}
                  </a>
                </span>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    @if($articles->count() > 3)
      <div class="d-block text-center my-5">
        <button
          class="btn btn-outline-success"
          id="btnLoadMore"
          data-id="{{ $article->id }}"
          data-author="{{ $profile->url }}"
        >Selanjutnya</button>
      </div>
    @endif
  </div>
@endsection

@section('scripts')
  <script>
    $('#btnLoadMore').on('click', function(){
      const id = $(this).data('id')
      const author = $(this).data('author')
      $.ajax({
          url : '{{ route("article.load_more") }}',
          method : "POST",
          data : {
            id:id,
            author: author,
            _token:"{{csrf_token()}}"
          },
          dataType : "text",
          success : function (data)
          {
            var data = JSON.parse(data)
            if(data.el != '')
            {
                // $('#btnLoadMore').remove();
                $('#load-data').append(data.el);
            }
            else
            {
                $('#btn-more').html("No Data");
            }

            if(data.nextId){
              $('#btnLoadMore').data('id', data.nextId)
            }else{
              $('#btnLoadMore').remove()
            }
          }
      });
    })
  </script>
@endsection
