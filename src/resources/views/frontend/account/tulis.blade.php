@extends('frontend.account-template')

@section('style')
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote.css') }}">
<link rel="stylesheet" href="{{ mix('css/frontend.css') }}">
@endsection

@section('account-content')
    <div class="col-md-12">
      <form action="{{ route('myaccount.article.store', $profile->url) }}" method="POST">
        {{ csrf_field() }}
        <div class="box mt-3">
          <div class="box-header d-flex align-items-center">
            <p class="mb-0">Judul Artikel</p>
          </div>
          <div class="box-content">
            @include('form.text', [
              'field' => 'title',
              'label' => 'Nama',
              'placeholder' => 'Silahkan masukan nama article',
              'required' => '*Required',
              'attributes' => [
                'id' => 'title'
              ]
            ])

            <div class="form-group">
              <label for="tag" class="control-label col-lg-2 text-semibold">Tags</label>
              <div class="col-lg-10">
                <input type="text" name="selected_tag" id="selected_tag" data-role="tagsinput" class="tagsinput-typeahead form-control">
                <span class="help-block">Tekan ENTER untuk memisahkan label</span>
              </div>
            </div>

            <div class="form-group">
              <label for="category" class="control-label col-lg-2 text-semibold">
                Category
              </label>
              <div class="col-lg-4">
                <select name="selected_category" id="selected_category" data-placeholder="Please Select Category First" class="select-clear form-control">
                  <option value=""></option>
                  @foreach($categories as $category)
                    <optgroup label="{{ $category->name }}">
                      @foreach($category->childs as $subcategory)
                      <option value="{{ $subcategory->id }}">{{$subcategory->name}}</option>
                      @endforeach
                    </optgroup>
                  @endforeach
                </select>
                <span class="help-block text-danger">*Required</span>
              </div>
            </div>

            @include('form.file', [
              'field' => 'file_upload',
              'label' => 'Image',
              'attributes' => [
                'id' => 'file_upload'
              ]
            ])
          </div>
        </div>
        <div class="box mt-3">
          <div class="box-header d-flex align-items-center">
            <p class="mb-0">Tulis Artikel</p>
          </div>
          <div class="box-content">
            <textarea class="form-control summernote" id="content" name="content"></textarea>
          </div>
        </div>
        <div class="text-right my-4">
          <button class="btn btn-outline-success" type="submit">Simpan Draft</button>
          <button class="btn btn-success" type="submit">POST UPLOAD</button>
        </div>
      </form>
		</div>
		{!! Form::hidden('tag',json_encode($array) , array('id' => 'tag')) !!}
@endsection

@section('scripts')
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/select2.js') }}"></script>
<script src="{{ mix('js/summernote.js') }}"></script>
<script src="{{ mix('js/tags.js') }}"></script>
<!--<script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>-->
<script>
	$('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });

	$(document).ready( function () {
		var substringMatcher = function(strs) {
			return function findMatches(q, cb) {
				var matches, substringRegex;

				// an array that will be populated with substring matches
				matches = [];

				// regex used to determine if a string contains the substring `q`
				substrRegex = new RegExp(q, 'i');

				// iterate through the pool of strings and for any string that
				// contains the substring `q`, add it to the `matches` array
				$.each(strs, function(i, str) {
					if (substrRegex.test(str)) {

						// the typeahead jQuery plugin expects suggestions to a
						// JavaScript object, refer to typeahead docs for more info
						matches.push({ value: str });
					}
				});
				cb(matches);
			};
		};

		// Data
		var states = JSON.parse($('#tag').val());

		// Attach typeahead
		$('.tagsinput-typeahead').tagsinput('input').typeahead(
			{
				hint: true,
				highlight: true,
				minLength: 1
			},
			{
				name: 'states',
				displayKey: 'value',
				source: substringMatcher(states)
			}
		).bind('typeahead:selected', $.proxy(function (obj, datum) {
			this.tagsinput('add', datum.value);
			this.tagsinput('input').typeahead('val', '');
		}, $('.tagsinput-typeahead')));

        // Select
        $('.selected_category').select2();
	});

	$('#form').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var selected_tag = $('#selected_tag').val();
		var selected_category = $('#selected_category').val();
		var content = $('#content').val();

		if (!title) {
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		/*if (!selected_tag) {
			$("#alert_error").trigger("click", 'Please Type Tag First.');
			return false;
		}*/

		if (!selected_category) {
			$("#alert_error").trigger("click", 'Please Select Category First.');
			return false;
		}

		if (!content.replace(/<[^>]+>/g, '')) {
			$("#alert_error").trigger("click", 'Please Fill Content First.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});


					},
					success: function (response) {
						$.unblockUI();
						var url = $('#url_article_index').attr('href');
						document.location.href = url;
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);
					}
				});
			}
		});
	});
</script>
@endsection
