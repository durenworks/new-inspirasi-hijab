@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    @include('frontend.account._nav')

    <div class="box mt-3">
      <div class="d-flex align-items-center">
        <p class="mb-0">PEMBERITAHUAN</p>
        {{--<div class="d-flex align-items-center ml-auto">
          <p class="mb-0 text-muted mr-3">Tampilkan</p>
          <ul class="nav nav-pills nav-filter">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">All (Default)</a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">All (Default)</a>
                <a class="dropdown-item" href="#">Artikel Terbaru</a>
                <a class="dropdown-item" href="#">Terbanyak Dibaca</a>
                <a class="dropdown-item" href="#">Terbanyak Dikomentari</a>
                <a class="dropdown-item" href="#">Terbanyak Dibagikan</a>
              </div>
            </li>
          </ul>
        </div>--}}
      </div>
      @foreach($notification_comments as $notification)
          <div class="box-content d-flex align-items-center">
              <i class="fa fa-comments fa-fw text-primary mr-2"></i> {!! $notification->message !!}
              <span class="badge badge-danger ml-2 rounded-circle">{{ $notification->total }}</span>
              <a href="{{ route('myaccount.updateNotifikasi',[$profile->url,'-1','comment']) }}" class="ml-auto"
                  onclick="event.preventDefault();
                      document.getElementById('follow-notification-2').submit();">
                  Lihat Komentar ></a>

              <form id="follow-notification-2" action="{{ route('myaccount.updateNotifikasi',[$profile->url,'-1','comment']) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                <input type="hidden" name="total" value="{{ $notification->total }}"/>
                <input type="hidden" name="message" value="{{ $notification->message }}"/>
              </form>
          </div>
      @endforeach
    </div>
    {{--<div class="box mt-3">
      <div class="box-content d-flex align-items-center">
          <i class="fa fa-envelope fa-fw text-primary mr-2"></i> Pesan Masuk
          <span class="badge badge-danger ml-2 rounded-circle">6</span>
          <a href="#" class="ml-auto">Lihat Pesan <i class="fas fa-angle-right fa-fw"></i></a>
      </div>
    </div>--}}
    @foreach($notification_followers as $notification)
      @if($notification->type == 'view')
        <div class="box mt-3">
          <div class="box-content d-flex align-items-center">
              <i class="fa fa-eye fa-fw text-primary mr-2"></i> {!! $notification->message !!}
              <a href="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'view']) }}" class="ml-auto"
                    onclick="event.preventDefault();
                        document.getElementById('follow-notification-3').submit();">
                    Lihat Artikel ></a>
                <form id="follow-notification-3" action="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'view']) }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
          </div>
        </div>
      @endif

      @if($notification->type == 'user-follow')
        <div class="box mt-3">
            <div class="box-content d-flex align-items-center">
                <i class="fa fa-user-plus fa-fw text-primary mr-2"></i> {{ $notification->message }}
                <a href="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'user-follow']) }}" class="ml-auto"
                    onclick="event.preventDefault();
                        document.getElementById('follow-notification').submit();">
                    Lihat Following ></a>
                <form id="follow-notification" action="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'user-follow']) }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>

            </div>
        </div>
      @endif

      @if($notification->type == 'new-article')
        <div class="box mt-3">
            <div class="box-content d-flex align-items-center">
                <i class="fa fa-user-plus fa-fw text-primary mr-2"></i> {!! $notification->message !!}
                <a href="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'new-article']) }}" class="ml-auto"
                    onclick="event.preventDefault();
                        document.getElementById('follow-notification-4').submit();">
                    Lihat Article ></a>
                <form id="follow-notification-4" action="{{ route('myaccount.updateNotifikasi',[$profile->url,$notification->id,'new-article']) }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>

            </div>
        </div>
      @endif
    @endforeach



    <div class="d-block text-center my-5">
      <button class="btn btn-outline-success">Selanjutnya</button>
    </div>
  </div>
@endsection
