@extends('frontend.account-template')

@section('style')
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
@endsection

@section('account-content')
    <div class="col-md-12">
      <form action="{{ route( 'myaccount.article.store', Auth::guard('web')->user()->url ) }}" method="POST">
        {{ csrf_field() }}
        <div class="box mt-3">
          <div class="box-header d-flex align-items-center">
            <h6 class="mb-0 font-weight-bold">Buat Event</h6>
          </div>
          <div class="box-content">
            <div class="row">
                <div class="col-12">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama Event</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="nama">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label for="tanggal" class="col-sm-4 col-form-label">Tanggal Event</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control datepicker" id="tanggal">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="waktu" class="col-sm-6 col-form-label">Waktu Event</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control timepicker">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group row">
                        <label for="waktu" class="col-sm-2 col-form-label">s.d</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control timepicker" id="end">
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="box mt-3">
          <div class="box-header d-flex align-items-center">
          <h6 class="mb-0 font-weight-bold">Tulis Artikel</h6>
          </div>
          <div class="box-content">
            <textarea class="form-control summernote" id="content" name="content"></textarea>
          </div>
        </div>
        <div class="text-right my-4">
          <button class="btn btn-outline-success" type="submit">Simpan Draft</button>
          <button class="btn btn-success" type="submit">POST UPLOAD</button>
        </div>
      </form>
    </div>
@endsection

@section('scripts')
<script src="{{ mix('js/backend.js') }}"></script>
<script src="{{ mix('js/select2.js') }}"></script>
<script src="{{ mix('js/summernote.js') }}"></script>
<script src="{{ mix('js/tags.js') }}"></script>
<script src="{{ mix('fe/vendor/moment/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!--<script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>-->
<script type="text/javascript">

    $('.timepicker').datetimepicker({
        format: 'LT'
    });

	$('.summernote').summernote({
        height: 300,
        toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            [ 'fontname', [ 'fontname' ] ],
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'table', [ 'table' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY/MM/DD'
    });

	$('#form').submit(function (event) {
		event.preventDefault();
		var title = $('#title').val();
		var selected_tag = $('#selected_tag').val();
		var selected_category = $('#selected_category').val();
		var content = $('#content').val();

		if (!title) {
			$("#alert_error").trigger("click", 'Please Type Title First.');
			return false;
		}

		/*if (!selected_tag) {
			$("#alert_error").trigger("click", 'Please Type Tag First.');
			return false;
		}*/

		if (!selected_category) {
			$("#alert_error").trigger("click", 'Please Select Category First.');
			return false;
		}

		if (!content.replace(/<[^>]+>/g, '')) {
			$("#alert_error").trigger("click", 'Please Fill Content First.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});


					},
					success: function (response) {
						$.unblockUI();
						var url = $('#url_article_index').attr('href');
						document.location.href = url;
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) alert(response.responseJSON);
					}
				});
			}
		});
	});
</script>
@endsection
