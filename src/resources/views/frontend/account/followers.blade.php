@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    <div class="filters d-flex align-items-center p-3 mt-4">
      <p class="mb-0 text-muted">Followers &nbsp;<span class="text-primary">100</span></p>
    </div>
    <div class="row mt-3">
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card writer mb-md-3">
          <img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/artikel-image-1.png') }}" alt="">
          <div class="card-body text-center">
            <h6 class="card-title mb-0 font-weight-bolder">Milka Ceria</h6>
            <p class="join-date mb-0">Bergabung : 01/01/2019</p>
            <p class="link"><a href="#" class="text-muted">https://www.inspirasihijab.com/milkaceria</a></p>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae mattis dolor, sed faucibus turpis.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="d-block text-center my-5">
      <button class="btn btn-outline-success">Selanjutnya</button>
    </div>
  </div>
@endsection
