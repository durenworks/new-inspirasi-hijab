@extends('frontend.account-template')

@section('account-content')
  @include('frontend.account._leftside')

  <div class="col-md-8">
    @include('frontend.account._nav')

    <div class="box mt-3">
      <div class="box-content d-flex align-items-center">
        <p class="mb-0">KUMPULAN ARTIKEL ANDA</p>
        <div class="d-flex align-items-center ml-auto">
          <p class="mb-0 text-muted mr-3">Tampilkan</p>
          <ul class="nav nav-pills nav-filter">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">All (Default)</a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">All (Default)</a>
                <a class="dropdown-item" href="#">Artikel Terbaru</a>
                <a class="dropdown-item" href="#">Terbanyak Dibaca</a>
                <a class="dropdown-item" href="#">Terbanyak Dikomentari</a>
                <a class="dropdown-item" href="#">Terbanyak Dibagikan</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="list-posts mt-3">
      <div class="list-post-content">
        @foreach ($articles as $article)
            <div class="post-item post-vertical mb-0 mt-3">
                <div class="post-item-image">
                  @if($article->image)
                      <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
                  @else
                      <img src="{{ asset('images/default-image.png') }}" alt="">
                  @endif
                </div>
                <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li><a href="{{ url( $article->category->slug ) }}">{{ $article->category->name }}</a></li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h4>
                    <a href="{{ route( 'detail', $article->slug ) }}" class="read-more">Continue Reading</a>
                    <div class="post-item-footer pt-3 mt-auto">
                        <span>{{ $article->created_at->format('d/m/Y') }}</span>
                        <span class="font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
                    </div>
                </div>
            </div>
        @endforeach
      </div>
    </div>

    <div class="d-block text-center my-5">
      <button class="btn btn-outline-success">Selanjutnya</button>
    </div>
  </div>
@endsection
