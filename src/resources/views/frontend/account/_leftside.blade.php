<div class="col-md-4">
  <div class="widget mt-4">
    <h5 class="title">PROFILE</h5>
    <p>
      @if ( Auth::user() )
        {{ ucwords(Auth::guard('web')->user()->description) }}
      @else
        {{ $profile->description }}
      @endif
    </p>
  </div>
  <div class="widget">
    <h5 class="title">STATISTIK</h5>
    <div class="row mb-3">
      <div class="col text-center">
        <span class="text-primary d-block">{{ App\Models\Article::where('user_id', Auth::user()->id)->count() }}</span>
        <span>Article</span>
      </div>
      <div class="col text-center">
        <span class="text-primary d-block">{{ App\Models\Article::where('user_id', Auth::user()->id)->sum('total_views') }}</span>
        <span>Dibaca</span>
      </div>
      <div class="col text-center">
        <span class="text-primary d-block">{{ App\Models\Article::where('user_id', Auth::user()->id)->sum('total_comments') }}</span>
        <span>Komentar</span>
      </div>
    </div>
    <div class="row">
      <div class="col text-center">
        <span class="text-primary d-block">{{ $profile->total_followers }}</span>
        <span>Followers</span>
      </div>
      <div class="col text-center">
        <span class="text-primary d-block">{{ $profile->total_following }}</span>
        <span>Following</span>
      </div>
    </div>
  </div>
</div>
