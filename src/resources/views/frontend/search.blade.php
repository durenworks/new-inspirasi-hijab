@extends('frontend.template')

@section('content')
<section id="post-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <ul class="breadcrumb mt-md-5">
            <li><a href="{{ route( 'homepage' ) }}">Home</a></li>
            <li class="active"><a href="#">Pencarian Artikel: {{ $query }}</a></li>
        </ul>
        <div class="list-posts pr-md-3 mt-5">
          <div class="list-post-content">
            @if ( $articles->count() > 0 )
              @foreach ( $articles as $article )
                <div class="post-item post-vertical">
                  <div class="post-item-image">
                    @if($article->image)
                        <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
                    @else
                        <img src="{{ asset('images/default-image.png') }}" alt="">
                    @endif
                  </div>
                  <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li>
                        <a href="{{ route('category', $article->category->slug ) }}">
                          {{ $article->category->name }}
                        </a>
                      </li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h4>
                    <a href="{{ route( 'detail', $article->slug ) }}" class="read-more">Continue Reading</a>
                    <div class="post-item-footer pt-3 mt-auto">
                      <span>{{ $article->created_at->format('d/m/Y') }}</span>
                      <span class="font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
                    </div>
                  </div>
                </div>
              @endforeach
              {{ $articles->links() }}
            @else
              Article not found :(
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
