@extends('frontend.template')

@section('content')
<section id="header-2" class="mb-md-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="header-content">
          <ul class="breadcrumb mt-md-5">
            <li><a href="{{ route('homepage') }}">Home</a></li>
            <li class="active"><a href="#">Community</a></li>
          </ul>
          <div class="card mt-md-4 border-0">
            <div class="card-body text-center has--box-shadow">
              <a href="#" class="link-pink">KOMUNITAS INSPIRASI HIJAB</a>
              <h1 class="heading-text my-md-4">JOIN WITH US</h1>
              <p>Daftarkan komunitas kamu sekarang supaya bisa sharing lebih banyak!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Post -->
      <div class="col-md-8">
        <div class="list-communities">
          <div class="row mt-5">
            @foreach ($communities as $community)
              <div class="col-md-4">
                <div class="card card-community">
                  @if($community->avatar)
                    <a href="{{ route('profile',$community->url) }}"><img class="card-img-top" src="{{ route('profile.avatar',$community->avatar ) }}" alt=""></a>
                  @else
                    <a href="{{ route('profile',$community->url) }}" class="text-center"><img class="card-img-top" src="{{ asset('images/avatar-default.jpeg') }}" alt=""></a>
                  @endif
                  <div class="card-body">
                    <h5 class="text-center font-weight-bold mb-3">{{ $community->name }}</h5>
                    <a href="{{ route('profile',$community->url) }}" class="text-muted">
                        {{ route('profile',$community->url) }}
                    </a>
                    <p class="card-text py-2">
                        {{ $community->description }}
                    </p>
                  </div>
                </div>
              </div>
            @endforeach
            {{-- <div class="col-sm-12 text-center">
              <button class="btn btn-outline-success">Load More</button>
            </div> --}}
          </div>
        </div>
      </div>
      <!-- End Post -->

      <!-- Sidebar -->
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
      <!-- End Sidebar -->
    </div>
  </div>
</section>
@endsection
