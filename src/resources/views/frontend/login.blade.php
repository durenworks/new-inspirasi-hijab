<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Inspirasi Hijab - Login</title>
  <link rel="stylesheet" href="{{ asset('fe/css/frontend.css') }}">
</head>
<body>
  <div class="container">
    <div class="h-100vh d-flex">
      <div class="col-md-12 p-0 m-auto">
        <div class="border rounded">
          <div class="row">
            <div class="col-6">
              <img src="{{ asset('images/login-banner.png') }}" class="rounded-left img-fluid" alt="">
            </div>
            <div class="col-6">
              <div class="ath__form__container">
                <h5 class="heading-text font-weight-bold text-blue mb-4">Masuk</h5>
                <form method="POST" class="ath" action="{{ route('do_login') }}">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <input type="email" class="form-control ath__fc" name="email" placeholder="Masukan Email / Username kamu" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control ath__fc" placeholder="Masukan Password" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-row py-3 d-flex align-items-center">
                    <div class="col">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="remember">
                        <label class="form-check-label" for="remember">
                          Remember me
                        </label>
                      </div>
                    </div>
                    <div class="col">
                      <button type="submit" class="btn btn-success float-right">Masuk</button>
                    </div>
                  </div>
                  <div class="form-row py-3 d-flex align-items-center">
                    <div class="col">
                      <a href="{{ route('register') }}" class="text-blue">Daftar Sekarang</a>
                    </div>
                    <div class="col text-right">
                      <a href="{{ route('register') }}" class="text-muted">Forgot password?</a>
                    </div>
                  </div>
                    @if ($errors->has('info'))
                        <strong>{{ $errors->first('info') }}</strong>
                    @endif
                </form>

                <div class="hr-or my-3">
                  <span class="hr-or__line"></span>
                  <span class="hr-or__text">or</span>
                  <span class="hr-or__line"></span>
                </div>

                <div class="sociallog">
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-fb.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-ig.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                  <div class="sociallog__item">
                    <a href="#">
                      <img src="{{ asset('images/login-with-google.png') }}" class="img-fluid" alt="fb">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
