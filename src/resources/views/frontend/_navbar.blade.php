<div class="navbar navbar-expand-md navbar-light bg-white">
  <div class="container">
    <a href="{{ route('homepage') }}" class="navbar-brand">
      <img src="{{ asset('images/logo-inspirasi-hijab.png') }}" alt="Logo Inspirasi Hijab">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topNavbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="topNavbar">
      <form class="form-inline ml-auto d-none d-sm-block" action="{{ route('search') }}" method="get">
        <div class="input-group mr-sm-4">
          <input class="form-control border-right-0" name="s" type="search" placeholder="Cari artikel disini" aria-label="Search">
          <div class="input-group-append">
            <span class="input-group-text bg-white"><i class="fa fa-search"></i></span>
          </div>
        </div>

      </form>
      <ul class="navbar-nav">
        <li class="nav-item">
            @guest
            <a href="#" data-toggle="modal" data-target="#login" class="btn btn-success mr-sm-3">Tulis Artikelmu</a>
            @else
            <a href="{{ route('myaccount.tulis',Auth::guard('web')->user()->url) }}" class="btn btn-success mr-sm-3">Tulis Artikelmu</a>
            @endguest
        </li>
        <li class="nav-item">
          @guest
           <a href="#" data-toggle="modal" data-target="#login" class="nav-link btn-masuk">Login</a>
            @else
              <a class="nav-link" href="{{ route('do_logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                   Logout
                </a>

                <form id="logout-form" action="{{ route('do_logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </li>
      </ul>
    </div>
  </div>
</div>
<nav class="navbar navbar-expand-md navbar-dark bg-pink py-lg-0" id="mainMenu">
  <div class="container">
    <button class="navbar-toggler navbar-btn-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimaryMenu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-chevron-down"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarPrimaryMenu">
      <ul class="navbar-nav">
        <li class="nav-item {{ @$navbar == 'community'? 'active' : '' }}">
          <a href="{{ route('community') }}" class="nav-link">Community</a>
        </li>
        @foreach( App\Models\Category::where('is_subcategory', 0)->orderby('sequence','asc')->get() as $category )
          <li class="nav-item {{ ( $category->childs()->count() > 0 ) ? 'dropdown' : '' }}">
            @if ( $category->childs()->count() > 0 )
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $category->name }}
              </a>
              <div class="dropdown-menu p-2 bg-light fade" aria-labelledby="navbarDropdown">
                <div class="dropdown-item">
                  <ul class="sub-menu">
                    @foreach ( $category->childs as $subcategory )
                    <li class="sub-menu-item">
                      <a href="{{ route('category', $subcategory->slug) }}" class="sub-menu-link">{{ $subcategory->name }}</a>
                      {{--
                      <!-- Sub Category -->
                      <div class="dropdown-menu sub-dropdown-menu p-2 bg-light" aria-labelledby="navbarDropdown">
                        <div class="dropdown-item">
                          <ul class="sub-menu">
                            <li class="sub-menu-item">
                              <a href="#" class="sub-menu-link">Tutorial</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      --}}
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            @endif
          </li>
        @endforeach
        <li class="nav-item {{ @$navbar == 'penulis'? 'active' : '' }}">
          <a href="{{ route('penulis') }}" class="nav-link">Penulis</a>
        </li>
        <li class="nav-item {{ @$navbar == 'event'? 'active' : '' }}">
          <a href="{{ route('event') }}" class="nav-link">Event</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
