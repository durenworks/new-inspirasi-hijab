<section id="newsletter" class="bg-light mt-5">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <h3 class="heading-text font-weight-bold">Inspirasi Hijab Newsletter</h3>
        <p>Get highlights of the most Importen articles delivered to your email inbox</p>
      </div>
      <div class="col-sm-12 d-flex justify-content-center my-3">
        {{ 
          Form::open([
            'method' => 'POST',
            'id' => 'insertProfileform',
            'class' => 'form-inline',
            'url' => route('subscribe'),
            'enctype' => 'multipart/form-data',
          ]) 
        }}
          <div class="input-group subscribe">
            <input type="email" name="email" class="form-control" placeholder="Email Address" aria-label="Example text with button addon" aria-describedby="button-addon1">
            <div class="input-group-append">
              <button class="btn btn-outline-success" type="submit" id="button-addon1">Subscribe</button>
            </div>
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</section>
