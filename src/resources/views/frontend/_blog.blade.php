<section id="blog">
    <div class="container">
      <div class="row py-5">
        <div class="col-sm-12">
          <div class="list-posts mt-5">
            <div class="list-post-heading d-flex align-items-center mb-5">
              <h3 class="heading-text has-line font-weight-bold">
                {{ $category->name }}
              </h3>
              <a href="{{ route('category', $category->slug ) }}" class="list-post--show-all ml-auto">Lihat Semua</a>
            </div>
            <div class="list-post-content row">
              @foreach ($categoryArticles as $item)
                <div class="col-md-4">
                  <div class="post-item post-horizontal">
                    <div class="post-item-image">
                      <a href="{{ route( 'detail', $item->slug ) }}">
                        @if($item->image)
                          <img src="{{ route('myaccount.artikel.showThumbnail',[$item->user->url,$item->image]) }}" alt="image">
                        @else
                            <img src="{{ asset('images/default-image.png') }}" alt="image">
                        @endif
                      </a>
                    </div>
                    <div class="post-item-content">
                      <ul class="post-item-categories">
                        <li>
                          <a href="{{ route('category', $item->category->slug ) }}">
                            {{ $item->category->slug }}
                          </a>
                        </li>
                      </ul>
                      <h4 class="post-item-heading">
                        <a href="{{ route( 'detail', $item->slug ) }}">
                          {{ $item->title }}
                        </a>
                      </h4>
                      <a href="{{ route( 'detail', $item->slug ) }}" class="read-more">
                        Continue Reading
                      </a>
                      <div class="post-item-footer pt-3">
                        <span class=" text-muted">
                          {{ $item->created_at->format('d/m/Y') }}
                        </span>
                        <span class="text-muted">- by
                          <a href="{{ route( 'profile', $item->user->url ) }}" class="text-muted">
                            {{ $item->user->last_name }}
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
