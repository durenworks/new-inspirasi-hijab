<div class="widget">
  <div class="cta-write pt-5 pb-4 px-4 text-center d-flex justify-content-center align-items-end">
    @guest
      <a href="#" data-toggle="modal" data-target="#login" class="btn btn-success rounded-0 mt-5">TULIS ARTIKELMU DISINI</a>
    @else
      <a href="{{ route('myaccount.tulis',Auth::guard('web')->user()->url) }}" class="btn btn-success rounded-0 mt-5">TULIS ARTIKELMU DISINI</a>
    @endguest
  </div>
</div>
<div class="widget">
  <h5 class="widget-heading">MOST LIKED ARTICLE</h5>
  <div class="list-thumbnail">
    @foreach (popularArticle() as $article)
      <div class="thumbnail-item">
        <div class="thumbnail-image">
          <a href="{{ route( 'detail', $article->slug ) }}">
            @if($article->image)
              <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" class="img-fluid" alt="image">
            @else
                <img src="{{ asset('images/default-image.png') }}" class="img-fluid" alt="image">
            @endif
          </a>
        </div>
        <div class="thumbnail-content">
          <h6 class="thumbnail-heading text-truncate">
            <a href="{{ route( 'detail', $article->slug ) }}">
              {{ $article->title }}
            </a>
          </h6>
          <p class="thumbnail-footer">
            <a href="{{ route( 'profile', $article->user->url ) }}" class="text-muted font-italic">
              - {{ $article->user->last_name }}
            </a>
            <span class="viewer"><i class="fa fa-eye"></i> {{ $article->total_views }}</span>
          </p>
        </div>
      </div>
    @endforeach
  </div>
</div>
<div class="widget">
  @php
      $ads = getAds();
  @endphp
  @if ($ads)
    <a href="{{ $ads->url }}">
      @if($ads->image)
        <img src="{{ route('media.advertisement',$ads->image) }}" class="img-fluid" alt="Iklan">
      @else
      <img src="{{ asset('images/iklan.png') }}" class="img-fluid" alt="Iklan">
      @endif
    </a>
  @else
        <a href="#">
            <img src="{{ asset('images/iklan.png') }}" class="img-fluid" alt="Iklan">
        </a>
  @endif
</div>
<div class="widget">
  <h5 class="widget-heading">FOLLOW & LIKE US ON</h5>
  <div class="social d-flex">
    <a href="{{ session('facebook_url') == '' ? getSettingValue('facebook') : session('facebook_url') }}" class="social-link flex-fill facebook"><i class="fab fa-facebook fa-2x"></i></a>
    <a href="{{ session('twitter_url') == '' ? getSettingValue('twitter') : session('twitter_url') }}" class="social-link flex-fill twitter"><i class="fab fa-twitter fa-2x"></i></a>
    <a href="{{ session('instagram_url') == '' ? getSettingValue('instagram') : session('instagram_url') }}" class="social-link flex-fill instagram"><i class="fab fa-instagram fa-2x"></i></a>
    <a href="{{ session('youtube_url') == '' ? getSettingValue('youtube') : session('youtube_url') }}" class="social-link flex-fill youtube"><i class="fab fa-youtube fa-2x"></i></a>
  </div>
</div>
