<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Inspirasi Hijab</title>
  <link rel="stylesheet" href="{{ asset('fe/css/frontend.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
  @yield('style')
</head>
<body>
  @include('frontend._navbar')

  @yield('content')

  <section id="footer" class="bg-soft-pink">
    <div class="container">
      <div class="footer-container pt-5">
        <div class="footer-widget">
          <img src="{{ asset('images/logo-inspirasi-hijab.png') }}" alt="Logo">
        </div>
        <div class="footer-widget">
          <h6 class="footer-title">TENTANG INSPIRASI HIJAB</h6>
          <ul class="list-unstyled">
            <li><a href="{{ route('faq') }}">PROFIL</a></li>
            <li><a href="{{ route('faq') }}">PERFORMA & STATISTIK</a></li>
            <li><a href="{{ route('faq') }}">TIM</a></li>
            <li><a href="{{ route('faq') }}">FAQ</a></li>
          </ul>
        </div>
        <div class="footer-widget">
          <h6 class="footer-title">SYARAT DAN KETENTUAN</h6>
          <ul class="list-unstyled">
            <li><a href="{{ route('tos') }}">KETENTUAN LAYANAN</a></li>
            <li><a href="{{ route('tos') }}">KETENTUAN KONTEN</a></li>
            <li><a href="{{ route('tos') }}">KETENTUAN PERUBAHAN</a></li>
            <li><a href="{{ route('tos') }}">UNDANG-UNDANG ITE</a></li>
          </ul>
        </div>
        <div class="footer-widget">
          <h6 class="footer-title">KONTAK KAMI</h6>
          <ul class="list-unstyled">
            <li><a href="#">Jl. Raya Pemuda Kav.713, Jati</a></li>
            <li><a href="#">Pulo Gadung, Jakarta Timur</a></li>
            <li><a href="#">Daerah Khusus Ibukota Jakarta 13220</a></li>
          </ul>
        </div>
        <div class="footer-widget">
          <h6 class="footer-title">FOLLOW US</h6>
          <div class="social d-flex mt-3">
            <a href="{{ session('facebook_url') == '' ? getSettingValue('facebook') : session('facebook_url') }}" class="social-link flex-fill facebook"><i class="fab fa-facebook fa-2x"></i></a>
            <a href="{{ session('twitter_url') == '' ? getSettingValue('twitter') : session('twitter_url') }}" class="social-link flex-fill twitter"><i class="fab fa-twitter fa-2x"></i></a>
            <a href="{{ session('instagram_url') == '' ? getSettingValue('instagram') : session('instagram_url') }}" class="social-link flex-fill instagram"><i class="fab fa-instagram fa-2x"></i></a>
            <a href="{{ session('youtube_url') == '' ? getSettingValue('youtube') : session('youtube_url') }}" class="social-link flex-fill youtube"><i class="fab fa-youtube fa-2x"></i></a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-center">
          <hr>
          <p class="credit">© Copyright by InspirasiHijab 2019</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Modal Login -->
  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content rounded">
        <button type="button" class="close close-modal" data-dismiss="modal" data-target="" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-content__col modal-content__image">
            <img src="{{ asset('images/register-banner.jpeg') }}" alt="">
        </div>
        <div class="modal-content__col modal-content__form">
            <div class="ath__form__container">
                <div class="form__login">
                <h5 class="heading-text font-weight-bold text-blue mb-4">Masuk</h5>
                <form method="POST" class="ath" action="{{ route('do_login') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                    <input type="email" class="form-control ath__fc" name="email" placeholder="Masukan Email kamu" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <input type="password" class="form-control ath__fc" placeholder="Masukan Password" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-row py-3 d-flex align-items-center">
                    <div class="col">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" id="remember">
                        <label class="form-check-label" for="remember">
                            Remember me
                        </label>
                        </div>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success float-right">Masuk</button>
                    </div>
                    </div>
                    <div class="form-row py-3 d-flex align-items-center">
                    <div class="col">
                        <a href="#" id="daftar-btn" class="text-blue">Daftar Sekarang</a>
                    </div>
                    <div class="col text-right">
                        <a href="{{ route('register') }}" class="text-muted">Forgot password?</a>
                    </div>
                    </div>
                    @if ($errors->has('info'))
                        <strong>{{ $errors->first('info') }}</strong>
                    @endif
                </form>
                </div>
                <div class="form__daftar d-none">
                <h5 class="heading-text font-weight-bold text-blue">Daftar Sebagai:</h5>
                <ul class="nav nav-pills nav-fill nav-account mb-4">
                    <li class="nav-item">
                    <a class="nav-link active" href="#personal-tab-content" id="personal-tab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">Personal</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#komunitas-tab-content" id="komunitas-tab" data-toggle="tab" role="tab" aria-controls="home" aria-selected="false">Komunitas</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="personal-tab-content" role="tabpanel" aria-labelledby="personal-tab">
                    <form method="POST" class="ath" action="{{ route('do_register') }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="user" name="role">
                        <div class="form-group">
                        <input type="text" class="form-control ath__fc" id="name" placeholder="Masukan Nama kamu" name="name" required value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-group">
                        <input type="email" class="form-control ath__fc" id="email" name="email" placeholder="Masukan Email kamu" required value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-group">
                        <input type="text" class="form-control ath__fc" id="username" placeholder="Masukan Username kamu" name="username" required value="{{ old('username') }}">
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-group">
                        <input type="password" class="form-control ath__fc" id="password" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-row">
                        <div class="col-1">
                            <input type="checkbox" name="agreement" value="1">
                        </div>
                        <div class="col">
                            <p class="agreement">Dengan klik Daftar, Anda telah menyetujui <a href="#">Syarat & Ketentuan</a> serta <a href="#">kebijakan Privasi</a> Inspirasi Hijab</p>
                        </div>
                        </div>
                        <div class="form-row d-flex align-items-center">
                        <div class="col">
                            <a href="#" class="text-blue btn-masuk">Masuk</a>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success float-right">Daftar</button>
                        </div>
                        </div>
                        @if ($errors->has('info'))
                            <strong>{{ $errors->first('info') }}</strong>
                        @endif
                    </form>
                    </div>
                    <div class="tab-pane fade" id="komunitas-tab-content" role="tabpanel" aria-labelledby="komunitas-tab">
                    <form method="POST" class="ath" action="{{ route('do_register') }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="community" name="role">
                        <div class="form-group">
                            <input type="text" class="form-control ath__fc" id="nama-komunitas" name="name" placeholder="Masukan Nama Komunitas kamu" required value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control ath__fc" id="email-komunitas" name="email" placeholder="Masukan Email kamu" requiredvalue="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control ath__fc" id="username-komunitas" placeholder="Masukan Username kamu" name="username" required value="{{ old('username') }}">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control ath__fc" id="password-komunitas" placeholder="Password" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-row">
                            <div class="col-1">
                            <input type="checkbox" name="agreement" value="1">
                            </div>
                            <div class="col">
                            <p class="agreement">Dengan klik Daftar, Anda telah menyetujui <a href="#">Syarat & Ketentuan</a> serta <a href="#">kebijakan Privasi</a> Inspirasi Hijab</p>
                            </div>
                        </div>
                        <div class="form-row d-flex align-items-center">
                            <div class="col">
                            <a href="#" class="text-blue btn-masuk">Masuk</a>
                            </div>
                            <div class="col">
                            <button type="submit" class="btn btn-success float-right">Daftar</button>
                            </div>
                        </div>
                            @if ($errors->has('info'))
                                <strong>{{ $errors->first('info') }}</strong>
                            @endif
                        </form>
                    </div>
                </div>
                </div>

                <div class="hr-or my-3">
                <span class="hr-or__line"></span>
                {{--<span class="hr-or__text">or</span>--}}
                <span class="hr-or__line"></span>
                </div>

                {{--<div class="sociallog">
                <div class="sociallog__item">
                    <a href="#">
                    <img src="{{ asset('images/login-with-fb.png') }}" class="img-fluid" alt="fb">
                    </a>
                </div>
                <div class="sociallog__item">
                    <a href="#">
                    <img src="{{ asset('images/login-with-ig.png') }}" class="img-fluid" alt="fb">
                    </a>
                </div>
                <div class="sociallog__item">
                    <a href="#">
                    <img src="{{ asset('images/login-with-google.png') }}" class="img-fluid" alt="fb">
                    </a>
                </div>
                </div>--}}
            </div>
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('fe/js/app.js') }}"></script>
  <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
  <script src="{{ asset('fe/js/frontend.js') }}"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137347510-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-137347510-1');
  </script>
  @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
  <script>
    $(function() {
        $('#login').modal('show');
    });
  </script>
  @endif
  @if(!empty(Session::get('error_code')) && Session::get('error_code') == 4)
  <script>
    $(function() {
        $('#login').modal('show');
        $('.form__daftar').addClass('d-block');
        $('.form__daftar').removeClass('d-none');
        $('.form__login').addClass('d-none');
        $('.form__login').removeClass('d-block');
        $('#personal-tab').addClass('active');
        $('#komunitas-tab').removeClass('active');
    });
  </script>
  @endif
  @if(!empty(Session::get('error_code')) && Session::get('error_code') == 3)
  <script>
    $(function() {
        $('#login').modal('show');
        $('.form__daftar').addClass('d-block');
        $('.form__daftar').removeClass('d-none');
        $('.form__login').addClass('d-none');
        $('.form__login').removeClass('d-block');
        $('#personal-tab').removeClass('active');
        $('#komunitas-tab').addClass('active');
    });
  </script>
  @endif
  @yield('scripts')
</body>
</html>
