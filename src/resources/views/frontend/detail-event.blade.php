@extends('frontend.template')

@section('content')
<section id="header-3" class="mb-md-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="header-content">
          <ul class="breadcrumb mt-md-5">
            <li><a href="{{ route( 'homepage' ) }}">Home</a></li>
            <li class="active"><a href="{{ route( 'event.detail', $event->slug ) }}">{{ $event->title }}</a></li>
          </ul>
          <div class="post-item post-vertical">
            <div class="p-4 text-center">
              <div class="post-item-content">
                <ul class="post-item-categories">
                  <li><a href="{{ route( 'event', 'filter='.$event->status ) }}">{{ $event->status }}</a></li>
                </ul>
              </div>
              <h1 class="post-item-heading">{{ $event->title }}</h1>
            </div>
            <div class="post-item-footer bg-soft-pink py-3 px-4">
              <div class="d-flex justify-content-center align-items-center">
                <div class="px-5">
                  <span class="d-block">{{ indonesianDate( $event->start ) }}</span>
                  <span class="d-block"><i class="far fa-clock"></i> {{ $event->start->format('H:i') }}</span>
                </div>
                <div class="px-5">
                  @guest
                    <a href="#" data-toggle="modal" data-target="#login" class="btn btn-success mr-sm-3">Daftar</a>
                  @else
                    <a href="#" class="btn btn-success px-5">Daftar</a>
                  @endguest
                </div>
                <div class="px-5">
                  <span class="text-muted d-block font-italic"><small>- Event By</small></span>
                  <span class="text-muted d-block font-italic"><small>{{ $event->user->first_name. ' ' .$event->user->last_name }}</small></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Post -->
      <div class="col-md-8">
        <div class="post-detail mt-md-5">
          {!! $event->content !!}
        </div>
        <hr class="my-5">
      </div>
      <!-- End Post -->

      <!-- Sidebar -->
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
      <!-- End Sidebar -->
    </div>
  </div>
</section>
@endsection
