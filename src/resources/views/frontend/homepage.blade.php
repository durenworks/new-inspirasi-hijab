@extends('frontend.template')

@section('content')
<section id="header" class="mb-md-5">
  <div class="container">
    <div id="carouselHeader" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @foreach( $slider as $index => $article )
        <li data-target="#carouselHeader" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}" ></li>
        @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
      @foreach( $slider as $index => $article )
        <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
          <div class="post-item post-vertical rounded p-4">
            <div class="post-item-content">
              <ul class="post-item-categories">
                <li><a href="{{ route('category', $article->category->slug ) }}">{{ $article->category->name }}</a></li>
              </ul>
            </div>
            <h2 class="post-item-heading size-2"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h2>
            <div class="post-item-footer pt-3 mt-auto">
              <span class="float-right font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</section>

<section id="post-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="list-posts pr-md-3 mt-5">
          <div class="list-post-heading d-flex align-items-center mb-5">
            <h3 class="heading-text has-line font-weight-bold">MOST RECENT</h3>
            <a href="#" class="list-post--show-all ml-auto">Lihat Semua</a>
          </div>
          <div class="list-post-content">
            @if ( $articles->count() > 0 )
              @foreach ( $articles as $article )
                <div class="post-item post-vertical">
                  <div class="post-item-image">
                    @if($article->image)
                        <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
                    @else
                        <img src="{{ asset('images/default-image.png') }}" alt="">
                    @endif
                  </div>
                  <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li>
                        <a href="{{ route('category', $article->category->slug ) }}">
                          {{ $article->category->slug }}
                        </a>
                      </li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h4>
                    <a href="{{ route( 'detail', $article->slug ) }}" class="read-more">Continue Reading</a>
                    <div class="post-item-footer pt-3 mt-auto">
                      <span>{{ $article->created_at->format('d/m/Y') }}</span>
                      <span class="font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
                    </div>
                  </div>
                </div>
              @endforeach
            @else
              <!-- Dummy Data -->
              @for( $no = 1; $no <= 4; $no++ )
              <div class="post-item post-vertical">
                <div class="post-item-image">
                  <img src="{{ asset('images/artikel-image-'. $no .'.png') }}" alt="">
                </div>
                <div class="post-item-content">
                  <ul class="post-item-categories">
                    <li><a href="{{ url('/detail') }}">FASHION HIJAB</a></li>
                  </ul>
                  <h4 class="post-item-heading"><a href="{{ url('/detail') }}">Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.</a></h4>
                  <a href="{{ url('/detail') }}" class="read-more">Continue Reading</a>
                  <div class="post-item-footer pt-3">
                    <span class=" text-muted">27/01/2019</span>
                    <span class="text-muted">&nbsp;&nbsp;&nbsp;- by <a href="#" class="text-muted">Dedjumadilakir</a></span>
                  </div>
                </div>
              </div>
              @endfor
            @endif

            {{ $articles->links() }}
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
    </div>
  </div>
</section>

@include('frontend._penulis')

@if ($category)
  @include('frontend._blog')
@endif

@if ($communities->count())
  <section id="komunitas">
    <div class="container">
      <div class="row py-5">
        <div class="col-sm-12">
          <div class="d-flex align-items-center">
            <h3 class="heading-text has-line font-weight-bold">KOMUNITAS</h3>
            <a href="{{ route('community') }}" class="list-post--show-all ml-auto">Lihat Semua</a>
          </div>
        </div>
      </div>
      @if ($communities->count() < 6)
        <div class="row pb-5 d-flex justify-content-center align-items-center">
          @foreach ($communities as $community)
            <div class="col-md-2">
              {{-- <img src="{{ asset('images/hijabers-jakarta.png') }}" class="img-fluid" alt="hijabers jakarta"> --}}
              @if($community->avatar)
                <a href="{{ route('profile',$community->url) }}">
                  <img class="img-fluid" src="{{ route('media.avatar',$community->avatar ) }}" alt="">
                </a>
              @else
                <a href="{{ route('profile',$community->url) }}">
                  <img class="img-fluid" src="{{ asset('images/avatar-default.jpeg') }}" alt="">
                </a>
              @endif
            </div>
          @endforeach
        </div>
      @else
        <div class="row slick-6-with-dots pb-5 d-flex justify-content-center align-items-center">
          @foreach ($communities as $community)
            <div class="col-md-2">
              {{-- <img src="{{ asset('images/hijabers-jakarta.png') }}" class="img-fluid" alt="hijabers jakarta"> --}}
              @if($community->avatar)
                <a href="{{ route('profile',$community->url) }}">
                  <img class="img-fluid" src="{{ route('media.avatar',$community->avatar ) }}" alt="">
                </a>
              @else
                <a href="{{ route('profile',$community->url) }}">
                  <img class="img-fluid" src="{{ asset('images/avatar-default.jpeg') }}" alt="">
                </a>
              @endif
            </div>
          @endforeach
        </div>
      @endif
    </div>
  </section>
@endif

@include('frontend._newsletter')
@endsection
