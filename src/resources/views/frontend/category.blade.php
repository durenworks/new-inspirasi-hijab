@extends('frontend.template')

@section('content')
<section id="header" class="mb-md-5">
  <div class="container">
    <div id="carouselHeader" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @foreach( $slider as $index => $article )
        <li data-target="#carouselHeader" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}" ></li>
        @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
      @foreach( $slider as $index => $article )
        <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
          <div class="post-item post-vertical rounded p-4">
            <div class="post-item-content">
              <ul class="post-item-categories">
                <li><a href="#">{{ $article->category->name }}</a></li>
              </ul>
            </div>
            <h2 class="post-item-heading size-2"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h2>
            <div class="post-item-footer pt-3 mt-auto">
              <span class="float-right font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</section>

<section id="post-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="list-posts pr-md-3 mt-5">
          <div class="list-post-heading d-flex align-items-center mb-5">
            <h3 class="heading-text has-line font-weight-bold">MOST POPULAR</h3>
            <a href="#" class="list-post--show-all ml-auto">Lihat Semua</a>
          </div>
          <div class="list-post-content">
            @if ( $articles->count() > 0 )
              @foreach ( $articles as $article )
                <div class="post-item post-vertical">
                  <div class="post-item-image">
                    @if($article->image)
                        <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
                    @else
                        <img src="{{ asset('images/default-image.png') }}" alt="">
                    @endif
                  </div>
                  <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li>
                        <a href="{{ route('category', $article->category->slug ) }}">
                          {{ $article->category->name }}
                        </a>
                      </li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h4>
                    <a href="{{ route( 'detail', $article->slug ) }}" class="read-more">Continue Reading</a>
                    <div class="post-item-footer pt-3 mt-auto">
                      <span>{{ $article->created_at->format('d/m/Y') }}</span>
                      <span class="font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
                    </div>
                  </div>
                </div>
              @endforeach
              {{ $articles->links() }}
            @else
              @foreach ( $hitsArticles as $article )
                <div class="post-item post-vertical">
                  <div class="post-item-image">
                    @if($article->image)
                        <img src="{{ route('myaccount.artikel.showThumbnail',[$article->user->url,$article->image]) }}" alt="">
                    @else
                        <img src="{{ asset('images/default-image.png') }}" alt="">
                    @endif
                  </div>
                  <div class="post-item-content">
                    <ul class="post-item-categories">
                      <li>
                        <a href="{{ route('category', $article->category->slug ) }}">
                          {{ $article->category->name }}
                        </a>
                      </li>
                    </ul>
                    <h4 class="post-item-heading"><a href="{{ route( 'detail', $article->slug ) }}">{{ $article->title }}</a></h4>
                    <a href="{{ route( 'detail', $article->slug ) }}" class="read-more">Continue Reading</a>
                    <div class="post-item-footer pt-3 mt-auto">
                      <span>{{ $article->created_at->format('d/m/Y') }}</span>
                      <span class="font-italic">&nbsp;&nbsp;&nbsp;- by <a href="{{ route( 'profile', $article->user->url ) }}">{{ $article->user->last_name }}</a></span>
                    </div>
                  </div>
                </div>
              @endforeach
              {{ $hitsArticles->links() }}
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
    </div>
  </div>
</section>

<section id="penulis" class="bg-soft-pink mt-5">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <h3 class="heading-text font-weight-bold">PENULIS</h3>
      </div>
    </div>
    <div class="row slick-5-with-arrows">
      @foreach($users as $user)
        <div class="col">
          <div class="card writer">
            @if($user->avatar)
              <a href="{{ route('profile',$user->url) }}"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ route('profile.avatar',$user->avatar ) }}" alt=""></a>
            @else
            <a href="{{ route('profile',$user->url) }}" class="text-center"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/avatar-default.jpeg') }}" alt=""></a>
            @endif

            <div class="card-body text-center">
              <h6 class="card-title mb-0 font-weight-bolder"><a href="{{ route('profile',$user->url) }}" style="color:black">{{ $user->name }}</a></h6>
              <p class="join-date mb-0">Bergabung : {{ $user->created_at->format('d/m/Y') }}</p>
              <p class="link"><a href="{{ route('profile',$user->url) }}" class="text-muted">{{ route('profile',$user->url) }}</a></p>
              <p class="card-text">
              {{ $user->description }}
              </p>
            </div>
          </div>
        </div>
      @endforeach


    </div>
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <a href="{{ route('penulis') }}" class="btn btn-outline-success">Lihat Semua</a>
      </div>
    </div>
  </div>
</section>

<section id="newsletter" class="bg-light">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12 text-center">
        <h3 class="heading-text font-weight-bold">Inspirasi Hijab Newsletter</h3>
        <p>Get highlights of the most Importen articles delivered to your email inbox</p>
      </div>
      <div class="col-sm-12 d-flex justify-content-center my-3">
        <form action="#" class="form-inline">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Email Address" aria-label="Example text with button addon" aria-describedby="button-addon1">
            <div class="input-group-append">
              <button class="btn btn-outline-success" type="button" id="button-addon1">Subscribe</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
