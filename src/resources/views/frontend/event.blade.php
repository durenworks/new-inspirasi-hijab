@extends('frontend.template')

@section('content')
<section id="header-event" class="mb-md-5">
  <div class="container">
    <div id="carouselEvent" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @foreach( $slider as $index => $event )
        <li data-target="#carouselEvent" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}" ></li>
        @endforeach
      </ol>
      <div class="carousel-inner" role="listbox">
        <ul class="breadcrumb mt-md-5">
            <li><a href="{{ route( 'homepage' ) }}">Home</a></li>
            <li class="active"><a href="{{ route( 'event' ) }}">Event</a></li>
        </ul>
      @foreach( $slider as $index => $event )
        <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
            <div class="post-item post-vertical event">
                <div class="p-4">
                    <div class="post-item-content">
                        <ul class="post-item-categories">
                            <li><a href="?filter={{ $event->status }}">{{ $event->status }}</a></li>
                        </ul>
                    </div>
                    <h1 class="post-item-heading"><a href="{{ route('event.detail', $event->slug) }}">{{ $event->title }}</a></h1>
                </div>
                <div class="post-item-footer bg-soft-pink py-3 px-4">
                    <div class="row">
                        <div class="col">
                            <span class="d-block">{{ indonesianDate( $event->start ) }}</span>
                            <span class="d-block"><i class="far fa-clock"></i> {{ $event->start->format('h:i') }} PM</span>
                        </div>
                        <div class="col post-by">
                            <span class="text-muted d-block font-italic"><small>- Event By</small></span>
                            <span class="text-muted d-block font-italic"><small>{{ $event->user->first_name . ' ' . $event->user->last_name }}</small></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</section>

<section id="post-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="list-posts list-event pr-md-3 mt-5">
          <div class="list-post-content">
            @foreach ( $events as $event )
            <div class="post-item post-vertical event">
              <div class="post-item-image">
                <img src="{{ asset('images/artikel-image-1.png') }}" alt="">
              </div>
              <div class="post-item-content m-0">
                <div class="p-3">
                  <ul class="post-item-categories">
                    <li><a href="?filter={{ $event->status }}">{{ $event->status }}</a></li>
                  </ul>
                  <h4 class="post-item-heading"><a href="{{ route('event.detail', $event->slug) }}">{{ $event->title }}</a></h4>
                  <a href="{{ route('event.detail', $event->slug) }}" class="read-more">Continue Reading</a>
                </div>
                <div class="post-item-footer border-0 bg-soft-pink py-3 px-4 mt-auto">
                  <div class="row">
                    <div class="col">
                        <span class="d-block">{{ indonesianDate( $event->start ) }}</span>
                        <span class="d-block"><i class="far fa-clock"></i> {{ $event->start->format('h:i') }} PM</span>
                    </div>
                    <div class="col post-by">
                      <span class="text-muted d-block font-italic"><small>- Event By</small></span>
                      <span class="text-muted d-block font-italic"><small>{{ $event->user->first_name . ' ' . $event->user->last_name }}</small></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="sidebar mt-5 pl-md-5">
          @include('frontend._sidebar')
        </div>
      </div>
    </div>
  </div>
</section>

@include('frontend._penulis')

@include('frontend._blog')

<section id="komunitas">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12">
        <div class="d-flex align-items-center">
          <h3 class="heading-text has-line font-weight-bold">KOMUNITAS</h3>
          <a href="#" class="list-post--show-all ml-auto">Lihat Semua</a>
        </div>
      </div>
    </div>
    <div class="row slick-6-with-dots pb-5 d-flex justify-content-center align-items-center">
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-jakarta.png') }}" class="img-fluid" alt="hijabers jakarta">
      </div>
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-tanjungpinang.png') }}" class="img-fluid" alt="hijabers tanjungpinang">
      </div>
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-depok.png') }}" class="img-fluid" alt="hijabers depok">
      </div>
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-surabaya.png') }}" class="img-fluid" alt="hijabers surabaya">
      </div>
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-mom.png') }}" class="img-fluid" alt="hijabers mom">
      </div>
      <div class="col-md-2">
        <img src="{{ asset('images/hijabers-magelang.png') }}" class="img-fluid" alt="hijabers magelang">
      </div>
    </div>
  </div>
</section>

@include('frontend._newsletter')
@endsection
