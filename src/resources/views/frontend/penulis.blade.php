@extends('frontend.template')

@section('content')
<section id="header-2" class="mb-md-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="header-content">
          <ul class="breadcrumb mt-md-5">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="#">Penulis</a></li>
          </ul>
          <div class="card mt-md-4 border-0">
            <div class="card-body text-center has--box-shadow">
              <a href="#" class="link-pink">KONTRIBUTOR INSPIRASI HIJAB</a>
              <h1 class="heading-text my-md-4">PENULIS</h1>
              <p>Bagikan inspirasi apapun yang kamu punya dan sebarkan lebih banyak manfaat</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="post-section" class="my-md-5">
  <div class="container">
    <div class="row">
      <!-- Post -->
      <div class="col-md-12">
        <div class="list-communities">
          <div class="row mt-5">
            @foreach($users as $user)
              <div class="col-md-3">
                <div class="card writer">
                  @if($user->avatar)
                    <a href="{{ route('profile',$user->url) }}"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ route('profile.avatar',$user->avatar ) }}" alt=""></a>
                  @else
                  <a href="{{ route('profile',$user->url) }}" class="text-center"><img class="card-img-top rounded-circle mx-auto mt-4" src="{{ asset('images/avatar-default.jpeg') }}" alt=""></a>
                  @endif
                  <div class="card-body text-center">
                    <h6 class="card-title mb-0 font-weight-bolder"><a href="{{ route('profile',$user->url) }}" style="color:black">{{ $user->name }}</a></h6>
                    <p class="join-date mb-0">Bergabung : {{ $user->created_at->format('d/m/Y') }}</p>
                    <p class="link"><a href="{{ route('profile',$user->url) }}" class="text-muted">{{ route('profile',$user->url) }}</a></p>
                    <p class="card-text">
                    {{ $user->description }}
                    </p>
                  </div>
                </div>
              </div>
            @endforeach
            <div class="col-sm-12 text-center">
              <button class="btn btn-outline-success">Load More</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Post -->
    </div>
  </div>
</section>
@endsection
