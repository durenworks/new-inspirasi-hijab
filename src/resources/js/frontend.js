$('document').ready(function() {
  $('.sub-menu-item.dropdown').on('mouseover', function() {
    $(this).addClass('show');
    $(this).find('.sub-dropdown-menu').addClass('show');
  });
  $('.sub-menu-item.dropdown').on('mouseout', function() {
    $('.sub-menu-item').removeClass('show');
    $('.sub-dropdown-menu').removeClass('show');
  });
  $('#daftar-btn').on('click', function(e) {
    e.preventDefault();

    $('.form__daftar').addClass('d-block');
    $('.form__daftar').removeClass('d-none');
    $('.form__login').addClass('d-none');
    $('.form__login').removeClass('d-block');
  });

  $('.btn-masuk').on('click', function(e) {
    e.preventDefault();

    $('.form__login').addClass('d-block');
    $('.form__login').removeClass('d-none');
    $('.form__daftar').addClass('d-none');
    $('.form__daftar').removeClass('d-block');
  });
});

$('.slick-5-with-arrows').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.slick-6-with-dots').slick({
  dots: true,
  infinite: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
