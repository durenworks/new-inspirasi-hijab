<?php

return [
    'advertisement_thumbnail' => storage_path() . '/app/advertisement/thumbnail',
    'advertisement' => storage_path() . '/app/advertisement',
    'article_thumbnail' => storage_path() . '/app/article/thumbnail',
    'article' => storage_path() . '/app/article',
    'event_thumbnail' => storage_path() . '/app/event/thumbnail',
    'event' => storage_path() . '/app/event',
    'category_thumbnail' => storage_path() . '/app/category/thumbnail',
    'category' => storage_path() . '/app/category',
    'user_avatar' => storage_path() . '/app/user_avatar',
];
