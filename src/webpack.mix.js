const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/js/app.js', 'public/fe/js')
    .js('resources/js/frontend.js', 'public/fe/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/sass/frontend/frontend.scss', 'public/fe/css')
    .scripts(
        [
            'resources/assets/js/pace.min.js',
            'resources/assets/js/limitless/libraries/jquery.min.js',
            'resources/assets/js/limitless/libraries/bootstrap.min.js',
            'resources/assets/js/limitless/app.min.js',
            'resources/assets/js/nicescroll.min.js',
            'resources/assets/js/drilldown.js',
            'resources/assets/vendor/moment/moment.min.js',
            'resources/assets/js/jgrowl.min.js',
            'resources/assets/js/data-tables/datatables.min.js',
            'resources/assets/js/blockui.min.js',
            'resources/assets/js/mustache.js',
            'resources/assets/js/fancybox.min.js',
            'resources/assets/js/form/interactions.min.js'
        ],
        'public/js/backend.js'
    )
    .scripts(
        [
            'resources/assets/js/limitless/libraries/jquery.min.js',
            'resources/assets/js/limitless/libraries/bootstrap.min.js',
            'resources/assets/js/fancybox.min.js',
            'resources/assets/js/form/interactions.min.js'
        ],
        'public/js/app.js'
    )
    .scripts(
        [
            'resources/assets/js/form/select2.min.js',
            'resources/assets/js/form/form_select2.js'
        ],
        'public/js/select2.js'
    )
    .scripts(
        [
            'resources/assets/pickadate/picker.js',
            'resources/assets/pickadate/picker.date.js',
            'resources/assets/pickadate/picker.time.js',
            'resources/assets/pickadate/legacy.js'
        ],
        'public/js/pickadate.js'
    )
    .scripts(
        [
            'resources/assets/js/datepicker.js',
            'resources/assets/js/daterangepicker.js'
        ],
        'public/js/datepicker.js'
    )
    .scripts(
        [
            'resources/assets/js/sweet_alert.min.js',
            'resources/assets/js/notification.js'
        ],
        'public/js/notifications.js'
    )
    .scripts(
        [
            'resources/assets/js/summernote/summernote.min.js',
            'resources/assets/js/plugins/forms/styling/uniform.min.js'
        ],
        'public/js/summernote.js'
    )
    .scripts(
        [
            'resources/assets/js/form/tags/tagsinput.min.js',
            'resources/assets/js/form/tags/tokenfield.min.js',
            'resources/assets/js/form/tags/prism.min.js',
            'resources/assets/js/form/tags/typeahead.bundle.min.js'
        ],
        'public/js/tags.js'
    )
    .scripts(['resources/assets/js/bootbox.min.js'], 'public/js/bootbox.js')
    .styles(
        [
            'resources/assets/css/limitless/icons/icomoon/styles.css',
            'resources/assets/css/limitless/bootstrap.min.css',
            'resources/assets/css/limitless/core.css',
            'resources/assets/css/limitless/components.min.css',
            'resources/assets/css/limitless/colors.min.css',
            'resources/assets/css/limitless/style.css'
        ],
        'public/css/backend.css'
    )
    .styles(
        [
            'resources/assets/css/limitless/icons/icomoon/styles.css',
            'resources/assets/css/limitless/components.min.css'
        ],
        'public/css/frontend.css'
    )
    .sass('resources/plugins/slick/slick-theme.scss', 'public/plugins/slick/')
    .copy('resources/plugins/slick/slick.css', 'public/plugins/slick')
    .copy('resources/plugins/slick/slick.min.js', 'public/plugins/slick')
    .copy(
        'resources/assets/vendor/bootstrap-datepicker/bootstrap-datepicker.standalone.css',
        'public/vendor/bootstrap-datepicker/css'
    )
    .copy(
        'resources/assets/vendor/moment/moment.min.js',
        'public/fe/vendor/moment/moment.js'
    )
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory(
        'resources/plugins/slick/fonts/',
        'public/plugins/slick/fonts'
    )
    .copyDirectory('resources/assets/fonts/icomoon', 'public/fonts/icomoon')
    .copyDirectory(
        'resources/assets/vendor/summernote',
        'public/vendor/summernote'
    )
    .copyDirectory(
        'resources/assets/vendor/datatables',
        'public/vendor/datatables'
    )
    .copyDirectory('resources/assets/vendor/lightbox/images', 'public/images')
    .version()
    .browserSync({
        proxy: 'localhost:8000'
    })
